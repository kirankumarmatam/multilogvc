class BFS : public AssociativeProgram
{
	public:
		VERTEX_TYPE source;
		unsigned int numOfIterations;
		BFS(VERTEX_TYPE start, VERTEX_TYPE destination, unsigned int num_iterations)
		{
			source = start;
			target = destination;
			numOfIterations = num_iterations;
			converged = false;
		}
		void before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
		void combine_message(VERTEX_TYPE vertex_id, EDGE_MESSAGE_TYPE &edge_message, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context);
		void edge_program_weights(EDGE_WEIGHT_TYPE *Edge_weights, VERTEX_VALUE_TYPE &Vertex_value, EDGE_MESSAGE_TYPE *Result_data, MultiLogContext *multi_log_context){}
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
		inline void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context);
#else//EDGE_PROGRAM_OPTIMIZATION
		void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context);
#endif//EDGE_PROGRAM_OPTIMIZATION
		void after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
};
