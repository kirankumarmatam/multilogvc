#include "../../src/header.h"

void BFS::before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	hasActiveVertices = false;
	bool *active_list;
	active_list = (bool *)calloc(multi_log_context->graph_info.NumNodes, sizeof(bool));
	if(iteration_num == 0)
	{
		multi_log_context->program_edge_processing = this;
		multi_log_context->application_type = BFS_APPLICATION;
		cout << "Active list source = " << source << endl;
		active_list[source] = 1;
		hasActiveVertices = true;
	}
//	cout << "Before iteration errno = " << errno << endl;
	lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
//	cout << "Before iteration errno = " << errno << endl;
	cout << "iteration_num = " << iteration_num << endl;
	write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
	free(active_list);
//	cout << "Before iteration errno = " << errno << endl;

	if(iteration_num == 0)
	{
		if(source == target)
		{
			converged = true;
		}
		else {
			VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
			assert(visited != NULL);
//	cout << "Before iteration errno = " << errno << endl;
			lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
//	cout << "Before iteration errno = " << errno << endl;
			write(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, sizeof(VERTEX_VALUE_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes);
//	cout << "Before iteration errno = " << errno << endl;
			free(visited);
			visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
			lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
//	cout << "Before iteration errno = " << errno << endl;
			read(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, (DATA_SIZE)multi_log_context->graph_info.NumNodes * sizeof(VERTEX_VALUE_TYPE));
//	cout << "Before iteration errno = " << errno << endl;
			for(BUFFER_SIZE loop_1 = 0; loop_1 < 10;  loop_1++)
			{
				cout << loop_1 << " = " << visited[loop_1] << " " << "; ";
			}
			cout << endl;
			free(visited);
			cout << "v file id = " << multi_log_context->graph_info.vertexValue_file_id << " a file id = " << multi_log_context->graph_info.vertexActive_file_id << endl;
		}
	}
}

void BFS::combine_message(VERTEX_TYPE vertex_id, EDGE_MESSAGE_TYPE &edge_message, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context)
{
//	cout << "vid = " << vertex_id << " al = " << *active_list << " vv = " << vertex_value << endl;
	if(vertex_id == target)
	{
		converged = true;
	}
	else {
		if(vertex_value == 0)
		{
			*active_list = 1;
			hasActiveVertices = true;
		}
		//case 1 - not visited, case 2 - visited in this iteration, case 3 - already visited, case 4 - not at all visited
		//don't change the active vertex list except in 1st case
	}
}
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
inline void BFS::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context)
{
	Vertex_value = 1;
	for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
	{
		UPDATE_MESSAGE message_to_send;
		message_to_send.target = Edge_list[loop_1];
		message_to_send.data = vertex_id;
		multi_log_context->multi_log_cache->send_message((char*)&message_to_send, sizeof(message_to_send));
	}
}
#else//EDGE_PROGRAM_OPTIMIZATION
void BFS::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context)
{
	Vertex_value = 1;
	for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list.size(); loop_1++)
	{
		Result_data.push_back(vertex_id);
	}
}
#endif//EDGE_PROGRAM_OPTIMIZATION

void BFS::after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if(converged == true)
	{
		cout << "Converged!! Element found!!" << endl;
		multi_log_context->Finalize();
		exit(0);
	} else if(hasActiveVertices == false)
	{
		cout << "Element not found." << endl;
		multi_log_context->Finalize();
		exit(0);
	}
	if(iteration_num == numOfIterations)
	{
		cout << "Done!! iteration_num = " << iteration_num << " numOfIterations = " << numOfIterations << endl;
		multi_log_context->Finalize();
		exit(0);
	}
}

int main(int argc, char *argv[])
{
	Initialization(argc, argv);
	MultiLogEngine engine(argc, argv, 1);
#if(DEBUG_CODE == 1)
	cout << "After multi log engine initialization" << endl;
#endif//DEBUG_CODE
	BFS program(atoi(argv[7]), atoi(argv[8]), atoi(argv[9]));
//	BFS program(std::stoul(argv[7], nullptr, 10), std::stoul(argv[8], nullptr, 10));
#if(DEBUG_CODE == 1)
	cout << "After bfs program initialization" << endl;
#endif//DEBUG_CODE
	engine.run(&program, 1, BFS_APPLICATION);	
#if(DEBUG_CODE == 1)
	cout << "After engine run" << endl;
#endif//DEBUG_CODE
}
