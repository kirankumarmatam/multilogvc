#include "../../src/header.h"

void FLP::before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	converged = true;
	if(iteration_num == 0)
	{
		multi_log_context->program_edge_processing = this;
		bool *active_list;
		active_list = (bool *)malloc((multi_log_context->graph_info.NumNodes) * sizeof(bool));
		memset(active_list, 1, multi_log_context->graph_info.NumNodes);
		if(iteration_num == 0)
		{
			converged = false;
		}
		lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
		cout << "iteration_num = " << iteration_num << endl;
		write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
		free(active_list);

		VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
		assert(visited != NULL);
		for(VERTEX_VALUE_TYPE loop_1 = 0; loop_1 < multi_log_context->graph_info.NumNodes; loop_1++)
		{
			visited[loop_1] = loop_1;
		}
		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		write(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, sizeof(VERTEX_VALUE_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes);
		free(visited);
		visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		read(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, (DATA_SIZE)multi_log_context->graph_info.NumNodes * sizeof(VERTEX_VALUE_TYPE));
		for(BUFFER_SIZE loop_1 = 0; loop_1 < 10;  loop_1++)
		{
			cout << loop_1 << " = " << visited[loop_1] << " " << "; ";
		}
		cout << endl;
		free(visited);
		cout << "v file id = " << multi_log_context->graph_info.vertexValue_file_id << " a file id = " << multi_log_context->graph_info.vertexActive_file_id << endl;
	}
}

#if(EDGE_PROGRAM_OPTIMIZATION == 1)
inline void FLP::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context)
{
	std::map<VERTEX_TYPE, VERTEX_TYPE> frequent_count;
	for(VERTEX_TYPE loop_1 = 0; loop_1 < Message_list_size; loop_1++)
	{
		if(frequent_count.find(Message_list[loop_1]) == frequent_count.end())
		{
			frequent_count[Message_list[loop_1]] = 1;
		} else {
			frequent_count[Message_list[loop_1]]++;
		}
	}
	VERTEX_TYPE max_count = 0;
	VERTEX_VALUE_TYPE max_label = Vertex_value;
	for(std::map<VERTEX_TYPE, VERTEX_TYPE>::iterator it1 = frequent_count.begin(); it1 != frequent_count.end(); it1++)
	{
		if(it1->second > max_count)
		{
			max_label = it1->first;
			max_count = it1->second;
		}
	}
	if(frequent_count.find(Vertex_value) == frequent_count.end())
	{
		converged = false;
		Vertex_value = max_label;
	}
	else if(frequent_count[Vertex_value] < max_count)
	{
		converged = false;
		Vertex_value = max_label;
	}
	for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
	{
		UPDATE_MESSAGE message_to_send;
		message_to_send.target = Edge_list[loop_1];
		message_to_send.data = Vertex_value;
		multi_log_context->multi_log_cache->send_message((char*)&message_to_send, sizeof(message_to_send));
	}
	frequent_count.clear();
}
#else//EDGE_PROGRAM_OPTIMIZATION
inline void FLP::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST &Message_list, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context)
{
	std::map<VERTEX_TYPE, VERTEX_TYPE> frequent_count;
	for(VERTEX_TYPE loop_1 = 0; loop_1 < Message_list.size(); loop_1++)
	{
		if(frequent_count.find(Message_list[loop_1]) == frequent_count.end())
		{
			frequent_count[Message_list[loop_1]] = 1;
		} else {
			frequent_count[Message_list[loop_1]]++;
		}
	}
	VERTEX_TYPE max_count = 0;
	VERTEX_VALUE_TYPE max_label = Vertex_value;
	for(std::map<VERTEX_TYPE, VERTEX_TYPE>::iterator it1 = frequent_count.begin(); it1 != frequent_count.end(); it1++)
	{
		if(it1->second > max_count)
		{
			max_label = it1->first;
			max_count = it1->second;
		}
	}
	if(frequent_count.find(Vertex_value) == frequent_count.end())
	{
		converged = false;
		Vertex_value = max_label;
	}
	else if(frequent_count[Vertex_value] < max_count)
	{
		converged = false;
		Vertex_value = max_label;
	}
	for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list.size(); loop_1++)
	{
		Result_data.push_back(Vertex_value);
	}
	frequent_count.clear();
}
#endif//EDGE_PROGRAM_OPTIMIZATION

void FLP::after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if((converged == true) || (iteration_num == numOfIterations))
	{
		cout << "Converged!! iteration_num = " << iteration_num << " numOfIterations = " << numOfIterations << endl;
		multi_log_context->Finalize();
		exit(0);
	}
}

int main(int argc, char *argv[])
{
	Initialization(argc, argv);
	MultiLogEngine engine(argc, argv, 2);
#if(DEBUG_CODE == 1)
	cout << "After multi log engine initialization" << endl;
#endif//DEBUG_CODE
	FLP program(atoi(argv[7]));
#if(DEBUG_CODE == 1)
	cout << "After FLP program initialization" << endl;
#endif//DEBUG_CODE
	engine.run(&program, 2, -1);	
#if(DEBUG_CODE == 1)
	cout << "After engine run" << endl;
#endif//DEBUG_CODE
}
