#include "../../src/header.h"

#define INS 3
#define TENTAINS 2
#define NOTINS 1
#define UNKNOWN 0
#define PRINTRESULT 0

//typedef unsigned int EDGE_MESSAGE_TYPE;//sometimes source id is required to find out the minimum vertex id, sometimes just blank message is enough

void GC::before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if(iteration_num == 0)
	{
		multi_log_context->program_edge_processing = this;
		active_list = (bool *)malloc((multi_log_context->graph_info.NumNodes) * sizeof(bool));
		assert(active_list != NULL);
		memset(active_list, 1, multi_log_context->graph_info.NumNodes);

		srand(time(NULL));
		VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
		assert(visited != NULL);
		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		write(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, sizeof(VERTEX_VALUE_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes);
		free(visited);
		converged=0;
		TotalColor=0;
		step=1;
		HasUnknown=1;
		fired=0;
		cout << "v file id = " << multi_log_context->graph_info.vertexValue_file_id << " a file id = " << multi_log_context->graph_info.vertexActive_file_id << endl;
#if(GC_DEBUG == 1)
	numOfColored = 0;
	numOfUnColored = multi_log_context->graph_info.NumNodes;
	numOfUnKnowns = 0;
	numOfTENTAINS = 0;
	numOfNotInS = 0;
	numOfInS = 0;
#endif//GC_DEBUG
	}
	else {
		converged = 1;
		HasUnknown = 0;
		fired = 0;
	}
	lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
	cout << "iteration_num = " << iteration_num << endl;
	write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
	memset(active_list, 0, multi_log_context->graph_info.NumNodes);
}

#if(EDGE_PROGRAM_OPTIMIZATION == 1)
#if(ASYNCHRONOUS_PROGRAMMING == 0)
inline void GC::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context)
{
//	if(multi_log_context->current_iteration >= 1)
//		cout << " = " << step << endl;
	if(step == 1)
	{
		if(Vertex_value.IsColored == 0)
		{
			Vertex_value.type = UNKNOWN;
			for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
			{
				UPDATE_MESSAGE message_to_send;
				message_to_send.target = Edge_list[loop_1];
				message_to_send.data = vertex_id;
				multi_log_context->multi_log_cache->send_message((char*)&message_to_send, sizeof(message_to_send));
			}
			converged = 0;
#if(GC_DEBUG == 1)
			numOfUnKnowns += 1;
#endif//GC_DEBUG
			active_list[vertex_id] = 1;
		}
	}
	else if((step == 2) || (step == 5))
	{
		if((Vertex_value.IsColored == 0) && (Vertex_value.type == UNKNOWN))
		{
			if(step == 2)
			{
				assert(Message_list_size >= 0);
				Vertex_value.degree = Message_list_size;
			}
			else if(step == 5)
			{
				if(Vertex_value.degree <  Message_list_size)
				{
					cout << "d = " << Vertex_value.degree << " Mls = " << Message_list_size << endl;
				}
				assert(Vertex_value.degree >= Message_list_size);
				Vertex_value.degree -= Message_list_size;
			}
			float p=(float)rand()/(float)(RAND_MAX);
//				cout << "E0 " << p << " " << Vertex_value.degree << endl;
			if ((p*2*Vertex_value.degree)<1.0)
			{
//				cout << "E1" << endl;
				Vertex_value.type=TENTAINS;
				for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
				{
					UPDATE_MESSAGE message_to_send;
					message_to_send.target = Edge_list[loop_1];
					message_to_send.data = vertex_id;
					multi_log_context->multi_log_cache->send_message((char*)&message_to_send, sizeof(message_to_send));
				}
				fired=1;
#if(GC_DEBUG == 1)
				numOfTENTAINS += 1;
				numOfUnKnowns -= 1;
#endif//GC_DEBUG
			}
		}
		if(Vertex_value.IsColored == 0)
			active_list[vertex_id] = 1;
		converged = 0;
	}
	else if(step == 3)
	{
		if((Vertex_value.IsColored == 0) && (Vertex_value.type==TENTAINS))
		{
			bool IsMin=1;
			for(VERTEX_TYPE loop_1 = 0; loop_1 < Message_list_size; loop_1++)
			{
				if (vertex_id > Message_list[loop_1])
				{
					IsMin=0;
				}
			}
			if (IsMin)
			{
				Vertex_value.type=INS;
				for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
				{
					UPDATE_MESSAGE message_to_send;
					message_to_send.target = Edge_list[loop_1];
					message_to_send.data = vertex_id;
					multi_log_context->multi_log_cache->send_message((char*)&message_to_send, sizeof(message_to_send));
				}
#if(GC_DEBUG == 1)
				numOfInS += 1;
				numOfTENTAINS -= 1;
#endif//GC_DEBUG
			}
			else
			{
				Vertex_value.type=UNKNOWN;
#if(GC_DEBUG == 1)
				numOfTENTAINS -= 1;
				numOfUnKnowns += 1;
#endif//GC_DEBUG
			}
		}
		if(Vertex_value.IsColored == 0)
		{
			active_list[vertex_id] = 1;
		}
		converged=0;
	}
	else if(step == 4)
	{
		if((Vertex_value.IsColored == 0)  && (Vertex_value.type == UNKNOWN))
		{
			if (Message_list_size > 0)
			{
				Vertex_value.type=NOTINS;
				for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
				{
					UPDATE_MESSAGE message_to_send;
					message_to_send.target = Edge_list[loop_1];
					message_to_send.data = vertex_id;
					multi_log_context->multi_log_cache->send_message((char*)&message_to_send, sizeof(message_to_send));
				}
#if(GC_DEBUG == 1)
				numOfNotInS += 1;
				numOfUnKnowns -= 1;
#endif//
			}
			else
			{
				HasUnknown=1;
			}
		}
		if(Vertex_value.IsColored == 0)
		{
			active_list[vertex_id] = 1;
		}
		converged=0;
	}
	else if(step == 6)
	{
		if(Vertex_value.IsColored == 0)
		{
			if (Vertex_value.type == INS)
			{
#if(PRINTRESULT==1)
				Vertex_value.color=TotalColor+1;
#endif
				Vertex_value.IsColored=1;
#if(GC_DEBUG == 1)
				numOfInS -= 1;
				numOfColored += 1;
				numOfUnColored -= 1;
#endif//GC_DEBUG
			}
			else
			{
#if(GC_DEBUG == 1)
				assert(Vertex_value.type != TENTAINS);
				if(Vertex_value.type == NOTINS)
				{
					numOfNotInS -= 1;
					numOfUnKnowns += 1;
				}
#endif//GC_DEBUG
				Vertex_value.type=UNKNOWN;
				converged=0;
				for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
				{
					UPDATE_MESSAGE message_to_send;
					message_to_send.target = Edge_list[loop_1];
					message_to_send.data = vertex_id;
					multi_log_context->multi_log_cache->send_message((char*)&message_to_send, sizeof(message_to_send));
				}
			}
			Vertex_value.degree=0;
		}
		if(Vertex_value.IsColored == 0)
		{
			active_list[vertex_id] = 1;
		}
	}
}
#endif//ASYNCHRONOUS_PROGRAMMING
#endif//EDGE_PROGRAM_OPTIMIZATION

void GC::after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
#if(GC_DEBUG == 1)
	cout << "s = " << step << " f = " << fired << " c = " << converged << " C = " << numOfColored << " nC = " << numOfUnColored << " U = " << numOfUnKnowns << " T = " << numOfTENTAINS << " NI = " << numOfNotInS << " I = " << numOfInS << endl;
#endif//GC_DEBUG
	switch (step)
	{
		case 1:
			{
				step = 2;
				break;
			}
		case 2:
			{
				if (fired)
				{
					step=3;
				}
				else
				{
					step=2;
				}
				break;
			}
		case 3:
			{
				step=4;
				break;
			}
		case 4:
			{
				if (HasUnknown)
				{
					step=5;
				}
				else
				{
					step=6;
				}
				break;
			}
		case 5:
			{
				if(fired)
				{
					step=3;
				}
				else
				{
					step=2;
				}
				break;
			}
		case 6:
			{
				step=1;
				TotalColor++;
#if(GC_DEBUG == 1)
				numOfUnKnowns  = 0;
#endif//GC_DEBUG
				break;
			}
	}

	if((converged == 1) || (iteration_num == numOfIterations))
	{
		std::cout << "Total number of colors is " << TotalColor << std::endl;
		cout << "Done!! iteration_num = " << iteration_num << " numOfIterations = " << numOfIterations << " converged = " << converged << endl;
		multi_log_context->Finalize();
		exit(0);
	}
}

int main(int argc, char *argv[])
{
	Initialization(argc, argv);
	MultiLogEngine engine(argc, argv, 2);
#if(DEBUG_CODE == 1)
	cout << "After multi log engine initialization" << endl;
#endif//DEBUG_CODE
	GC program(atoi(argv[7]));
#if(DEBUG_CODE == 1)
	cout << "After GC program initialization" << endl;
#endif//DEBUG_CODE
	engine.run(&program, 2, -1);	
#if(DEBUG_CODE == 1)
	cout << "After engine run" << endl;
#endif//DEBUG_CODE
}
