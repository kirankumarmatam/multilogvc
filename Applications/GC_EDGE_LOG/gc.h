class GC : public MultiLogProgram
{
	public:
		ITERATION_NUM_TYPE numOfIterations;
		bool *active_list;
		bool converged;
		VERTEX_TYPE TotalColor;
		VERTEX_TYPE step;
		bool HasUnknown;
		bool fired;
		GC(ITERATION_NUM_TYPE num_iterations)
		{
			numOfIterations = num_iterations;
		}
		void before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
#if(ASYNCHRONOUS_PROGRAMMING == 0)
		inline void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context);
#endif//ASYNCHRONOUS_PROGRAMMING
#endif//EDGE_PROGRAM_OPTIMIZATION
		void after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
#if(GC_DEBUG == 1)
		VERTEX_TYPE numOfColored, numOfUnColored;
		VERTEX_TYPE numOfTENTAINS, numOfUnKnowns, numOfNotInS, numOfInS;
#endif//GC_DEBUG
#if(EDGE_LOG_MEASURE == 1)
		DATA_SIZE edge_log_written_data;
#endif//EDGE_LOG_MEASURE
};
