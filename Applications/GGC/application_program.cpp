#include "../../src/header.h"

extern CONFIGURATION configuration;

void GGC::before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	converged = true;
	if(iteration_num == 0)
	{
		multi_log_context->program_edge_processing = this;
		active_list = (bool *)malloc((multi_log_context->graph_info.NumNodes) * sizeof(bool));
		memset(active_list, 1, multi_log_context->graph_info.NumNodes);
		if(iteration_num == 0)
		{
			converged = false;
		}
//		lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
//		cout << "iteration_num = " << iteration_num << endl;
//		write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
//		free(active_list);

		VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
		assert(visited != NULL);
		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		write(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, sizeof(VERTEX_VALUE_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes);
		free(visited);
		cout << "v file id = " << multi_log_context->graph_info.vertexValue_file_id << " a file id = " << multi_log_context->graph_info.vertexActive_file_id << endl;
		bit_array = (bool *)calloc(configuration.graph_loader_colInd_buffer_size/sizeof(VERTEX_TYPE), sizeof(bool));
	}
	lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
	cout << "iteration_num = " << iteration_num << endl;
	write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
	memset(active_list, 0, multi_log_context->graph_info.NumNodes);
}

#if(EDGE_PROGRAM_OPTIMIZATION == 1)
#if(ASYNCHRONOUS_PROGRAMMING == 1 && PROCESS_EDGE_WEIGHT == 1)
inline void GGC::edge_program_lists_weights(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, IN_EDGE_AND_WEIGHT_FILE_TYPE *In_edge_weights, UPDATE_MESSAGE_LIST Results_data, BUFFER_SIZE &Result_data_size, MultiLogContext *multi_log_context)
{
	if(Message_list_size > Edge_list_size)
	{
		cout << "Mls = " << Message_list_size << " Els = " << Edge_list_size << " v = " << vertex_id << endl;
		for(BUFFER_SIZE loop_1 = 0; loop_1 < Message_list_size; loop_1++)
		{
			cout << "l1 = " << loop_1 << " M_s = " << Message_list[loop_1].source << " M_d = " << Message_list[loop_1].data << endl;
		}
		for(BUFFER_SIZE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
		{
			cout << "l1 = " << loop_1 << " e = " << Edge_list[loop_1] << endl;
		}
	}
	assert(Message_list_size <= Edge_list_size);
	if(multi_log_context->current_iteration == 0)
	{
	}
	for(BUFFER_SIZE loop_1 = 0; loop_1 < Message_list_size; loop_1++)
	{
		VERTEX_TYPE index, count;
		assert(Edge_list_size > 0);
		//	multi_log_context->timer_multi_log_access.start_timer();
		multi_log_context->multi_log_cache->binarySearch(Edge_list, 0, Edge_list_size-1, Message_list[loop_1].source, &index, &count );
		//	multi_log_context->timer_multi_log_access.end_timer();
		(In_edge_weights[index]).data = (Message_list[loop_1]).data;
	}
	if(multi_log_context->current_iteration == 0)
	{
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
		{
			Results_data[loop_1].data.source = vertex_id;
			Results_data[loop_1].target = Edge_list[loop_1];
			Results_data[loop_1].data.data = 0;
			active_list[Edge_list[loop_1]] = 1;
		}
		Result_data_size = Edge_list_size;
	}
	else {
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
		{
			assert((In_edge_weights[loop_1]).data < (configuration.graph_loader_colInd_buffer_size) && (In_edge_weights[loop_1]).data >= 0);
			bit_array[(In_edge_weights[loop_1]).data] = 1;
		}
		VERTEX_TYPE newColor = 0;
		for(VERTEX_TYPE loop_1 = 0; loop_1 < configuration.graph_loader_colInd_buffer_size; loop_1++)
		{
			if(bit_array[loop_1] == 0)
			{
				newColor = loop_1;
				break;
			}
		}
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
		{
			assert((In_edge_weights[loop_1]).data < (configuration.graph_loader_colInd_buffer_size) && (In_edge_weights[loop_1]).data >= 0);
			bit_array[(In_edge_weights[loop_1]).data] = 0;
		}

		if(newColor != Vertex_value)
		{
			converged = false;
			for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
			{
				Results_data[loop_1].data.source = vertex_id;
				Results_data[loop_1].target = Edge_list[loop_1];
				Results_data[loop_1].data.data = newColor;
				active_list[Edge_list[loop_1]] = 1;
			}
			Result_data_size = Edge_list_size;
			Vertex_value = newColor;
		}
	}
}
#endif//ASYNCHRONOUS_PROGRAMMING && PROCESS_EDGE_WEIGHT
#endif//EDGE_PROGRAM_OPTIMIZATION

void GGC::after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if((converged == true) || (iteration_num == numOfIterations))
	{
		cout << "Converged!! iteration_num = " << iteration_num << " numOfIterations = " << numOfIterations << endl;
		multi_log_context->Finalize();
#if(VERIFY_FLP == 1)
		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)mmap64(0, sizeof(VERTEX_VALUE_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes, PROT_READ, MAP_SHARED, multi_log_context->graph_info.vertexValue_file_id, 0);
		assert(visited != NULL);
		int rowPtr_file_id;
		rowPtr_file_id = open(multi_log_context->graph_info.csr_file_content.data(), O_RDWR);
		if (rowPtr_file_id == -1) {
			perror("Error opening file for writing");
			exit(EXIT_FAILURE);
		}

		FILE_OFFSET_TYPE *rowPtr = (FILE_OFFSET_TYPE *)mmap64(0, sizeof(FILE_OFFSET_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes, PROT_READ, MAP_SHARED, rowPtr_file_id, 0);
		assert(rowPtr != NULL);
//		int edgeLabels_file_id;
//		edgeLabels_file_id = open("/Data/Temp/temp7.txt", O_RDWR);
//		if (edgeLabels_file_id == -1) {
//			perror("Error opening file for writing");
//			exit(EXIT_FAILURE);
//		}

//		IN_EDGE_AND_WEIGHT_TYPE *edgeLabels = (IN_EDGE_AND_WEIGHT_TYPE *)mmap64(0, ((DATA_SIZE)(multi_log_context->graph_info.NumEdges))*sizeof(IN_EDGE_AND_WEIGHT_TYPE), PROT_READ, MAP_SHARED, edgeLabels_file_id, 0);
//		assert(edgeLabels != NULL);

		for(VERTEX_VALUE_TYPE loop_1 = 0; loop_1 < 1000; loop_1++)
		{
			cout << "l1 = " << loop_1 << " " << visited[loop_1] << " :";
			for(DATA_SIZE loop_2 = rowPtr[loop_1];loop_2 < rowPtr[loop_1+1]; loop_2++)
			{
				cout << " " << ((IN_EDGE_AND_WEIGHT_TYPE *)(multi_log_context->Edge_weights_file_array))[loop_2].data;
			}
			cout << endl;
		}
#endif//VERIFY_FLP
		exit(0);
	}
}

int main(int argc, char *argv[])
{
	Initialization(argc, argv);
	MultiLogEngine engine(argc, argv, 2);
#if(DEBUG_CODE == 1)
	cout << "After multi log engine initialization" << endl;
#endif//DEBUG_CODE
	GGC program(atoi(argv[7]));
#if(DEBUG_CODE == 1)
	cout << "After GGC program initialization" << endl;
#endif//DEBUG_CODE
	engine.run(&program, 2, -1);	
#if(DEBUG_CODE == 1)
	cout << "After engine run" << endl;
#endif//DEBUG_CODE
}
