#include "../../src/header.h"

unsigned int GGC::binarySearch(struct IN_EDGE_AND_WEIGHT_TYPE *partition_starting_vertex, VERTEX_TYPE l, VERTEX_TYPE r, VERTEX_TYPE x, VERTEX_TYPE *index, VERTEX_TYPE *number)
{
        while (l < r)
        {
                int64_t m = l + (r-l)/2;

                if (partition_starting_vertex[m].source == x)
                {
                        VERTEX_TYPE start = m;
                        m = m-1;
                        while((m >= 0) && (partition_starting_vertex[m].source == x))
                        {
                                start = m;
                                m = m-1;
                        }
                        m = start;
                        VERTEX_TYPE count=0;
                        while(((VERTEX_TYPE)m <= r) && (partition_starting_vertex[m].source == x))
                        {
                                count++;
                                m++;
                        }
                        *index = start;
                        *number = count;
                        return 0;
                }
                else if (partition_starting_vertex[m].source < x) 
                {
                        if(x < partition_starting_vertex[m+1].source)
                        {
                                *index = m;
                                *number = 1;
                                return 0;
                        }
                        else if(x > partition_starting_vertex[m+1].source)
                        {
                                l = m+1;
                        }
                        else if(x == partition_starting_vertex[m+1].source)
                        {
                                VERTEX_TYPE start = m+1;
                                m = start;
                                VERTEX_TYPE count = 0;
                                while(((VERTEX_TYPE)m <= r) && (partition_starting_vertex[m].source == x))
                                {
                                        count++;
                                        m++;
                                }
                                *index = start;
                                *number = count;
                                return 0;
                        }
                }
                else if(partition_starting_vertex[m].source > x) 
                {
                        r = m;
                }
        }
        if(l == r)
        {
                if(partition_starting_vertex[l].source <= x)
                {
                        *index = l;
                        *number = 1;
                        return 0;
                }
                else {
                        return 1;
                }
        }

        return 1;
}
