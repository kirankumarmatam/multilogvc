g++-7 -std=c++17 -std=gnu++17 -Wall -fopenmp -g -O3 -o GGC ../../src/framework_setup.cpp ../../src/logAndGraphData.cpp ../../src/logAndGraphData_cVC.cpp ../../src/IO_manager.cpp ../../src/multi_log_cache.cpp ../../src/initialization.cpp application_program.cpp binary_search.cpp ../../src/binary_search.cpp ../../src/utility.cpp -lrt -lboost_system -lstdc++fs -w  -lboost_thread -DGGC_COMPILE=1 -DVC_PROGRAM=1 -DASYNCHRONOUS_PROGRAMMING=1 -DPROCESS_EDGE_WEIGHT=1 -DNDEBUG -DPAGE_RANK_THRESHOLD_DEBUG=1 -DPAGE_RANK_THRESHOLD_DEBUG=1 -DSTATS_PAGE_IN_EFFICIENCY=1

#add -NDEBUG when collecting results
