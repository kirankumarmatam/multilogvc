class GGC : public MultiLogProgram
{
	public:
		ITERATION_NUM_TYPE numOfIterations;
		bool *active_list;
		bool converged;
		bool *bit_array;
		GGC(ITERATION_NUM_TYPE num_iterations)
		{
			converged = false;
			numOfIterations = num_iterations;
		}
		void before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
#if(ASYNCHRONOUS_PROGRAMMING == 0)
		inline void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context);
#elif(ASYNCHRONOUS_PROGRAMMING == 1)
		inline void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, UPDATE_MESSAGE_LIST Results_data, BUFFER_SIZE &Result_data_size, MultiLogContext *multi_log_context){}
#if(PROCESS_EDGE_WEIGHT == 1)
		inline void edge_program_lists_weights(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, IN_EDGE_AND_WEIGHT_FILE_TYPE *Edge_weights, UPDATE_MESSAGE_LIST Results_data, BUFFER_SIZE &Result_data_size, MultiLogContext *multi_log_context);
		unsigned int binarySearch(struct IN_EDGE_AND_WEIGHT_TYPE *partition_starting_vertex, VERTEX_TYPE l, VERTEX_TYPE r, VERTEX_TYPE x, VERTEX_TYPE *index, VERTEX_TYPE *number);
#endif//PROCESS_EDGE_WEIGHT
#endif//ASYNCHRONOUS_PROGRAMMING
#else//EDGE_PROGRAM_OPTIMIZATION
		inline void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST &Message_list, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context);
#endif//EDGE_PROGRAM_OPTIMIZATION
		void after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
};
