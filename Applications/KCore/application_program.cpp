#include "../../src/header.h"

void KCore::before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if(iteration_num == 0)
	{
		multi_log_context->program_edge_processing = this;
		active_list = (bool *)malloc((multi_log_context->graph_info.NumNodes) * sizeof(bool));
		assert(active_list != NULL);
		memset(active_list, 1, multi_log_context->graph_info.NumNodes);

		VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
		assert(visited != NULL);
		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		write(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, sizeof(VERTEX_VALUE_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes);
		free(visited);
		converged=0;
		cout << "v file id = " << multi_log_context->graph_info.vertexValue_file_id << " a file id = " << multi_log_context->graph_info.vertexActive_file_id << endl;
	}
	else {
		converged = 1;
	}
	lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
	cout << "iteration_num = " << iteration_num << endl;
	write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
	memset(active_list, 0, multi_log_context->graph_info.NumNodes);
}

#if(EDGE_PROGRAM_OPTIMIZATION == 1)
#if(ASYNCHRONOUS_PROGRAMMING == 0)
inline void KCore::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context)
{
	if(Edge_list_size < k_clique)
	{
		STRUCTURE_UPDATE_MESSAGE message_to_send;
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
		{
			message_to_send.target = Edge_list[loop_1];
			message_to_send.data.target_id = Edge_list[loop_1];
			message_to_send.data.operation = 1;
			message_to_send.data.source_id = vertex_id;
			multi_log_context->delete_edge((char*)&message_to_send, sizeof(message_to_send));
			active_list[Edge_list[loop_1]] = 1;
#if(DEBUG_CODE == 1)
			cout << "de vi = " << vertex_id << " El = " << Edge_list[loop_1] << endl;
#endif//DEBUG_CODE
		}
		multi_log_context->delete_vertex(vertex_id);
#if(DEBUG_CODE == 1)
		cout << "d vi = " << vertex_id << endl;
#endif//DEBUG_CODE
		converged = 0;
	}
}
#endif//ASYNCHRONOUS_PROGRAMMING
#endif//EDGE_PROGRAM_OPTIMIZATION

void KCore::after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if((converged == 1) || (iteration_num == numOfIterations))
	{
		cout << "Done!! iteration_num = " << iteration_num << " numOfIterations = " << numOfIterations << " converged = " << converged << endl;
		multi_log_context->Finalize();
		exit(0);
	}
}

int main(int argc, char *argv[])
{
	Initialization(argc, argv);
	MultiLogEngine engine(argc, argv, 2);
#if(DEBUG_CODE == 1)
	cout << "After multi log engine initialization" << endl;
#endif//DEBUG_CODE
	KCore program(atoi(argv[7]));
#if(DEBUG_CODE == 1)
	cout << "After KCore program initialization" << endl;
#endif//DEBUG_CODE
	engine.run(&program, 2, -1);	
#if(DEBUG_CODE == 1)
	cout << "After engine run" << endl;
#endif//DEBUG_CODE
}
