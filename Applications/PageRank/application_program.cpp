#include "../../src/header.h"

class PageRank : public AssociativeProgram
{
	public:
		VERTEX_TYPE RANDOMRESETPROB;
		ITERATION_NUM_TYPE numOfIterations;
		PageRank(ITERATION_NUM_TYPE num_iterations)
		{
			numOfIterations = num_iterations;
			RANDOMRESETPROB = 0.15;
		}
		void before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
		inline void combine_message(VERTEX_TYPE vertex_id, EDGE_MESSAGE_TYPE &edge_message, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context);
		inline void edge_program_weights(EDGE_WEIGHT_TYPE *Edge_weights, VERTEX_VALUE_TYPE &Vertex_value, EDGE_MESSAGE_TYPE *Result_data, MultiLogContext *multi_log_context){}
		inline void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context);
		void after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
};

void PageRank::before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if(iteration_num == 0)
	{
		multi_log_context->application_type = PAGE_RANK_APPLICATION;
		bool *active_list;
		active_list = (bool *)malloc((multi_log_context->graph_info.NumNodes) * sizeof(bool));
		memset(active_list, 1, multi_log_context->graph_info.NumNodes);
		lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
		cout << "iteration_num = " << iteration_num << endl;
		write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
		free(active_list);

		VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
		assert(visited != NULL);
		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		write(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, sizeof(VERTEX_VALUE_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes);
		free(visited);
		cout << "v file id = " << multi_log_context->graph_info.vertexValue_file_id << " a file id = " << multi_log_context->graph_info.vertexActive_file_id << endl;
	}
}

inline void PageRank::combine_message(VERTEX_TYPE vertex_id, EDGE_MESSAGE_TYPE &edge_message, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context)
{
	vertex_value += edge_message;
}

inline void PageRank::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context)
{
	VERTEX_VALUE_TYPE pageRank;
	if(multi_log_context->current_iteration == 0)
	{
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list.size(); loop_1++)
		{
			Result_data.push_back(1.0/Edge_list.size());
		}
		pageRank = RANDOMRESETPROB;
	} else {
		pageRank = RANDOMRESETPROB + (1 - RANDOMRESETPROB) * Vertex_value;
		if(Edge_list.size() > 0) {
			VERTEX_VALUE_TYPE pagerankcont = pageRank / Edge_list.size();
			for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list.size(); loop_1++)
			{
				Result_data.push_back(pagerankcont);
			}
		}
	}
	Vertex_value = 0;
}

void PageRank::after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if(iteration_num == numOfIterations)
	{
		cout << "Done!! iteration_num = " << iteration_num << " numOfIterations = " << numOfIterations << endl;
		multi_log_context->Finalize();
		exit(0);
	}
}

int main(int argc, char *argv[])
{
	Initialization(argc, argv);
	MultiLogEngine engine(argc, argv, 1);
#if(DEBUG_CODE == 1)
	cout << "After multi log engine initialization" << endl;
#endif//DEBUG_CODE
	PageRank program(atoi(argv[7]));
#if(DEBUG_CODE == 1)
	cout << "After PageRank program initialization" << endl;
#endif//DEBUG_CODE
	engine.run(&program, 1, PAGE_RANK_APPLICATION);	
#if(DEBUG_CODE == 1)
	cout << "After engine run" << endl;
#endif//DEBUG_CODE
}
