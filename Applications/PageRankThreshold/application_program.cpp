#include "../../src/header.h"

void PageRankThreshold::before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	bool *active_list;
	active_list = (bool *)malloc((multi_log_context->graph_info.NumNodes) * sizeof(bool));
	if(iteration_num == 0)
	{
		multi_log_context->program_edge_processing = this;
		multi_log_context->application_type = PAGE_RANK_THRESHOLD_APPLICATION;
		memset(active_list, 1, multi_log_context->graph_info.NumNodes);
		cout << "iteration_num = " << iteration_num << endl;

		VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
		assert(visited != NULL);
		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		write(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, sizeof(VERTEX_VALUE_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes);
		free(visited);
		//		cout << "v file id = " << multi_log_context->graph_info.vertexValue_file_id << " a file id = " << multi_log_context->graph_info.vertexActive_file_id << endl;
	}
	else {
		memset(active_list, 0, multi_log_context->graph_info.NumNodes);
	}
	lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
	write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
	free(active_list);
}

inline void PageRankThreshold::combine_message(VERTEX_TYPE vertex_id, EDGE_MESSAGE_TYPE &edge_message, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context)//This is a dummy implementation
{
	if((edge_message > 10.0) || (multi_log_context->current_iteration == 1))
	{
		*active_list = 1;
		if(multi_log_context->current_iteration != 1)
		{
			edge_message -= 10.0;
		}
	}
	vertex_value.change += edge_message;
}
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
inline void PageRankThreshold::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context)
{
	VERTEX_VALUE_TYPE PageRankThreshold;
	if(multi_log_context->current_iteration == 0)
	{
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
		{
			UPDATE_MESSAGE message_to_send;
			message_to_send.target = Edge_list[loop_1];
			message_to_send.data = 1.0f/Edge_list_size;
			multi_log_context->multi_log_cache->send_message((char*)&message_to_send, sizeof(message_to_send));
		}
		Vertex_value.value = RANDOMRESETPROB;
	} else {
		Vertex_value.change = Vertex_value.change * (1 - RANDOMRESETPROB);
		Vertex_value.value += Vertex_value.change;
		if(Edge_list_size > 0) {
			for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
			{
				UPDATE_MESSAGE message_to_send;
				message_to_send.target = Edge_list[loop_1];
				if(fabsf(Vertex_value.change) >= Delta)
				{
					assert((Vertex_value.change / Edge_list_size) <= 10.0);
					message_to_send.data = 10.0 + (Vertex_value.change / Edge_list_size);
				}
				else {
					message_to_send.data = (Vertex_value.change / Edge_list_size);
				}
				multi_log_context->multi_log_cache->send_message((char*)&message_to_send, sizeof(message_to_send));
			}
		}
		Vertex_value.change = 0;
	}
}
#else//EDGE_PROGRAM_OPTIMIZATION
inline void PageRankThreshold::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context)
{
	VERTEX_VALUE_TYPE PageRankThreshold;
	if(multi_log_context->current_iteration == 0)
	{
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list.size(); loop_1++)
		{
			Result_data.push_back(1.0/Edge_list.size());
		}
		Vertex_value.value = RANDOMRESETPROB;
	} else {
		Vertex_value.change = Vertex_value.change * (1 - RANDOMRESETPROB);
		Vertex_value.value += Vertex_value.change;
		if(Edge_list.size() > 0) {
			for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list.size(); loop_1++)
			{
				Result_data.push_back(Vertex_value.change / Edge_list.size());
			}
		}
		Vertex_value.change = 0;
	}
}
#endif//EDGE_PROGRAM_OPTIMIZATION

void PageRankThreshold::after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if(iteration_num == numOfIterations)
	{
		cout << "Done!! iteration_num = " << iteration_num << " numOfIterations = " << numOfIterations << endl;
		multi_log_context->Finalize();
		exit(0);
	}
}

int main(int argc, char *argv[])
{
	Initialization(argc, argv);
	MultiLogEngine engine(argc, argv, 1);
#if(DEBUG_CODE == 1)
	cout << "After multi log engine initialization" << endl;
#endif//DEBUG_CODE
	PageRankThreshold program(atoi(argv[7]));
#if(DEBUG_CODE == 1)
	cout << "After PageRankThreshold program initialization" << endl;
#endif//DEBUG_CODE
	engine.run(&program, 1, PAGE_RANK_APPLICATION);	
#if(DEBUG_CODE == 1)
	cout << "After engine run" << endl;
#endif//DEBUG_CODE
}
