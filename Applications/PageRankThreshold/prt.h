class PageRankThreshold : public AssociativeProgram
{
	public:
		PAGERANK_VERTEX_VALUE_TYPE RANDOMRESETPROB;
		ITERATION_NUM_TYPE numOfIterations;
		PageRankThreshold(ITERATION_NUM_TYPE num_iterations)
		{
			numOfIterations = num_iterations;
			RANDOMRESETPROB = 0.15;
			Delta = 1;
		}
		void before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
		inline void combine_message(VERTEX_TYPE vertex_id, EDGE_MESSAGE_TYPE &edge_message, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context);
		inline void edge_program_weights(EDGE_WEIGHT_TYPE *Edge_weights, VERTEX_VALUE_TYPE &Vertex_value, EDGE_MESSAGE_TYPE *Result_data, MultiLogContext *multi_log_context){}
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
		inline void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context);
#else//EDGE_PROGRAM_OPTIMIZATION
		inline void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context);
#endif//EDGE_PROGRAM_OPTIMIZATION
		void after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
};
