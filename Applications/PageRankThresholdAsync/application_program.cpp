#include "../../src/header.h"

void PRTA::before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if(iteration_num == 0)
	{
		multi_log_context->program_edge_processing = this;
		active_list = (bool *)malloc((multi_log_context->graph_info.NumNodes) * sizeof(bool));
		memset(active_list, 1, multi_log_context->graph_info.NumNodes);
//		lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
//		write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
//		memset(active_list, 0, multi_log_context->graph_info.NumNodes);

		VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes, sizeof(VERTEX_VALUE_TYPE));
		assert(visited != NULL);
		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		write(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, sizeof(VERTEX_VALUE_TYPE) * (uint64_t)multi_log_context->graph_info.NumNodes);
		free(visited);
//		cout << "v file id = " << multi_log_context->graph_info.vertexValue_file_id << " a file id = " << multi_log_context->graph_info.vertexActive_file_id << endl;
	}
	lseek(multi_log_context->graph_info.vertexActive_file_id, 0, SEEK_SET);
	cout << "iteration_num = " << iteration_num << endl;
	write(multi_log_context->graph_info.vertexActive_file_id, (void *)active_list, sizeof(bool) * (uint64_t)multi_log_context->graph_info.NumNodes);
	memset(active_list, 0, multi_log_context->graph_info.NumNodes);
}

#if(EDGE_PROGRAM_OPTIMIZATION == 1)
#if(ASYNCHRONOUS_PROGRAMMING == 1)
#if(STRUCTURE_UPDATE == 1)
//handle the updates here
#endif//STRUCTURE_UPDATE
//Need to add associative program for page rank
inline void PRTA::edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, UPDATE_MESSAGE_LIST Results_data, BUFFER_SIZE &Result_data_size, MultiLogContext *multi_log_context)
{
	if(multi_log_context->current_iteration == 0)
	{
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
		{
			Results_data[loop_1].target = Edge_list[loop_1];
			Results_data[loop_1].data = 1.0f/Edge_list_size;
		}
		Result_data_size = Edge_list_size;
		Vertex_value.value = RANDOMRESETPROB;
		active_list[vertex_id] = 1;
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Message_list_size; loop_1++)
		{
			Vertex_value.change += Message_list[loop_1];
		}
	} else {
//		bool activate = 0;
		for(VERTEX_TYPE loop_1 = 0; loop_1 < Message_list_size; loop_1++)
		{
//			if((fabsf(Message_list[loop_1]) > 10.0) || (multi_log_context->current_iteration == 1))
//			{
//				activate = 1;
//				if(multi_log_context->current_iteration != 1)
//				{
//					Message_list[loop_1] -= 10.0;
//				}
//			}
			Vertex_value.change += Message_list[loop_1];
		}
//		if(activate == 1 || multi_log_context->current_iteration == 1)
//		{
			Vertex_value.change = Vertex_value.change * (1 - RANDOMRESETPROB);
			Vertex_value.value += Vertex_value.change;
			if(Edge_list_size > 0) {
				for(VERTEX_TYPE loop_1 = 0; loop_1 < Edge_list_size; loop_1++)
				{
					Results_data[loop_1].target = Edge_list[loop_1];
//					active_list[Edge_list[loop_1]] = 1;
#if(DeltaNotChange == 1)
					if(fabsf(Vertex_value.change)/(Edge_list_size*1.0) >= 0.4)
#elif(DeltaNotChange == 0)
					if(fabsf(Vertex_value.change) >= Delta)
#endif//DeltaNotChange
					{
						active_list[Edge_list[loop_1]] = 1;
	//					assert((Vertex_value.change / Edge_list_size) <= 10.0);
	//					Results_data[loop_1].data = 10.0 + (Vertex_value.change / Edge_list_size);
				}
	//				else {
						Results_data[loop_1].data = (Vertex_value.change / Edge_list_size);
//					}
				}
				Result_data_size = Edge_list_size;
			}
			Vertex_value.change = 0;
//		}
	}
}
#endif//ASYNCHRONOUS_PROGRAMMING
#endif//EDGE_PROGRAM_OPTIMIZATION

void PRTA::after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context)
{
	if(iteration_num == numOfIterations)
	{
		cout << "Done!! iteration_num = " << iteration_num << " numOfIterations = " << numOfIterations << endl;
		multi_log_context->Finalize();
		exit(0);
	}
}

int main(int argc, char *argv[])
{
	Initialization(argc, argv);
	MultiLogEngine engine(argc, argv, 2);
#if(DEBUG_CODE == 1)
	cout << "After multi log engine initialization" << endl;
#endif//DEBUG_CODE
	PRTA program(atoi(argv[7]));
#if(DEBUG_CODE == 1)
	cout << "After PRTA program initialization" << endl;
#endif//DEBUG_CODE
	engine.run(&program, 2, -1);	
#if(DEBUG_CODE == 1)
	cout << "After engine run" << endl;
#endif//DEBUG_CODE
}
