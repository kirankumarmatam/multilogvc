class PRTA : public MultiLogProgram
{
	public:
		PAGERANK_VERTEX_VALUE_TYPE RANDOMRESETPROB;
		ITERATION_NUM_TYPE numOfIterations;
		bool *active_list;
		PRTA(ITERATION_NUM_TYPE num_iterations)
		{
			numOfIterations = num_iterations;
			RANDOMRESETPROB = 0.05;
			Delta = 100.0;
		}
		void before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
#if(ASYNCHRONOUS_PROGRAMMING == 1)
		inline void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, UPDATE_MESSAGE_LIST Results_data, BUFFER_SIZE &Result_data_size, MultiLogContext *multi_log_context);
#endif//ASYNCHRONOUS_PROGRAMMING
#endif//EDGE_PROGRAM_OPTIMIZATION
		void after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context);
};
