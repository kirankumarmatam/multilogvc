#include "graphFiltering.h"

int main(int argc, char **argv)
{
	if(argc < 3)
	{
		cout << "Usage is ./a.out input_csr_file.txt edge_list_file.txt" << endl;
		return 0;
	}

	myCsrFile.open(argv[1], fstream::in);
	unsigned int NumNodes;
	EdgeIndexType NumEdges;
	myCsrFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myCsrFile.read((char *)(&NumEdges), sizeof(EdgeIndexType));
	cout << "CSR numNodes = " << NumNodes << " numEdges = " << NumEdges << endl;
	myCsrFile.close();

	int fd;

	fd = open(argv[1], O_RDONLY);
	if (fd == -1) {
		perror("Error opening file for reading");
		exit(EXIT_FAILURE);
	}

	uint64_t FileSize = sizeof(unsigned int) + sizeof(EdgeIndexType) + ((uint64_t)(NumNodes+1))*(sizeof(EdgeIndexType)) + (NumEdges)*(sizeof(unsigned int));
	void *temp_array_address = mmap64(0, FileSize, PROT_READ, MAP_SHARED, fd, 0);
	if (temp_array_address == MAP_FAILED) {
		close(fd);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}

	EdgeIndexType *Array_input_rowPtr = (EdgeIndexType *)((char *)temp_array_address + sizeof(unsigned int) + sizeof(EdgeIndexType));//new EdgeIndexType [NumNodes + 1];
	unsigned int *Array_input_colPtr = (unsigned int *)((char *)temp_array_address + sizeof(unsigned int) + sizeof(EdgeIndexType) + ((uint64_t)(NumNodes+1))*(sizeof(EdgeIndexType)));//new unsigned int [NumEdges];

	myEdgeListFile.open(argv[2], fstream::in | fstream::out | fstream::trunc);
	if (!myEdgeListFile.is_open())
		cout << " Cannot open edge list output file!" << endl;

	if (myEdgeListFile.fail())
	{
		cout <<"In store_graph" << endl;
		cout << "Failed = " << myEdgeListFile.fail() << endl;
		exit(0);
	}

	cout <<"GetNodes " << NumNodes << endl;

	for (unsigned int i = 0; i < NumNodes; i++)
	{
		if(i % 10000000 == 0)
			cout << "Printed 10M nodes into flash file" << endl;
		for (EdgeIndexType e = Array_input_rowPtr[i]; e < Array_input_rowPtr[i+1]; e++)
		{
			string temp = to_string(i) + " " + to_string(Array_input_colPtr[e]) + "\n";
			myEdgeListFile.write(temp.data(), temp.size());
			temp.clear();
		}
	}

	cout << " Finished writing flash to file" << endl;
	myEdgeListFile.close();

	if (munmap(temp_array_address, FileSize) == -1) {
		perror("Error un-mmapping the file");
	}
	close(fd);

	return 0;
}
