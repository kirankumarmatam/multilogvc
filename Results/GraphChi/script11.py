#This script is to run the graphchi applications

import os
#Datasets = ["/Datasets/Benchmarks/SNAP/GraphChi/soc-LiveJournal1.net","/Datasets/Benchmarks/SNAP/GraphChi/graph500_32M.txt","/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.net","/Datasets/Benchmarks/SNAP/GraphChi/graph500_128M.txt"]
#Datasets = ["/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.net", "/Data/Benchmarks/SNAP/LargeGraphs/YahooWebScope/ydata-yaltavista-webmap-v1_0_links.txt"]
Datasets = ["/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.net", "/Data/Benchmarks/SNAP/LargeGraphs/YahooWebScope/ydata-yaltavista-webmap-v1_0_links.txt"]
#Datasets = ["/Data/Benchmarks/SNAP/LargeGraphs/YahooWebScope/ydata-yaltavista-webmap-v1_0_links.txt"]
#Datasets = ["/ssd/graph500/soc-LiveJournal1.net","/ssd/graph500/graph500_32M.txt"]
#Memory = ["20","225","800", "900"]
Memory = ["800","1000"]
os.chdir("/home/ossd/GraphSSD/MultiLogVC/graphchi-cpp")
Filetype = ["edgelist","adjlist"]

Run = [0,0,0,0,0,0,0,1,0]
Datasets_root = ["101","5"]
Datasets_target = ["2000000000","2000000000","2000000000","2000000000","2000000000","2000000000","2000000000","2000000000","2000000000"]
Datasets_max_level = [24,1553]
Datasets_iterations = [2, 3, 4, 5, 6, 7, 8, 12, 16]
os.system("make myapps/BFS_edge")
for i in range(len(Datasets)):
	for j in range(len(Run)):
		max_iteration = Datasets_iterations[j]+1
		if(Run[j] == 1):
			os.system("bin/myapps/BFS_edge file " + Datasets[i] + " filetype " + Filetype[i] + " execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i] + " root " + Datasets_root[i] + " target " + Datasets_target[j] + " niters " + str(max_iteration) + " scheduler 1")

Run = [0,0]
PR_iterations = [3,3]
os.system("make myapps/pagerank")
for i in range(len(Datasets)):
	if(Run[i] == 1):
		os.system("bin/myapps/pagerank file "+ Datasets[i] + " filetype "+ Filetype[i] +" execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i]+" niters " + str(PR_iterations[i]))

Run = [1,1]
PRT_iterations = [15,15]
os.system("make myapps/pagerank_delta_edge_delta")
for i in range(len(Datasets)):
	if(Run[i] == 1):
		os.system("bin/myapps/pagerank_delta_edge_delta file "+ Datasets[i] + " filetype "+ Filetype[i] +" execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i]+" niters " + str(PRT_iterations[i]))

Run = [1,1]
FLP_iterations = [25,25]
os.system("make myapps/flp_edge")
for i in range(len(Datasets)):
	if(Run[i] == 1):
		os.system("bin/myapps/flp_edge file "+ Datasets[i] + " filetype "+ Filetype[i] +" execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i]+" niters " + str(FLP_iterations[i]))
