#This script is to run graph applications and collect performance numbers for analysing
#!/bin/bash

import os

Dataset_directory = "/Data/Benchmarks/"
'''
Graphs_to_run = ["YahooWebScope", "graph500_512M", "graph500_1G"]
Graphs_flash_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/YahooWebScope_flash.txt", "SNAP/ProcessedGraphs/graphSSD_files/graph500_flash_512M.txt", "graph500/graph500_layout/graphSSD_files/graph500_flash_1G.txt"]
Graphs_gtl_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/YahooWebScope_gtl.txt", "SNAP/ProcessedGraphs/graphSSD_files/graph500_gtl_512M.txt", "graph500/graph500_layout/graphSSD_files/graph500_gtl_1G.txt"]
Graphs_flash_to_run_baseline = ["SNAP/ProcessedGraphs/csr_files/YahooWebScope_csr.txt", "SNAP/ProcessedGraphs/csr_files/graph500_csr_512M.txt", "graph500/graph500_layout/csr_files/graph500_csr_1G.txt"]
'''
Graphs_to_run = ["com-friendster","YahooWebScope"]
Graphs_csr_file = ["/Data/Benchmarks/SNAP/ProcessedGraphs/csr_files/com-friendster.ungraph_csr.net", "/Data/Benchmarks/SNAP/ProcessedGraphs/csr_files/YahooWebScope_csr.txt"]
Graphs_csr_tail_file = ["/Data/Benchmarks/SNAP/ProcessedGraphs/csr_files/com-friendster.ungraph_csr_tail.net", "/Data/Benchmarks/SNAP/ProcessedGraphs/csr_files/YahooWebScope_csr_tail.txt"]
Output_file = "/home/ossd/GraphSSD/MultiLogVC/output.net"
Processed_output_file = "/home/ossd/GraphSSD/MultiLogVC/Processed_output.net"
os.system("rm "+Output_file)
os.system("rm "+Processed_output_file)
os.system("touch "+Output_file)
os.system("touch "+Processed_output_file)

##BFS -- BEGIN --
Run = [0,0,0,0,0,0,0,0,0]
BFS_MaxLevelNumber = [24, 1553]
BFS_iterations = [2, 3, 4, 5, 6, 7, 8, 12, 16]
BFS_root = ["101", "5"]

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/MultiLogVC/Applications/BFS")
	os.system("echo " + Graphs_to_run[i] + " BFS >> " + Output_file)
	for k in range(len(BFS_iterations)):
		max_iteration = BFS_iterations[k]
		if(Run[k] == 1):
			os.system("sed -i 's/typedef .* VERTEX_VALUE_TYPE/typedef unsigned int VERTEX_VALUE_TYPE/g' ../../src/datatypes.h")
			os.system("sed -i 's/typedef .* EDGE_MESSAGE_TYPE/typedef unsigned int EDGE_MESSAGE_TYPE/g' ../../src/datatypes.h")
			os.system("rm BFS;sh compile.sh; ./BFS " + Graphs_csr_file[i] + " " + Graphs_csr_tail_file[i] + " /Data/Temp/temp3.txt /Data/Temp/temp4.txt /Data/Temp/temp5.txt /Data/Temp/temp6.txt " + str(BFS_root[i]) + " 2000000000 " + str(BFS_iterations[k]) +  " >> " + Output_file)

##BFS -- END --

##Page rank -- BEGIN --
Run = [0,0]
PageRank_MaxIterations = 2

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/MultiLogVC/Applications/PageRank")
	os.system("echo " + Graphs_to_run[i] + " PageRank " + " >> " + Output_file)
	if(Run[i] == 1):
		os.system("sed -i 's/typedef .* VERTEX_VALUE_TYPE/typedef double VERTEX_VALUE_TYPE/g' ../../src/datatypes.h")
		os.system("sed -i 's/typedef .* EDGE_MESSAGE_TYPE/typedef double EDGE_MESSAGE_TYPE/g' ../../src/datatypes.h")
		os.system("rm PageRank;sh compile.sh; ./PageRank " + Graphs_csr_file[i] + " " + Graphs_csr_tail_file[i] + " /Data/Temp/temp3.txt /Data/Temp/temp4.txt /Data/Temp/temp5.txt /Data/Temp/temp6.txt " + str(PageRank_MaxIterations) + "  >> " + Output_file)

##Page rank -- END --

##Page rank threshold -- BEGIN --
Run = [0,0]
PageRank_MaxIterations = 15

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/MultiLogVC/Applications/PageRankThreshold")
	os.system("echo " + Graphs_to_run[i] + " PageRankThreshold " + " >> " + Output_file)
	if(Run[i] == 1):
		os.system("sed -i 's/typedef .* VERTEX_VALUE_TYPE/typedef PAGE_RANK_THRESHOLD_VALUE VERTEX_VALUE_TYPE/g' ../../src/datatypes.h")
		os.system("sed -i 's/typedef .* EDGE_MESSAGE_TYPE/typedef PAGERANK_VERTEX_VALUE_TYPE EDGE_MESSAGE_TYPE/g' ../../src/datatypes.h")
		os.system("rm PageRankThreshold;sh compile.sh; ./PageRankThreshold " + Graphs_csr_file[i] + " " + Graphs_csr_tail_file[i] + " /Data/Temp/temp3.txt /Data/Temp/temp4.txt /Data/Temp/temp5.txt /Data/Temp/temp6.txt " + str(PageRank_MaxIterations) + "  >> " + Output_file)
##Page rank threshold -- END --

##Page rank threshold async -- BEGIN --
Run = [1,1]
PageRank_MaxIterations = 15

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/MultiLogVC/Applications/PageRankThresholdAsync")
	os.system("echo " + Graphs_to_run[i] + " PageRankThresholdAsync " + " >> " + Output_file)
	if(Run[i] == 1):
		os.system("sed -i 's/typedef .* VERTEX_VALUE_TYPE/typedef PAGE_RANK_THRESHOLD_VALUE VERTEX_VALUE_TYPE/g' ../../src/datatypes.h")
		os.system("sed -i 's/typedef .* EDGE_MESSAGE_TYPE/typedef PAGERANK_VERTEX_VALUE_TYPE EDGE_MESSAGE_TYPE/g' ../../src/datatypes.h")
		os.system("rm PRTA;sh compile.sh; ./PRTA " + Graphs_csr_file[i] + " " + Graphs_csr_tail_file[i] + " /Data/Temp/temp3.txt /Data/Temp/temp4.txt /Data/Temp/temp5.txt /Data/Temp/temp6.txt " + str(PageRank_MaxIterations) + "  >> " + Output_file)
##Page rank threshold async -- END --


##FLP -- BEGIN --
Run = [0,0]
FLP_MaxIterations = 2

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/MultiLogVC/Applications/FLP")
	os.system("echo " + Graphs_to_run[i] + " FLP " + " >> " + Output_file)
	if(Run[i] == 1):
		os.system("sed -i 's/typedef .* VERTEX_VALUE_TYPE/typedef unsigned int VERTEX_VALUE_TYPE/g' ../../src/datatypes.h")
		os.system("sed -i 's/typedef .* EDGE_MESSAGE_TYPE/typedef unsigned int EDGE_MESSAGE_TYPE/g' ../../src/datatypes.h")
		os.system("rm FLP;sh compile.sh; ./FLP " + Graphs_csr_file[i] + " " + Graphs_csr_tail_file[i] + " /Data/Temp/temp3.txt /Data/Temp/temp4.txt /Data/Temp/temp5.txt /Data/Temp/temp6.txt " + str(FLP_MaxIterations) + "  >> " + Output_file)

##FLP -- END --

os.system("cgcreate -g memory,cpu:flpGroup")
os.system("cgset -r memory.limit_in_bytes=$((2*1024*1024*1024)) flpGroup")

##FLP ASYNC -- BEGIN --
Run = [1,1]
FLP_MaxIterations = 15
useCGgroup = 1
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/MultiLogVC/Applications/FLP_ASYNC")
	os.system("echo " + Graphs_to_run[i] + " FLP ASYNC " + " >> " + Output_file)
	if(Run[i] == 1):
		os.system("sed -i 's/typedef .* VERTEX_VALUE_TYPE/typedef unsigned int VERTEX_VALUE_TYPE/g' ../../src/datatypes.h")
		os.system("sed -i 's/typedef .* EDGE_MESSAGE_TYPE/typedef IN_EDGE_AND_WEIGHT_TYPE EDGE_MESSAGE_TYPE/g' ../../src/datatypes.h")
		if(useCGgroup == 0):
			os.system("rm FLPA;sh compile.sh; ./FLPA " + Graphs_csr_file[i] + " " + Graphs_csr_tail_file[i] + " /Data/Temp/temp3.txt /Data/Temp/temp4.txt /Data/Temp/temp5.txt /Data/Temp/temp6.txt " + str(FLP_MaxIterations) + "  /Data/Temp/temp7.txt >> " + Output_file)
		elif(useCGgroup == 1):
			os.system("rm FLPA;sh compile.sh;cgexec -g memory,cpu:flpGroup ./FLPA " + Graphs_csr_file[i] + " " + Graphs_csr_tail_file[i] + " /Data/Temp/temp3.txt /Data/Temp/temp4.txt /Data/Temp/temp5.txt /Data/Temp/temp6.txt " + str(FLP_MaxIterations) + "  /Data/Temp/temp7.txt >> " + Output_file)

##FLP ASYNC -- END --

