bfs_update()
{
	if(not visited) {mark as visited, keep the message id as parent} vertex_update
	if(changed in this iteration){activate} finalize, decide activation
	if(activated){send message to out-going neighbours with vertex id in it} send message, edge_program
}

class MultiLogVC
{
	void run()=0;
};

//declare other classes not in AssociativeMultiLogVC class so that they can be persistent across the supersteps

template <T>
struct IORequest {
	FILE *fileHandle;
	queue<T> *returnQueue;
	void *buffer;
};

template <T>
class Utils
{
	queue<T> returnQueue;
	AsyncIO_util *asynchronousIO;
	T currentOffset;
	shared_mutex lock;
	Utils(AsyncIO_util *asyncIO)
	{
		asynchronousIO = asyncIO;
		currentOffset = 0;
	}

	loadContiguousFileToBuffer(FILE *fileHandle, void *buffer, T loadLength)
	{
		//work with asynchronous IO and load vector from file
		//push all the required things to the IO queue
		unsigned int numberOfPages = ceil((loadLength*1.0) / SSD_PAGE_SIZE);
		//need to change this to request more data at a time
		for(unsigned int loopVar_i = 0; i < numberOfPages; i++)
		{
			asynchronousIO->submit_request(fileHandle, &returnQueue, buffer, currentOffset + i * SSD_PAGE_SIZE, loadLength, &lock);
		}
		currentOffset += numberOfPages * SSD_PAGE_SIZE;
		unsigned int numberOfPagesReturned = numberOfPages;
		unsigned int returnQueueSize;
		while(1)
		{
			lock.lock();
			if(returnQueue.size() == numberOfPages)
			{
				lock.unlock();
				break;
			}
			lock.unlock();
		}
	}
}

template <..., ...>
class Log_loader
{
	public:
		map<unsigned int, list<...> > partition_page_list;
	//should I keep this log loader persistent or construct every time? for every time one needs to do memory allocation again, if I make this persistent then how do I resize every time? 
	vector<...> buffer[2];
	vector<VertexDataType> VertexOutput;
	vector<bool> VertexActiveNess;
	FILE *vertexData_file;
	Utils *logLoader;
	Log_loader()
	{
		VertexOutput.resize(...);//need to align this properly, no need to use vector, same programmability can be achieved through aligned malloc allocation also
		vertexData_file.open();
		logLoader = ...;
		...;
		VertexActiveNess.resize(..., 0);
	}
		calculate_partition_space()
		{
			//is this the right place to calculate the partition space?
			//one needs to caculate partition space of vertex and log messages every time we load
			//actually in associative function one, we don't need to calculate this
		}
		reset_parititon_info()
		{
			currentPartitionNumber = ...;
		}
		store_vertex_output()
		{
		}
		load_vertex_data()
		{
			loadContiguousFileToBuffer(...);
		}
		load_next_parition()
		{
			while((space available in buffer) && (there are pages to be loaded))
			{
				//keep all the required pages into a vector and call the IO function
				//how to indicate that we loaded the buffer? keep a bit for every buffer, start with first buffer, then alternate
				//keep number of data elements in the buffer also
			}
		}
}

Graph_data loader class
{
	public:
		map<unsigned int, list<...> > partition_page_list;
	//should I keep this log loader persistent or construct every time? for every time one needs to do memory allocation again, if I make this persistent then how do I resize every time? 
	vector<...> buffer[2];
	vector<VertexDataType> VertexOutput;
	vector<bool> VertexActiveNess;
	FILE *vertexData_file;
	Utils *logLoader;
	Log_loader()
	{
		VertexOutput.resize(...);//need to align this properly, no need to use vector, same programmability can be achieved through aligned malloc allocation also
		vertexData_file.open();
		logLoader = ...;
		...;
		VertexActiveNess.resize(..., 0);
	}
	//from where will it get the active list? you can get that from log buffer, how to process the active list?
	load_next_list()
	{
		//first need to load row pointer thing into memory adress
		//bring row pointer one in to main memory
		//initialize buffer top, which buffer and so on
		for(unsigned int loop_1; loop_1 < activeVertexSize; loop_1 += chunkSize)
		{
			loadContiguousFileToBuffer(...);

			for(unsigned int loop_2; loop_2 < chunkSize; loop_2++)
			{
				//current page, buffer space available
				//can this vertex fit in the buffer
					//number of pages, required by the vertex
					//if(currentVertexPage is equal to currentPage) then number of pages -= 1
				//if number of pages is 0, then continue
				//if there is space in the buffer, fetch them, continue
				//if there is not enough space in the buffer, indicate buffer finished, wait for newer buffer, fetch them
			}
		}
		//if there is data in the buffer, indicate buffer finished
	}
}

template<class T>
cache_buffer class
{
	vector< <CPN, pageTopPointer> > vertexToPartitionPage;
	send_message(message, destination)
	{
		DataWritten = 0;
		//get partition top page
		lock();
		while(dataToWrite > 0)
		{
			AvailableSpace = SSD_PAGE_SIZE - vertexToPartitionPage[destination].pageTopPointer;
			DataToWrite = AvailableSpace > (message.size - DataWritten) ? message.size : AvailableSpace;
			memcpy(vertexToPartitionPage[destination].CPN, message + dataWritten, DataToWrite);
			AvailableSpace -= DataToWrite;
			DataWritten += DataToWrite;
			if(AvailableSpace == 0)
			{
				//request new page, how to maintain the free page list? maintain it as a double linked list? add, free pages at one end, remove free pages at the other end, if there are no pages, then wait for free, page, after IO has freed the page, it will add it to the free page,
				//how can I get this free page design? double linked list, it is easy to get, it is easy to get double linked list, get from GraphSSD project?
				//declare page array, page lock array
				//vertexToPartitionPage[destination].CPN = new page
				//vertexToPartitionPage[destination].pageTopPointer = 0;
				//write the older data to the storage
			}
			else {
				vertexToPartitionPage[destination].pageTopPointer += DataToWrite;
			}
		}
		unlock();
		//what should we lock? instead, how about using a static function for mapping from vertex to partition, each partition can have a lock, so there will be distributed locking and only updates which go to same page, contend for locking, this good, because 1) each thing that reads a page in partition will modify the location.
		//if more space is required, get a new page, write the older page to storage
		//keep all the partition pages list in memory, you might need to keep 2 lists for each of the alternating partition
	}
	//Function to add an element to the log, this can be used for adding graph modifications also
	//number of pages, partitions, data structure for keeping track of partition pages, threshold rate after which one has to evict the pages, file to which one has to evict
	//map from partition to page top
	//how to deal with the pages that are there in the cache? how about maintaining if it is in cache or storage? maintain this in the storage, while getting a new page for partition, indicate that this page is in the cache, if it is written to storage then indicate that this has been written to stoage
}

class AssocativeMultiLogVC : public MultiLogVC
{
	Log_loader *log_buffer;
	Graph_data *graph_data;
	AssocativeMultiLogVC():MultiLogVC()
	{
		log_buffer = ...;
		graph_data = ...;
	}

	//how to keep log loader?
	void run()
	{
		while(finished())
		{
			//iniitalize for every iteration
			before_iteration();
			//how did they manage io? they managed it using separate class for other components
			//our processing logic
			load_vertex_data();
			//run for all the partitions
#pragma omp parallel//	create threads
			{
				unsigned int tid = omp_thread_num();
				unsigned int maxElementsInBuffer = log_buffer->max_elements_in_buffer();
				unsigned int numOfPartitions = log_buffer->num_of_partitions();
				unsigned int numOfPartitions_processed = 0;
				while(numOfPartitions_processed++ <= numOfPartitions) {//all the partitions are done
					if(tid == 0)
					{
						log_buffer->load_next_parition();
					}
					else {
						until() {//all the buffers are done
							while(buffer not ready()){}
							unsigned int numberOfElementsInBuffer = ...;//given by the load_next_partition
							T startingVertexIndexOfBuffer = ...;//given by the load_next_partition
#pragma omp parallel for
							for(unsigned int loop_1 = 0; loop_1 < numberOfElementsInBuffer; loop_1++)
							{
								//threads apply the function
								update(startingVertexIndex + loop_1, &buffer_location/*i.e. update location*/, &vertex_data_location);
							}
							if(numberOfElementsInBuffer < maxElementsInBuffer)
							{break;}
						}
					}
#pragma barrier
					T startingVertexIndexOfPartition = ...;
#pragma omp parallel for
					for(unsigned int loop_1 = 0; loop_1 < ...; loop_1++)
					{
						is_active(&(log_buffer->), &(log_buffer->), startingVertexIndexOfPartition + loop_1, loop_1);
					}
#pragma barrier
					if(tid == 0)
					{
						graph_data->load_next_list();
					}
					else {
						result_data = malloc(...);
						until() {//all the buffers are done
							while(buffer not ready()){}
							unsigned int numberOfElementsInBuffer = ...;//given by the load_next_list
							T  vector_index_list = ...;//given by the load_next_list
#pragma omp parallel for
							for(unsigned int loop_1 = 0; loop_1 < vector_index_list.size(); loop_1++)
							{
								//threads apply the function
								//might require edge weights also
								for(unsigned int loop_2 = vector_index_list[]; loop_2 < vector_index_list[]; loop_2++)
								{
									edge_program(&(graph_data->edge_weights), vector_index_list[].vertex_data_index, result_data);
									send_message(result_data, &(graph_data->edge_list));
								}
							}
							//switch to next buffer
							//how to check buffer?
							if((graph_data->done()) && (graph_data->buffer() != 1) )
							{break;}
						}
						free(result_data);
					}
				}
			}
			after_iteration();
		}
	}
}

void finished()
{

}
};

class BFS: public AssocativeMultiLogVC
{
	BFS()
	{
	}
	void update()
	{
	}
	void send_message()
	{
	}
	void is_active()
	{
	}
	void finalize()
	{
	}
};

int main(nt argc, char *argv[])
{
	MultiLogVC_init(argc, argv);
	BFS forwardTraversal(argv[], argv[], ...);
	forwardTraversal.run();
}

//An application might have several supersteps, 

//application - supersteps - function calls
//each phase will have some input, each superstep will have some input, each function will have some input - phase is not required as it is not common across many applications
//we can define the input properly
//after we define the input properly, it is easy to manage, this is good because with one change we can affect many
//each function call will also have something, 

// so our job now is to define the inputs properly, also what to output and what to clear after each input
//phase inputs - 
//superstep input - 
//superstep input - 
//do I need to do command checking, we can add that feature later, it might not be that important now, 
//in scatter gather it is just scatter and gather, it is difficult to build general applications over that, one needs a more general framework over that, for some we can show that edge-centric framework is good and for other we can argue qualitatively
//I think it is easy to convey this message to everyone, and there might not be much innovation in this area, so just try to finish this project and move on to something else or say done!! so go in the direction of finishing the project quickly
//should I use undirected graphs or directed graphs? use directed graphs, treat everything as directed, that is easier to handle
//you can add command parsing later
