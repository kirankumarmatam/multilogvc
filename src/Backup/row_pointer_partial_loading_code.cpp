#include "header.h"

extern std::queue<struct IORequest> cache_to_IO_request_queue;
extern mutex cache_to_IO_request_queue_lock;

class Utils//Util class is for loading from either graph data or partition file, basically it needs offset, file handle, and length
{
	AsyncIO_util *asynchronousIO;
	shared_mutex lock;
	unsigned int numberOfPagesReturned;
	Utils(Asynchronous_IO *asyncIO)
	{
		asynchronousIO = asyncIO;
		asynchronousIO->utility_object = this;
	}

	void loadContiguousFileToBuffer(int fileHandle, void *buffer, DATA_SIZE fileOffset, DATA_SIZE loadLength)
	{
		unsigned int numberOfPages = ceil((loadLength*1.0) / SSD_PAGE_SIZE);
		//need to change this to request more data at a time
		DATA_SIZE startingOffset = fileOffset;
		numberOfPagesReturned = 0;
		IORequest io_request;
		for(unsigned int loop_1 = 0; loop_1 < numberOfPages; loop_1++)
		{
			io_request.fileHandle = fileHandle;
			io_request.buffer = (void *)((char *)buffer + SSD_PAGE_SIZE * i);
			io_request.isRead = 1;
			io_request.returnReqType = 2;
			io_request.file_offset = startingOffset + SSD_PAGE_SIZE * i;
			io_request.length = SSD_PAGE_SIZE > (loadLength - SSD_PAGE_SIZE * i) ? (loadLength - SSD_PAGE_SIZE * i) : SSD_PAGE_SIZE;
			cache_to_IO_request_queue_lock.lock();
			cache_to_IO_request_queue.push(io_request);
			cache_to_IO_request_queue_lock.unlock();
		}
		unsigned int returnQueueSize;
		while(1)
		{
			lock.lock();
			if(numberOfPagesReturned == numberOfPages) { break; }
			lock.unlock();
		}
	}
	void handleReturnRequest(struct IORequest io_req)
	{
			lock.lock();
			numberOfPagesReturned++;
			lock.unlock();
	}
}

struct bufferIndexPtr {
	VERTEX_TYPE vertex_id;
	DATA_SIZE start_index;
	DATA_SIZE end_index;
};

class Graph_data_loader
{
	public:
		void *buffer[2];
		void *buffer_ready_bit[2];
		vector<mutex_wrapper> buffer_lock[2];
		list<struct bufferIndexPtr> vertex_index_list[2];//need to make sure we clear this also
		void *row_buffer;
		int vertexData_file_id;
		Utils *logLoader;
		DATA_SIZE vertexFileOffset;
		DATA_SIZE bufferTop;
		DATA_SIZE availableBufferSpace;
		DATA_SIZE rowBufferVertices;
		DATA_SIZE currentRowPointerFileOffset;
		DATA_SIZE rowBufferAvailableSpace;
		DATA_SIZE currentRowPointerBufferOffset;
		DATA_SIZE NumberOfVertices;

		Graph_data_loader(std::string file1, class Utils *util_ptr, DATA_SIZE rowPtr_buffer_size, DATA_SIZE colPtr_buffer_size)
		{
			for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
			{
				if(posix_memalign((void **)&(buffer[loop_1]), SSD_PAGE_SIZE, colPtr_buffer_size))
				{
					fprintf(stderr, "cannot allocate io payload for data_wr\n");
					exit(0);
				}
			}
			if(posix_memalign((void **)&(row_buffer), SSD_PAGE_SIZE, rowPtr_buffer_size))
			{
				fprintf(stderr, "cannot allocate io payload for data_wr\n");
				exit(0);
			}
			rowBufferVertices = rowPtr_buffer_size / sizeof(DATA_SIZE);
			currentRowPointerBufferOffset = 0;
			rowBufferAvailableSpace = rowPtr_buffer_size;
			currentRowPointerFileOffset = -1;
			NumberOfVertices = ...;

			vertexData_file_id = open(file1.data(), O_RDONLY | O_DIRECT);
			if (vertexData_file_id == -1) {
				perror(file1.data());
				exit(1);
			}
			logLoader = util_ptr;
			vertexFileOffset = -1;
			bufferTop = 0;
			availableBufferSpace = colPtr_buffer_size;
		}
		~Graph_data_loader()
		{
			close(vertexData_file_id);
			for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
			{
				free(buffer[loop_1]);
			}
			free(row_buffer);
		}
		void load_next_list(bool *vertex_active_vector, VERTEX_TYPE partition_start_vertex, PARTITION_SIZE activeVertexSize)
		{
			bool buffer_modified = 0;
			DATA_SIZE startingIndexInRowBuffer, lengthInRowBuffer;
			DATA_SIZE rowPtr_load_start_index, rowPtr_load_length;
			VERTEX_TYPE loop_start_row_vertex;
			... numberOfRequiredPages;
			for(unsigned int loop_1 = 0; loop_1 < activeVertexSize; )
			{
				loop_start_row_vertex = partition_start_vertex + loop_1;
				VERTEX_TYPE verticesRequired = activeVertexSize - loop_1;
				numberOfRequiredPages = ceil(((loop_start_row_vertex + verticesRequired)*1.0) / (SSD_PAGE_SIZE/sizeof(DATA_SIZE)));
				fileOffset = ((loop_start_row_index*sizeof(DATA_SIZE)) / SSD_PAGE_SIZE) * SSD_PAGE_SIZE;
				if(fileOffset == currentRowPtrFileOffset)
				{
					startingIndexInRowBuffer = ( loop_start_row_vertex * sizeof(DATA_SIZE)+ rowPtr_buffer_size - SSD_PAGE_SIZE) % SSD_PAGE_SIZE;
					remainingVerticesInPage = (SSD_PAGE_SIZE / sizeof(DATA_SIZE)) - (loop_start_row_vertex % (SSD_PAGE_SIZE / sizeof(DATA_SIZE)));
					lengthInRowBufferVertices = remainingVerticesInPage < (activeVertexSize - loop_1) ? remainingVerticesInPage : (activeVertexSize - loop_1);
					loop_1 += lengthInRowBufferVertices;

					verticesRequired = activeVertexSize - loop_1;
					if((verticesRequired > 0) && (currentRowPointerBufferOffset != 0))
					{
						rowPtr_load_start_index = currentRowPointerFileOffset;
						numberOfRequiredPages -= 1;
						loop_start_row_vertex = partition_start_vertex + loop_1;
						fileOffset = ((loop_start_row_index*sizeof(DATA_SIZE)) / SSD_PAGE_SIZE) * SSD_PAGE_SIZE;
						rowPtr_load_length = (numberOfRequiredPages < rowBufferAvailableSpace) ? numberOfRequiredPages : rowBufferAvailableSpace;
						loadContiguousFileToBuffer(vertexData_file_id, rowPtr_load_start_index, fileOffset, rowPtr_load_length);
						verticesLoaded = rowPtr_load_length / sizeof(DATA_SIZE);
						lengthInRowBufferVertices += verticesLoaded < (activeVertexSize - loop_1) > verticesLoaded : (activeVertexSize - loop_1);
						loop_1 += lengthInRowBufferVertices;
						currentRowPointerFileOffset = fileOffset + rowPtr_load_length - SSD_PAGE_SIZE;
						rowBufferAvailableSpace -= rowPtr_load_length;
						if(rowBufferAvailableSpace == 0)
						{
							currentRowPointerBufferOffset = 0;
							rowBufferAvailableSpace = rowPtr_buffer_size;
						}
						else {
							currentRowPointerBufferOffset += rowPtr_load_length;
						}
					}
				}
				else
				{
					VERTEX_TYPE verticesRequired = activeVertexSize - loop_1;
					numberOfRequiredPages = ceil((((loop_start_row_vertex % (SSD_PAGE_SIZE / sizeof(DATA_SIZE)))  + verticesRequired)*1.0) / (SSD_PAGE_SIZE/sizeof(DATA_SIZE)));
					fileOffset = ((loop_start_row_index*sizeof(DATA_SIZE)) / SSD_PAGE_SIZE) * SSD_PAGE_SIZE;
					rowPtr_load_start_index = currentRowPointerFileOffset;
					rowPtr_load_length = (numberOfRequiredPages < rowBufferAvailableSpace) ? numberOfRequiredPages : rowBufferAvailableSpace;
					loadContiguousFileToBuffer(vertexData_file_id, rowPtr_load_start_index, fileOffset, rowPtr_load_length);
					startingIndexInRowBuffer = ((loop_start_row_vertex * sizeof(DATA_SIZE)) % SSD_PAGE_SIZE);
					verticesLoaded = rowPtr_load_length / sizeof(DATA_SIZE);
					verticesLoaded = verticesLoaded - (loop_start_row_vertex % (SSD_PAGE_SIZE / sizeof(DATA_SIZE)));
					lengthInRowBufferVertices = verticesLoaded < (activeVertexSize - loop_1) > verticesLoaded : (activeVertexSize - loop_1);
					loop_1 += lengthInRowBufferVertices;
					currentRowPointerFileOffset = fileOffset + rowPtr_load_length - SSD_PAGE_SIZE;
					rowBufferAvailableSpace -= rowPtr_load_length;
					if(rowBufferAvailableSpace == 0)
					{
						currentRowPointerBufferOffset = 0;
						rowBufferAvailableSpace = rowPtr_buffer_size;
					}
					else {
						currentRowPointerBufferOffset += rowPtr_load_length;
					}
				}
				//need to fix lengthInRowBufferVertices
				//need to make sure + 1 for rowptr is taken care off
				for(unsigned int loop_2 = 0; loop_2 < lengthInRowBufferVertices; loop_2++)
				{
					partition_index = loop_2 - partition_start_vertex;
					if(vertex_active_vector[partition_index] == 1)
					{
						startIndexInRowPtrBuffer = ;
						vertexColPtrSize = ((DATA_SIZE *)row_buffer)[startIndexInRowPtrBuffer + loop_2+1] - ((DATA_SIZE *)row_buffer)[startIndexInRowPtrBuffer + loop_2];//Leaving it here....
						numberOfRequiredPages = ceil(((vertexColPtrSize)*1.0) / SSD_PAGE_SIZE);
						currentColPtrFileOffset = (row_buffer[loop_2] / SSD_PAGE_SIZE) * SSD_PAGE_SIZE;
						if(vertexFileOffset == currentColPtrFileOffset) numberOfRequiredPages -= 1;
						if(numberOfRequiredPages == 0)
							continue;
						if(availableBufferSpace > numberOfRequiredPages)
						{
							loadContiguousFileToBuffer(vertexData_file_id, buffer + bufferTop, currentColPtrFileOffset, numberOfRequiredPages * SSD_PAGE_SIZE);
							buffer_modified = 1;
						}
						else {
							while(1)
							{
								if(buffer_lock)
								{
									buffer_ready_bit[current_buffer] = 1;
									buffer_unlock;
									break;
								}
								buffer_unlock;
							}
							current_buffer = (current_buffer + 1) % 2;
							while(1)
							{
								if(buffer_lock)
								{
									if(buffer_ready_bit[current_buffer] == 0)
									{
										buffer_unlock;
										break;
									}
								}
								buffer_unlock;
							}
							loadContiguousFileToBuffer(vertexData_file_id, buffer + bufferTop, currentColPtrFileOffset, numberOfRequiredPages * SSD_PAGE_SIZE);
							buffer_modified = 1;
							bufferTop += numberOfRequiredPages * SSD_PAGE_SIZE;
							availableBufferSpace -= (numberOfRequiredPages * SSD_PAGE_SIZE);
							vertexFileOffset = currentColPtrFileOffset + (numberOfRequiredPages-1) * SSD_PAGE_SIZE;
						}
						//need to prepare the vertex_index_list
					}
				}
			}
			if(buffer_modified == 1)//there might be cases, where even without the buffer being modified, it might need to be sent, should we just send if there was an active vertex?
			{
				while(1)
				{
					if(buffer_lock)
					{
						buffer_ready_bit[current_buffer] = 1;
						buffer_unlock;
						break;
					}
					buffer_unlock;
				}
			}
			done_lock();
			done=1;
			done_unlock();
		}
}

class AssocativeMultiLogVC
{
	Graph_data *graph_data;
	AssocativeMultiLogVC()
	{
		graph_data = ...;
	}

	void run()
	{
#pragma omp parallel//	create threads
		{
			unsigned int tid = omp_thread_num();
			unsigned int maxElementsInBuffer = ;
			unsigned int numOfPartitions = ;
			unsigned int numOfPartitions_processed = 0;
			while(numOfPartitions_processed++ <= numOfPartitions) {//all the partitions are done
				T startingVertexIndexOfPartition = numOfPartitions_processed * maxElementsInBuffer;
#pragma barrier
				if(tid == 0)
				{
					graph_data->load_next_list();
				}
				else {
					bool exit;
					while(1) {//all the buffers are done
						exit = 0;
						while(1){
							((graph_data->buffer)[current_buffer]).lock();
							if((graph_data->buffer_read_bit)[current_buffer] == 1)
							{
								((graph_data->buffer)[current_buffer]).unlock();
								break;
							}
							else {
								graph_data->done_lock();
								if(graph_data->done == 1) {
									exit = 1;
								}
								graph_data->done_unlock();
							}
							((graph_data->buffer)[current_buffer]).unlock();
							if(exit == 1) break;
						}
						if(exit == 1) break;
						unsigned int numberOfElementsInBuffer = (graph_data->buffer_elements[current_buffer]);
						list<T>::iterator vertex_index_list = (graph_data->vertex_index_list)[current_buffer];
#pragma omp parallel for
						for(list<T>::iterator vertex_index_list = (graph_data->vertex_index_list)[current_buffer].begin(); vertex_index_list != (graph_data->vertex_index_list)[current_buffer].end(); vertex_index_list++)
						{
							cout << vertex_index_list.vertex_id << ":";
							for(unsigned int loop_2 = vector_index_list.start_index; loop_2 < vector_index_list.end_index; loop_2++)
							{
								cout << " " << ((graph_data->buffer)[current_buffer])[loop_2];
							}
							cout << endl;
						}
						current_buffer = (current_buffer + 1) % 2;
					}
				}
			}
		}
	}
};

int main(int argc, char *argv[])
{
	std::string csr_file(argv[1]);

	Asynchronous_IO asyncIO;
	Utils utility_object(&asyncIO);
	DATA_SIZE rowPtr_buffer_size = 64 * SSD_PAGE_SIZE, colPtr_buffer_size = 64 * SSD_PAGE_SIZE;
	Graph_data_loader graph_loader(csr_file, &utility_object, rowPtr_buffer_size, colPtr_buffer_size);
	AssocativeMultiLogVC associateVC(&graph_loader);
}
