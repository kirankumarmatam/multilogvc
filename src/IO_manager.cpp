#include "header.h"

std::queue<struct IORequest> cache_to_IO_request_queue;
mutex cache_to_IO_request_queue_lock;
extern vector<ofstream> IO_output_file;

vector< map<unsigned int, struct IORequest> > id_to_io_request_map;

extern unsigned int NumOfIOThreads;

static inline int
io_setup(unsigned maxevents, aio_context_t *ctx) {
	    return syscall(SYS_io_setup, maxevents, ctx);
}

static inline int
io_submit(aio_context_t ctx, long nr, struct iocb **iocbpp) {
	    return syscall(SYS_io_submit, ctx, nr, iocbpp);
}

static inline int
io_getevents(aio_context_t ctx, long min_nr, long nr,
		                 struct io_event *events, struct timespec *timeout) {
	    return syscall(SYS_io_getevents, ctx, min_nr, nr, events, timeout);
}

void Asynchronous_IO::Initialize()
{
	id_to_io_request_map.resize(NumOfIOThreads);
#if(DEBUG_CODE == 1)
	cout << "B idMap s = " << id_to_io_request_map.size()  << endl;
#endif//DEBUG_CODE
}

void Asynchronous_IO::IO_manager()
{
	printf("IO manager: entering here\n");
#pragma omp parallel num_threads(NumOfIOThreads)
	{
		IO_TID_TYPE tid = omp_get_thread_num();
		IO_manager_async_tid(tid);
	}
}

void Asynchronous_IO::IO_manager_async_tid(IO_TID_TYPE tid)
{
	unsigned int io_currentEvents = 0;
	size_t nevents = 1;
	struct io_event events[nevents];
	struct timespec waitingTime;
	waitingTime.tv_sec = 0;
	waitingTime.tv_nsec = 0;
	aio_context_t ioctx;
	ioctx = 0;
	io_maxevents = 128;

	if (io_setup(io_maxevents, &ioctx) < 0) {
		perror("io_setup");
		exit(1);
	}

	unsigned int token = 0;

	while(1)
	{
		if(io_currentEvents < io_maxevents)
		{
			cache_to_IO_request_queue_lock.lock();
			if(!cache_to_IO_request_queue.empty())
			{
				IORequest io_req = cache_to_IO_request_queue.front();
				cache_to_IO_request_queue.pop();
				cache_to_IO_request_queue_lock.unlock();
				AccessSSDN(io_req.file_offset, io_req.length, io_req.buffer, io_req.isRead, io_req.fileHandle, token, tid, ioctx);
#if(DEBUG_CODE == 1)
				IO_output_file[tid] << "IO submitting p = " << io_req.file_offset/SSD_PAGE_SIZE << " t = " << token << endl;
#endif//DEBUG_CODE
				id_to_io_request_map[tid][token] = io_req;
				token = (token + 1) % 100000000;
				io_currentEvents++;
			}
			else
			{
				cache_to_IO_request_queue_lock.unlock();
			}
		}
		//Perform this step only if io_currentEvents > 0
		int ret;
		if(io_currentEvents > 0)
		{
			ret = io_getevents(ioctx, 1 /* min */, 1, events, &waitingTime);
		} else {
			continue;
		}
//		int ret = io_getevents(ioctx, 1 /* min */, nevents, events, NULL);
//		cout << "ret = " << ret << endl;
		if (ret < 0) {
			perror("io_getevents");
			exit(1);
		}

		for (size_t i=0; i<ret; i++) {
			struct io_event *ev = &events[i];
			//            assert(ev->data == 0xbeef || ev->data == 0xbaba);
			IORequest io_req = id_to_io_request_map[tid][ev->data];
			id_to_io_request_map[tid].erase(ev->data);
#if(DEBUG_CODE == 1)
			IO_output_file[tid] << "IO received " << ev->data << " " << io_req.file_offset / SSD_PAGE_SIZE << endl;
/*			for(VERTEX_TYPE loop_1 = 0; loop_1 < (SSD_PAGE_SIZE/sizeof(DATA_SIZE)); loop_1++)
			{
				IO_output_file[0] << loop_1 << " = " << ((DATA_SIZE *)(io_req.buffer))[loop_1] << "; ";
			}
			IO_output_file[0] << endl;
*/
#endif//DEBUG_CODE
			if(io_req.returnReqType == 1) multi_log_cache->handleReturnRequest(io_req);
			else if(io_req.returnReqType == 2) utility_object->handleReturnRequest(io_req);
			else if(io_req.returnReqType == 3) utility_object->handleReturnRequest2(io_req);
#if(STRUCTURE_UPDATE == 1)
			else if(io_req.returnReqType == 4) structure_multi_log_cache->handleReturnRequest(io_req);
#endif//STRUCTURE_UPDATE
#if(EDGE_LOG_PROCESSING == 1)
			else if(io_req.returnReqType == 5) edge_log_multi_log_cache->handleReturnRequest(io_req);
#endif//EDGE_LOG_PROCESSING
			//make multi_log_cache self aware of the structural upate
			io_currentEvents--;
		}
	}
}

void Asynchronous_IO::AccessSSDN(FILE_OFFSET_TYPE file_offset, DATA_SIZE length, void *buffer, bool isRead, int myFlashFile, unsigned int data, IO_TID_TYPE tid, aio_context_t &ioctx)
{
#if(DEBUG_CODE == 1)
	IO_output_file[tid] << "fo = " << file_offset << " l = " << length << " iR = " << isRead << " mFF = " << myFlashFile << " d = " << data << " t = " << tid <<  endl;
#endif//DEBUG_CODE
/*#if(DEBUG_CODE == 1)
	read(myFlashFile, buffer, length);
	IO_output_file[tid] << "read syscall begin" << endl;
	for(VERTEX_TYPE loop_1 = 0; loop_1 < (length/sizeof(VERTEX_TYPE)); loop_1++)
	{
		IO_output_file[0] << loop_1 << " = " << ((VERTEX_TYPE *)buffer)[loop_1] << "; ";
	}
	IO_output_file[tid] << "read syscall end" << endl;
#endif//DEBUG_CODE*/
	struct iocb iocb1;// = {0};//do we need to allocate this?
	memset(&iocb1, 0, sizeof(iocb1));
	iocb1.aio_data       = data;
	iocb1.aio_fildes     = myFlashFile;
	if(isRead == 1)
		iocb1.aio_lio_opcode = IOCB_CMD_PREAD;
	else if(isRead == 0)
		iocb1.aio_lio_opcode = IOCB_CMD_PWRITE;
	iocb1.aio_reqprio    = 0;
	iocb1.aio_buf        = (uint64_t)buffer;
	iocb1.aio_nbytes     = length;
	iocb1.aio_offset     = file_offset;

	struct iocb *iocb_ptrs[1] = { &iocb1 };
	// submit operations
	int ret = io_submit(ioctx, 1, iocb_ptrs);
	if (ret < 0) {
		perror("io_submit");
		exit(1);
	} else if (ret != 1) {
		perror("io_submit: unhandled partial success");
		exit(1);
	}
}
