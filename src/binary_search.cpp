#include "header.h"

unsigned int Multi_log_cache::binarySearch(std::vector<VERTEX_TYPE> & partition_starting_vertex, VERTEX_TYPE l, VERTEX_TYPE r, VERTEX_TYPE x, VERTEX_TYPE *index, VERTEX_TYPE *number)
{
        while (l < r)
        {
                int64_t m = l + (r-l)/2;

                if (partition_starting_vertex[m] == x)
                {
                        VERTEX_TYPE start = m;
                        m = m-1;
                        while((m >= 0) && (partition_starting_vertex[m] == x))
                        {
                                start = m;
                                m = m-1;
                        }
                        m = start;
                        VERTEX_TYPE count=0;
                        while(((VERTEX_TYPE)m <= r) && (partition_starting_vertex[m] == x))
                        {
                                count++;
                                m++;
                        }
                        *index = start;
                        *number = count;
                        return 0;
                }
                else if (partition_starting_vertex[m] < x) 
                {
                        if(x < partition_starting_vertex[m+1])
                        {
                                *index = m;
                                *number = 1;
                                return 0;
                        }
                        else if(x > partition_starting_vertex[m+1])
                        {
                                l = m+1;
                        }
                        else if(x == partition_starting_vertex[m+1])
                        {
                                VERTEX_TYPE start = m+1;
                                m = start;
                                VERTEX_TYPE count = 0;
                                while(((VERTEX_TYPE)m <= r) && (partition_starting_vertex[m] == x))
                                {
                                        count++;
                                        m++;
                                }
                                *index = start;
                                *number = count;
                                return 0;
                        }
                }
                else if(partition_starting_vertex[m] > x) 
                {
                        r = m;
                }
        }
        if(l == r)
        {
                if(partition_starting_vertex[l] <= x)
                {
                        *index = l;
                        *number = 1;
                        return 0;
                }
                else {
                        return 1;
                }
        }

        return 1;
}

unsigned int Multi_log_cache::binarySearch(VERTEX_TYPE * partition_starting_vertex, VERTEX_TYPE l, VERTEX_TYPE r, VERTEX_TYPE x, VERTEX_TYPE *index, VERTEX_TYPE *number)
{
        while (l < r)
        {
                int64_t m = l + (r-l)/2;

                if (partition_starting_vertex[m] == x)
                {
                        VERTEX_TYPE start = m;
                        m = m-1;
                        while((m >= 0) && (partition_starting_vertex[m] == x))
                        {
                                start = m;
                                m = m-1;
                        }
                        m = start;
                        VERTEX_TYPE count=0;
                        while(((VERTEX_TYPE)m <= r) && (partition_starting_vertex[m] == x))
                        {
                                count++;
                                m++;
                        }
                        *index = start;
                        *number = count;
                        return 0;
                }
                else if (partition_starting_vertex[m] < x) 
                {
                        if(x < partition_starting_vertex[m+1])
                        {
                                *index = m;
                                *number = 1;
                                return 0;
                        }
                        else if(x > partition_starting_vertex[m+1])
                        {
                                l = m+1;
                        }
                        else if(x == partition_starting_vertex[m+1])
                        {
                                VERTEX_TYPE start = m+1;
                                m = start;
                                VERTEX_TYPE count = 0;
                                while(((VERTEX_TYPE)m <= r) && (partition_starting_vertex[m] == x))
                                {
                                        count++;
                                        m++;
                                }
                                *index = start;
                                *number = count;
                                return 0;
                        }
                }
                else if(partition_starting_vertex[m] > x) 
                {
                        r = m;
                }
        }
        if(l == r)
        {
                if(partition_starting_vertex[l] <= x)
                {
                        *index = l;
                        *number = 1;
                        return 0;
                }
                else {
                        return 1;
                }
        }

        return 1;
}
