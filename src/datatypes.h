#include "testing.h"
typedef float PAGERANK_VERTEX_VALUE_TYPE;
struct PAGE_RANK_THRESHOLD_VALUE {
	PAGERANK_VERTEX_VALUE_TYPE change;
	PAGERANK_VERTEX_VALUE_TYPE value;
};
typedef unsigned int PARTITION_NUM_TYPE;
typedef unsigned int PAGE_OFFSET_TYPE;
typedef char * LOG_MESSAGE_PTR_TYPE;
typedef unsigned int LOG_MESSAGE_SIZE_TYPE;
typedef unsigned int VERTEX_TYPE;
typedef unsigned int CPN_TYPE;
typedef uint64_t CACHE_SIZE;
const unsigned int SSD_PAGE_SIZE = 16384;
typedef uint64_t FILE_OFFSET_TYPE;
typedef unsigned int PAGE_NUM_TYPE;
typedef unsigned int IO_TID_TYPE;
typedef unsigned int PARTITION_SIZE;
typedef uint64_t DATA_SIZE;
typedef unsigned int BUFFER_SIZE;
typedef unsigned int EDGE_WEIGHT_TYPE;
struct GraphColorVertex
{
	bool IsColored;
	unsigned char type;
	VERTEX_TYPE degree;
#if(PRINTRESULT==1)
	T color;
#endif
};
#if(ASYNCHRONOUS_PROGRAMMING == 1)
#if(PROCESS_EDGE_WEIGHT == 1)
struct IN_EDGE_AND_WEIGHT_FILE_TYPE {
	EDGE_WEIGHT_TYPE data;
};
struct IN_EDGE_AND_WEIGHT_TYPE {
	EDGE_WEIGHT_TYPE data;
	VERTEX_TYPE source;
	VERTEX_TYPE temp;
};
#endif//PROCESS_EDGE_WEIGHT
#endif//ASYNCHRONOUS_PROGRAMMING
//typedef unsigned int VERTEX_VALUE_TYPE;
//typedef IN_EDGE_AND_WEIGHT_TYPE EDGE_MESSAGE_TYPE;
//typedef unsigned int VERTEX_VALUE_TYPE;
//typedef IN_EDGE_AND_WEIGHT_TYPE EDGE_MESSAGE_TYPE;
typedef unsigned int VERTEX_VALUE_TYPE;
typedef IN_EDGE_AND_WEIGHT_TYPE EDGE_MESSAGE_TYPE;
typedef std::vector<EDGE_MESSAGE_TYPE> EDGE_MESSAGES_TYPE;
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
typedef EDGE_MESSAGE_TYPE * MESSAGE_LIST;
typedef VERTEX_TYPE * EDGE_LIST;
#else//EDGE_PROGRAM_OPTIMIZATION
typedef std::vector<EDGE_MESSAGE_TYPE> MESSAGE_LIST;
typedef std::vector<VERTEX_TYPE> EDGE_LIST;
#endif//EDGE_PROGRAM_OPTIMIZATION
typedef unsigned int ITERATION_NUM_TYPE;

#if(ASYNCHRONOUS_PROGRAMMING == 1)
typedef struct UPDATE_MESSAGE * UPDATE_MESSAGE_LIST;
struct LIST_UPDATE_MESSAGE {
	EDGE_MESSAGE_TYPE data;
	BUFFER_SIZE pointer;
};
#endif//ASYNCHRONOUS_PROGRAMMING


struct CONFIGURATION {
	BUFFER_SIZE graph_loader_rowPtr_buffer_size;
	BUFFER_SIZE graph_loader_colInd_buffer_size;
	BUFFER_SIZE associateVC_partition_vertices_vector_size;
	BUFFER_SIZE associateVC_partition_buffer_size;
	BUFFER_SIZE multiLogVC_partition_buffer_size;
	CACHE_SIZE logCacheSize;
	DATA_SIZE totalSize;
#if(ASYNCHRONOUS_PROGRAMMING == 1)
	BUFFER_SIZE multiLogVC_messages_list_buffer_size;
	FILE_OFFSET_TYPE max_message_file_top;
#endif//ASYNCHRONOUS_PROGRAMMING
#if(ASYNCHRONOUS_PROGRAMMING == 1 || STRUCTURE_UPDATE == 1)
	BUFFER_SIZE multiLogVC_vertex_to_message_list_ptr_size;
#endif//ASYNCHRONOUS_PROGRAMMING || STRUCTURE_UPDATE
#if(STRUCTURE_UPDATE == 1)
	CACHE_SIZE structure_logCacheSize;
#endif//STRUCTURE_UPDATE
};

struct UPDATE_MESSAGE {
	VERTEX_TYPE target;
	EDGE_MESSAGE_TYPE data;
};

#if(STRUCTURE_UPDATE == 1)
struct STRUCTURE_EDGE_MESSAGE_TYPE {
	VERTEX_TYPE target_id;
	unsigned int operation;
	VERTEX_TYPE source_id;
};
struct STRUCTURE_UPDATE_MESSAGE {
	VERTEX_TYPE target;
	STRUCTURE_EDGE_MESSAGE_TYPE data;
};
#endif//STRUCTURE_UPDATE

struct IORequest {
	int fileHandle;
	CPN_TYPE cpn;
	void *buffer;
	bool isRead;
	int returnReqType;
	FILE_OFFSET_TYPE file_offset;
	DATA_SIZE length;
	list<struct page_info>::iterator page_info_ptr;
};

struct page_info {
	public:
		FILE_OFFSET_TYPE file_offset;
		CPN_TYPE cpn;
		bool isCached;
};

struct partition_cache_page_pointer {
	public:
		CPN_TYPE cpn;
		PAGE_OFFSET_TYPE pageTopPointer;
		list<struct page_info>::iterator partition_page_info_ptr;
};//should initialize  this to appropriate CPN

typedef struct HostCacheBlock
{
	        char data[SSD_PAGE_SIZE];
} CacheBlock;
typedef CacheBlock * LOG_CACHE_BLOCK_TYPE;

struct mutex_wrapper : std::mutex
{
	mutex_wrapper() = default;
	mutex_wrapper(mutex_wrapper const&) noexcept : std::mutex() {}
	bool operator==(mutex_wrapper const&other) noexcept { return this==&other; }
};

struct shared_mutex_wrapper : std::shared_mutex
{
	shared_mutex_wrapper() = default;
	shared_mutex_wrapper(shared_mutex_wrapper const&) noexcept : std::shared_mutex() {}
	bool operator==(shared_mutex_wrapper const&other) noexcept { return this==&other; }
};
