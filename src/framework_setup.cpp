#include "header.h"

struct CONFIGURATION configuration;

void Graph_info::Initialize(int argc, char *argv[])
{
	fstream csrHeader;
	csrHeader.open(argv[1]);
	csrHeader.read((char *)(&NumNodes), sizeof(VERTEX_TYPE));
	csrHeader.read((char *)(&NumEdges), sizeof(DATA_SIZE));
	csrHeader.close();
	csr_file_header = argv[1];
	cout <<"argv_1 = " << argv[1] << " Numnodes = " << NumNodes << " Numedges = " << NumEdges << endl;
	csr_file_content = argv[2];
	//shift this to context, even graph info is fine
	vertexValue_file_id = open(argv[5], O_RDWR | O_TRUNC);
	if (vertexValue_file_id == -1) {
		perror(argv[5]);
		exit(1);
	}
	cout << "Graph_info errno = " << errno << endl;
	vertexActive_file_id = open(argv[6], O_RDWR | O_TRUNC);//O_DIRECT should be used with asynchronous IO
	if(vertexActive_file_id == -1) {
		perror(argv[6]);
		exit(1);
	}
	cout << "Graph_info errno = " << errno << endl;
}

void MultiLogContext::Initialize(int argc, char *argv[], unsigned int program_type)
{
	graph_info.Initialize(argc, argv);
	current_iteration = 0;
#if(PAGE_RANK_THRESHOLD_DEBUG == 1)
	num_active_vertices = 0;
#endif//PAGE_RANK_THRESHOLD_DEBUG
	programType = program_type;
	configuration.graph_loader_rowPtr_buffer_size = 128 * 1024 * 8;
	configuration.graph_loader_colInd_buffer_size = 3*32 * 1024 * 1024;
	configuration.associateVC_partition_vertices_vector_size = 640 * 1024 * 1024;
	configuration.associateVC_partition_buffer_size = 128 * 1024 * 1024;
	configuration.multiLogVC_partition_buffer_size = 448 * 1024 * 1024;
#if(ASYNCHRONOUS_PROGRAMMING == 1)
	configuration.multiLogVC_messages_list_buffer_size = 448 * 1024 * 1024;
	configuration.max_message_file_top = (ceil((graph_info.NumEdges * sizeof(UPDATE_MESSAGE) * 3.0) / SSD_PAGE_SIZE) * (FILE_OFFSET_TYPE)SSD_PAGE_SIZE);//change if more number of messages are being generated
#endif//ASYNCHRONOUS_PROGRAMMING
#if(ASYNCHRONOUS_PROGRAMMING == 1 || STRUCTURE_UPDATE == 1)
	configuration.multiLogVC_vertex_to_message_list_ptr_size = 0;//will be updated later
#endif//ASYNCHRONOUS_PROGRAMMING == 1 || STRUCTURE_UPDATE == 1
	configuration.logCacheSize = 32 * 1024 * 1024;
	configuration.totalSize = 1024 * 1024 * 1024;
	totalMessages = 0;
#if(EDGE_LOG_PROCESSING == 1)
	edge_log_read = 0;
	edge_log_write = 0;
#endif//EDGE_LOG_PROCESSING
#if(STRUCTURE_UPDATE == 1)
	structure_delete_vertex_list = (bool *)calloc(graph_info.NumNodes, sizeof(bool));
#endif//STRUCTURE_UPDATE
}

MultiLogEngine::MultiLogEngine(int argc, char *argv[], unsigned int programType)
{
	asyncIO.Initialize();
	multi_log_context.Initialize(argc, argv, programType);
	omp_set_nested(100);
	cout << "NumNodes = " << multi_log_context.graph_info.NumNodes << " NumEdges = " << multi_log_context.graph_info.NumEdges << endl;
	utility_object.Initialize(&asyncIO);
#if(DEBUG_CODE == 1)
	cout << "Before graph data loader " << multi_log_context.graph_info.csr_file_content << endl;
#endif//DEBUG_CODE
	graph_loader.Initialize(multi_log_context.graph_info.csr_file_content, &utility_object, configuration.graph_loader_rowPtr_buffer_size, configuration.graph_loader_colInd_buffer_size, multi_log_context.graph_info.NumNodes);
#if(DEBUG_CODE == 1)
	cout << "Before associateVC Initialization" << endl;
#endif//DEBUG_CODE
	if(programType == 1)
	{
		associateVC.Initialize(&multi_log_context, &graph_loader, &utility_object, configuration.associateVC_partition_vertices_vector_size, multi_log_context.graph_info.NumNodes, configuration.associateVC_partition_buffer_size);
	}
	else if(programType == 2)
	{
#if(PROCESS_EDGE_WEIGHT == 1)
		graph_loader.edge_weights_filename = argv[8];
#endif//PROCESS_EDGE_WEIGHT
#if(STRUCTURE_UPDATE == 1)
		graph_loader.structure_delete_edge_filename = argv[11];
#endif//STRUCTURE_UPDATE
		multiLogVC.Initialize(&multi_log_context, &graph_loader, &utility_object, configuration.associateVC_partition_vertices_vector_size, multi_log_context.graph_info.NumNodes, configuration.multiLogVC_partition_buffer_size);
	}
#if(DEBUG_CODE == 1)
	cout << "Before multi_log_cache Initialization" << endl;
#endif//DEBUG_CODE
	std::string log_file1(argv[3]);
	std::string log_file2(argv[4]);

	CACHE_SIZE cache_size = configuration.logCacheSize;
	if(programType == 2)
	{
		cache_size = (CACHE_SIZE)SSD_PAGE_SIZE * multi_log_context.numOfPartitions * 3;
	}
	multi_log_cache.Initialize(&multi_log_context, log_file1, log_file2, multi_log_context.graph_info.NumNodes, multi_log_context.numOfPartitions, associateVC.maxElementsInBuffer, cache_size, &asyncIO, 0);
	multi_log_context.multi_log_cache = &multi_log_cache;
	if(programType == 1)
		associateVC.multi_log_cache = &multi_log_cache;
	else if(programType == 2)
		multiLogVC.multi_log_cache = &multi_log_cache;
	utility_object.multi_log_cache = &multi_log_cache;
#if(STRUCTURE_UPDATE == 1)
	std::string log_file3(argv[12]);
	std::string log_file4(argv[13]);
	CACHE_SIZE structure_cache_size = 0;

	if(programType == 2)
	{
		structure_cache_size = (CACHE_SIZE)SSD_PAGE_SIZE * multi_log_context.numOfPartitions * 3;
	}
	structure_multi_log_cache.Initialize(&multi_log_context, log_file3, log_file4, multi_log_context.graph_info.NumNodes, multi_log_context.numOfPartitions, associateVC.maxElementsInBuffer, structure_cache_size, &asyncIO, 1);
	multi_log_context.structure_multi_log_cache = &structure_multi_log_cache;
	if(programType == 2)//no structural changes for associative programs?
		multiLogVC.structure_multi_log_cache = &structure_multi_log_cache;//declare structure multi log cache here
	utility_object.structure_multi_log_cache = &structure_multi_log_cache;//why does utility needs to have structure multi_log_cache?
#endif//STRUCTURE_UPDATE
#if(EDGE_LOG_PROCESSING == 1)
	std::string log_file5(argv[9]);
	std::string log_file6(argv[10]);
	//how much buffer size should I allocate?

	CACHE_SIZE edge_log_cache_size;
	if(programType == 2)
	{
		edge_log_cache_size = (CACHE_SIZE)SSD_PAGE_SIZE * multi_log_context.numOfPartitions * 3;
	}
	edge_log_multi_log_cache.Initialize(&multi_log_context, log_file5, log_file6, multi_log_context.graph_info.NumNodes, multi_log_context.numOfPartitions, associateVC.maxElementsInBuffer, edge_log_cache_size, &asyncIO, 2);
	multi_log_context.edge_log_multi_log_cache = &edge_log_multi_log_cache;
	if(programType == 2)//no structural changes for associative programs?
		multiLogVC.edge_log_multi_log_cache = &edge_log_multi_log_cache;
	utility_object.edge_log_multi_log_cache = &edge_log_multi_log_cache;
#endif//EDGE_LOG_PROCESSING
}

void MultiLogEngine::run(ApplicationProgram *Program, unsigned int programType, unsigned int applicationType)
{
	multi_log_context.program = Program;
	multi_log_context.application_type = applicationType;
	multi_log_context.app_timer.start_timer();
#pragma omp parallel num_threads(2)
	{
		int tid = omp_get_thread_num();
		if(tid == 0)
		{
			while(1)
			{
#if(DEBUG_CODE == 1)
				cout << "Before associateVC run " << multi_log_context.current_iteration << endl;
#endif//DEBUG_CODE
				if(programType == 1)
				{
					associateVC.run();
				}
				else if(programType == 2)
				{
					multiLogVC.run();
				}
				multi_log_context.current_iteration += 1;
#if(MEASURE == 1)
				std::string str;
				str = "after iterations ";
				str = str + std::to_string(multi_log_context.current_iteration);
				multi_log_context.app_timer.record_print_timer(str.c_str());
				cout << "iteration message count = " << multi_log_context.iterationMessages << endl;
				multi_log_context.totalMessages += multi_log_context.iterationMessages;
#endif//MEASURE
#if(DEBUG_CODE == 1)
				cout << "After associateVC run " << multi_log_context.current_iteration << endl;
#endif//DEBUG_CODE
#if(PAGE_RANK_THRESHOLD_DEBUG == 1)
				cout << "Active vertices count = " << multi_log_context.num_active_vertices << endl;
				multi_log_context.num_active_vertices = 0;
#endif//PAGE_RANK_THRESHOLD_DEBUG
#if(STATS_PAGE_IN_EFFICIENCY == 1)
				cout << "total_number_pages_accessed = " << utility_object.total_number_pages_accessed << endl;
				utility_object.total_number_pages_accessed = 0;
				cout << "total_number_edge_pages_accessed = " << utility_object.total_number_edge_pages_accessed << endl;
				utility_object.total_number_edge_pages_accessed = 0;
				cout << "total_number_edge_list_data_accessed = " << graph_loader.total_number_edge_list_size << endl;
				graph_loader.total_number_edge_list_size = 0;
				vector<int> bucket(11, 0);
				for(unordered_map<FILE_OFFSET_TYPE, int>::iterator it = graph_loader.total_data_accessed_in_a_page.begin(); it != graph_loader.total_data_accessed_in_a_page.end(); it++)
				{
					int index = ceil(((it->second * 1.0) / (SSD_PAGE_SIZE*1.0)) * 10.0);
					assert(index < 11 && index >= 0);
					bucket[index] += 1;
				}
				for(auto var:bucket)
				{
					cout << var << " ";
				}
				cout << endl;
				graph_loader.total_data_accessed_in_a_page.clear();
				graph_loader.total_vertices_accessed_in_a_page.clear();
#endif//STATS_PAGE_IN_EFFICIENCY
#if(STRUCTURE_UPDATE == 1)
				//write a function here
				//calculate how many edges to load here
				//load from the file and sort them and add it to the file
				//also check if a merge of file is necessary, if it is necessary then initiate one
				//for merging, write it to another file
				//create dynamic partitioning here, keep multiple partitions, one for the currently loading one, one for the next iteration one
#endif//STRUCTURE_UPDATE
			}
			multi_log_context.Finalize();
			exit(0);
		}
		else {
			asyncIO.IO_manager();
		}
	}
}
