#include "header.h"

extern std::queue<struct IORequest> cache_to_IO_request_queue;
extern mutex cache_to_IO_request_queue_lock;
extern vector<ofstream> associativeVC_output_file;
extern vector<ofstream> partition_loader_output_file;
extern vector<ofstream> graph_loader_output_file;
extern vector<ofstream> Utility_output_file;

void Utils::Initialize(Asynchronous_IO *asyncIO)
{
	asynchronousIO = asyncIO;
	asynchronousIO->utility_object = this;
}

void Utils::loadContiguousFileToBuffer(int fileHandle, void *buffer, FILE_OFFSET_TYPE fileOffset/*in bytes*/, DATA_SIZE loadLength/*in bytes*/)
{
#if(DEBUG == 1)
	Utility_output_file[0] << "fO = " << fileOffset << " lL = " << loadLength << endl;
#endif//DEBUG
	PAGE_NUM_TYPE numberOfPages = ceil((loadLength*1.0) / SSD_PAGE_SIZE);
#if(DEBUG == 1)
	Utility_output_file[0] << "nP = " << numberOfPages << endl;
#endif//DEBUG
	//need to change this to request more data at a time
	FILE_OFFSET_TYPE startingOffset = fileOffset;
	numberOfPagesReturned = 0;
	IORequest io_request;
	for(PAGE_NUM_TYPE loop_1 = 0; loop_1 < numberOfPages; loop_1++)
	{
		io_request.fileHandle = fileHandle;
		io_request.buffer = (void *)((char *)buffer + SSD_PAGE_SIZE * (BUFFER_SIZE) loop_1);
		io_request.isRead = 1;
		io_request.returnReqType = 2;
		io_request.file_offset = startingOffset + SSD_PAGE_SIZE * (FILE_OFFSET_TYPE)loop_1;
		//		cout << "Req no. = " << loop_1 << " file_offset = " << io_request.file_offset / SSD_PAGE_SIZE << endl;
		io_request.length = SSD_PAGE_SIZE > (loadLength - SSD_PAGE_SIZE * (FILE_OFFSET_TYPE)loop_1) ? (loadLength - SSD_PAGE_SIZE * (FILE_OFFSET_TYPE)loop_1) : SSD_PAGE_SIZE;
		cache_to_IO_request_queue_lock.lock();
		cache_to_IO_request_queue.push(io_request);
		cache_to_IO_request_queue_lock.unlock();
	}
#if(DEBUG == 1)
	Utility_output_file[0] << "Finished submitting requests tot = " << numberOfPages << endl;
#endif//DEBUG
	while(1)
	{
		lock.lock();
		if(numberOfPagesReturned == numberOfPages) { lock.unlock();break; }
		lock.unlock();
	}
}

void Utils::handleReturnRequest(struct IORequest io_req)
{
	lock.lock();
	numberOfPagesReturned++;
#if(DEBUG == 1)
		Utility_output_file[0] << "nOPR = " << numberOfPagesReturned << " fo = " << io_req.file_offset / SSD_PAGE_SIZE << endl;
//	for(VERTEX_TYPE loop_1 = 0; loop_1 < (SSD_PAGE_SIZE / sizeof(DATA_SIZE)); loop_1++)
//	{
//		cout << loop_1 << " = " << ((DATA_SIZE *)io_req.buffer)[loop_1] << "; ";
//	}
//	cout << endl;
#endif//DEBUG
	lock.unlock();
}

void Graph_data_loader::Initialize(std::string file1, class Utils *util_ptr, BUFFER_SIZE rowPtr_buffer_size/*in bytes*/, BUFFER_SIZE colPtr_buffer_size/*in bytes*/, VERTEX_TYPE VertexCount)
{
	for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
	{
		if(posix_memalign((void **)&(buffer[loop_1]), SSD_PAGE_SIZE, colPtr_buffer_size))
		{
			fprintf(stderr, "cannot allocate io payload for data_wr\n");
			exit(0);
		}
	}
	if(posix_memalign((void **)&(row_buffer), SSD_PAGE_SIZE, rowPtr_buffer_size))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
	rowBufferVertices = (rowPtr_buffer_size / sizeof(DATA_SIZE)) - 1;
	NumberOfVertices = VertexCount;

	vertexData_file_id = open(file1.data(), O_RDONLY | O_DIRECT);
	if (vertexData_file_id == -1) {
		perror(file1.data());
		exit(1);
	}
	logLoader = util_ptr;
	vertexFileOffset = -1;
	bufferTop = 0;
	availableBufferSpace = colPtr_buffer_size;
	colPtrBufferSize = colPtr_buffer_size;
	buffer_elements[0] = 0;
	buffer_elements[1] = 0;
	buffer_ready_bit[0] = 0;
	buffer_ready_bit[1] = 0;
	buffer_lock.resize(2);
	current_buffer = 0;
	done = 0;

#if(DEBUG == 1)
	graph_loader_output_file[0] << "rBV = " << rowBufferVertices << " NOV = " << NumberOfVertices << " vertexFileOffset = " << vertexFileOffset << " aBS = " << availableBufferSpace << " cPBS = " << colPtr_buffer_size << endl;
#endif//DEBUG
}

Graph_data_loader::~Graph_data_loader()
{
	close(vertexData_file_id);
	for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
	{
		free(buffer[loop_1]);
	}
	free(row_buffer);
	free(buffer_ready_bit);
	free(buffer_elements);
}

inline DATA_SIZE Graph_data_loader::CALC_COLUMN_INDEX_OFFSET(DATA_SIZE columnIndex)
{
	return (((DATA_SIZE)NumberOfVertices+1) * sizeof(DATA_SIZE) + columnIndex * sizeof(VERTEX_TYPE));
}

void Graph_data_loader::load_next_list(bool *vertex_active_vector, VERTEX_TYPE partition_start_vertex, PARTITION_SIZE activeVertexSize/*numberOfVertices*/)
{
#if(DEBUG == 1)
	graph_loader_output_file[0] << "p_s = " << partition_start_vertex << " aVS = " << activeVertexSize << endl;
#endif//DEBUG
	bool active_vetex_present = 0;
	struct bufferIndexPtr buffer_ptr;
	BUFFER_SIZE rowPtr_load_length;
	VERTEX_TYPE loop_start_row_vertex, rowPtr_load_length_vertices, verticesRequired;
	FILE_OFFSET_TYPE fileOffset;
	BUFFER_SIZE vertexColPtrSize;
	PAGE_NUM_TYPE numberOfRequiredPages;
	FILE_OFFSET_TYPE currentColPtrFileOffset;
	for(VERTEX_TYPE loop_1 = 0; loop_1 < activeVertexSize; )
	{
		loop_start_row_vertex = partition_start_vertex + loop_1;
		verticesRequired = activeVertexSize - loop_1;
		rowPtr_load_length_vertices = (rowBufferVertices < verticesRequired) ? rowBufferVertices : verticesRequired;
		rowPtr_load_length = (BUFFER_SIZE)(rowPtr_load_length_vertices+1) * sizeof(DATA_SIZE);
		fileOffset = (((FILE_OFFSET_TYPE)loop_start_row_vertex*sizeof(DATA_SIZE)) / SSD_PAGE_SIZE) * SSD_PAGE_SIZE;
#if(DEBUG == 1)
		graph_loader_output_file[0] << "l_1 = " << loop_1 << " l_s_r_v = " << loop_start_row_vertex << " vR = " << verticesRequired << " r_l_l_v = " << rowPtr_load_length_vertices << " r_l_l = " << rowPtr_load_length << " fO = " << fileOffset << endl;
#endif//DEBUG
		logLoader->loadContiguousFileToBuffer(vertexData_file_id, row_buffer, fileOffset, rowPtr_load_length);
#if(DEBUG == 1)
/*		cout << "After loading a row buffer" << endl;
		for(VERTEX_TYPE loop_2 = 0; loop_2 < rowPtr_load_length_vertices; loop_2++)
		{
			cout << loop_2 << " = " << ((DATA_SIZE *)row_buffer)[loop_2] << "; ";
		}
		cout << endl;*/
#endif//DEBUG
		for(VERTEX_TYPE loop_2 = 0; loop_2 < rowPtr_load_length_vertices; loop_2++)
		{
			if(vertex_active_vector[loop_1 + loop_2] == 1)
			{
				active_vetex_present = 1;
				vertexColPtrSize = ((DATA_SIZE *)row_buffer)[loop_2+1] - ((DATA_SIZE *)row_buffer)[loop_2];
				numberOfRequiredPages = ceil(((CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE + (DATA_SIZE)vertexColPtrSize*sizeof(DATA_SIZE))*1.0) / SSD_PAGE_SIZE);
				currentColPtrFileOffset = (CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) / SSD_PAGE_SIZE) * SSD_PAGE_SIZE;
#if(DEBUG == 1)
				graph_loader_output_file[0] << "l_2 = " << loop_2 << " vCPS = " << vertexColPtrSize << " nORP = " << numberOfRequiredPages << " cCPFO = " << currentColPtrFileOffset  << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG
				//Leaving it here - was adding output statements
				if(vertexColPtrSize == 0)
				{
					buffer_ptr = {loop_start_row_vertex+loop_2, bufferTop, vertexColPtrSize};
#if(DEBUG == 1)
					graph_loader_output_file[0] << "vI = " << buffer_ptr.vertex_id << " bT = " << buffer_ptr.start_index << " s = " << buffer_ptr.size << endl;
#endif//DEBUG
					vertex_index_list[current_buffer].push_back(buffer_ptr);
					buffer_elements[current_buffer] += 1;
					continue;
				}
				else if((vertexFileOffset != -1) && (vertexFileOffset == currentColPtrFileOffset) && (numberOfRequiredPages == 1))
				{
					buffer_ptr = {loop_start_row_vertex+loop_2,bufferTop - SSD_PAGE_SIZE + CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE, vertexColPtrSize};
#if(DEBUG == 1)
					graph_loader_output_file[0] << "vI = " << buffer_ptr.vertex_id << " bT = " << buffer_ptr.start_index << " s = " << buffer_ptr.size << endl;
#endif//DEBUG
					vertex_index_list[current_buffer].push_back(buffer_ptr);
					buffer_elements[current_buffer] += 1;
					continue;
				}
				if(availableBufferSpace >= ((BUFFER_SIZE)numberOfRequiredPages*SSD_PAGE_SIZE))
				{
					logLoader->loadContiguousFileToBuffer(vertexData_file_id, (char *)(buffer[current_buffer]) + bufferTop, currentColPtrFileOffset, (DATA_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE);
#if(DEBUG == 1)
					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG
					buffer_ptr = {loop_start_row_vertex+loop_2, bufferTop + CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE, vertexColPtrSize};
#if(DEBUG == 1)
					graph_loader_output_file[0] << "vI = " << buffer_ptr.vertex_id << " bT = " << buffer_ptr.start_index << " s = " << buffer_ptr.size << endl;
#endif//DEBUG
#if(DEBUG == 1)
					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG
					vertex_index_list[current_buffer].push_back(buffer_ptr);
#if(DEBUG == 1)
					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG
					buffer_elements[current_buffer] += 1;
#if(DEBUG == 1)
					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG
					bufferTop += (BUFFER_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE;
#if(DEBUG == 1)
					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG
					availableBufferSpace -= ((BUFFER_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE);
#if(DEBUG == 1)
					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG
					vertexFileOffset = currentColPtrFileOffset + (DATA_SIZE)(numberOfRequiredPages-1) * SSD_PAGE_SIZE;
#if(DEBUG == 1)
					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG
				}
				else {
#if(DEBUG == 1)
					graph_loader_output_file[0] << "Sending a buffer from graph data loader" << endl;
#endif//DEBUG
					buffer_lock[current_buffer].lock();
					buffer_ready_bit[current_buffer] = 1;
					buffer_lock[current_buffer].unlock();
					current_buffer = (current_buffer + 1) % 2;
					while(1)
					{
						buffer_lock[current_buffer].lock();
						if(buffer_ready_bit[current_buffer] == 0)
						{
							buffer_lock[current_buffer].unlock();
							break;
						}
						buffer_lock[current_buffer].unlock();
					}
					logLoader->loadContiguousFileToBuffer(vertexData_file_id, buffer[current_buffer], currentColPtrFileOffset, (BUFFER_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE);
					buffer_ptr = {loop_start_row_vertex+loop_2, CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE, vertexColPtrSize};
#if(DEBUG == 1)
					graph_loader_output_file[0] << "vI = " << buffer_ptr.vertex_id << " bT = " << buffer_ptr.start_index << " s = " << buffer_ptr.size << endl;
#endif//DEBUG
					vertex_index_list[current_buffer].push_back(buffer_ptr);
					buffer_elements[current_buffer] += 1;
					bufferTop = numberOfRequiredPages * SSD_PAGE_SIZE;
					availableBufferSpace = colPtrBufferSize - ((BUFFER_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE);
					vertexFileOffset = currentColPtrFileOffset + ((DATA_SIZE)numberOfRequiredPages-1) * SSD_PAGE_SIZE;
#if(DEBUG == 1)
					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG
				}
			}
		}
		loop_1 += rowPtr_load_length_vertices;
	}
	buffer_lock[current_buffer].lock();
	if(active_vetex_present == 1)
	{
		graph_loader_output_file[0] << "Sending last buffer in the partition cb = " << current_buffer <<endl;
		buffer_ready_bit[current_buffer] = 1;
	}
	done_lock.lock();
	done=1;
	done_lock.unlock();
	buffer_lock[current_buffer].unlock();
	if(active_vetex_present == 1)
	{
		current_buffer = (current_buffer + 1) % 2;
	}
	bufferTop = 0;//Left here
	vertexFileOffset = -1;
}

AssocativeMultiLogVC::AssocativeMultiLogVC(Graph_data_loader *ptr, BUFFER_SIZE bufferElementsSize/*in bytes*/, VERTEX_TYPE VertexCount)
{
	graph_data = ptr;
	maxElementsInBuffer = bufferElementsSize / sizeof(DATA_SIZE);
#if(TESTING == 0)
	numOfPartitions = ceil((VertexCount*1.0) / maxElementsInBuffer);
#else
	numOfPartitions = 2;//for testing
#endif//TESTING
	NumOfVertices = VertexCount;
	current_buffer = 0;
#if(DEBUG == 1)
	associativeVC_output_file[0] << "mEIB = " << maxElementsInBuffer << " nOP = " << numOfPartitions << " NOV = " << NumOfVertices << endl;
#endif//DEBUG

}

void AssocativeMultiLogVC::run()
{
	PARTITION_NUM_TYPE numOfPartitions_processed = 0;
	bool *active_list = (bool *)malloc(sizeof(bool) * maxElementsInBuffer);
	for(BUFFER_SIZE loop_1 = 0; loop_1 < maxElementsInBuffer; loop_1++)
	{
		active_list[loop_1] = 1;
	}
#if(DEBUG == 1)
	associativeVC_output_file[0] << "mEIB = " << maxElementsInBuffer <<endl;
#endif//DEBUG
	while(numOfPartitions_processed < numOfPartitions) {//all the partitions are done
		VERTEX_TYPE startingVertexIndexOfPartition = numOfPartitions_processed * maxElementsInBuffer;
		PARTITION_SIZE activeVertexSize = maxElementsInBuffer < (NumOfVertices - numOfPartitions_processed * maxElementsInBuffer)? maxElementsInBuffer:(NumOfVertices - numOfPartitions_processed*maxElementsInBuffer);
		numOfPartitions_processed++;
#if(DEBUG == 1)
		associativeVC_output_file[0] << "nPp = " << numOfPartitions_processed << " sVIP = " << startingVertexIndexOfPartition << " aVS = " << activeVertexSize << endl;
#endif//DEBUG
#pragma omp parallel num_threads(2)
		{
			unsigned int tid = omp_get_thread_num();
			if(tid == 0)
			{
				graph_data->load_next_list(active_list, startingVertexIndexOfPartition, activeVertexSize);
			}
			else {
				bool exit;
				while(1) {//all the buffers are done
					exit = 0;
					while(1){
						((graph_data->buffer_lock)[current_buffer]).lock();
						if((graph_data->buffer_ready_bit)[current_buffer] == 1)
						{
							((graph_data->buffer_lock)[current_buffer]).unlock();
							break;
						}
						else {
#if(DEBUG == 1)
				//			associativeVC_output_file[0] << "cb = " << current_buffer << endl;
#endif//DEBUG
							graph_data->done_lock.lock();
							if(graph_data->done == 1) {
								graph_data->done = 0;
#if(DEBUG == 1)
				//				associativeVC_output_file[0] << "done cb = " << current_buffer << endl;
#endif//DEBUG
								exit = 1;
							}
							graph_data->done_lock.unlock();
						}
						((graph_data->buffer_lock)[current_buffer]).unlock();
						if(exit == 1) break;
					}
#if(DEBUG == 1)
					associativeVC_output_file[0] << "cb = " << current_buffer << endl;
					associativeVC_output_file[0] << "Received a buffer from graph data loader" << endl;
#endif//DEBUG
					if(exit == 1) break;
					BUFFER_SIZE numberOfElementsInBuffer = (graph_data->buffer_elements[current_buffer]);
#if(DEBUG == 1)
					associativeVC_output_file[0] << "nOEIB = " << numberOfElementsInBuffer << endl;
#endif//DEBUG
//#pragma omp parallel for
					for(BUFFER_SIZE loop_2 = 0; loop_2 < numberOfElementsInBuffer; loop_2++)
					{
#if(DEBUG == 1)
						associativeVC_output_file[0] << "vi = " <<graph_data->vertex_index_list[current_buffer][loop_2].vertex_id << " si = " << (graph_data->vertex_index_list)[current_buffer][loop_2].start_index << " s = " << graph_data->vertex_index_list[current_buffer][loop_2].size << endl;
#endif//DEBUG
						associativeVC_output_file[0] << graph_data->vertex_index_list[current_buffer][loop_2].vertex_id << ":";
						for(VERTEX_TYPE loop_3 = 0; loop_3 < graph_data->vertex_index_list[current_buffer][loop_2].size; loop_3++)
						{
							associativeVC_output_file[0] << " " << ((VERTEX_TYPE *)((char *)((graph_data->buffer)[current_buffer]) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index))[loop_3];
						}
						associativeVC_output_file[0] << endl;
					}

					graph_data->vertex_index_list[current_buffer].clear();
					graph_data->buffer_elements[current_buffer] = 0;
					((graph_data->buffer_lock)[current_buffer]).lock();
					(graph_data->buffer_ready_bit)[current_buffer] = 0;
					((graph_data->buffer_lock)[current_buffer]).unlock();
					current_buffer = (current_buffer + 1) % 2;
#if(DEBUG == 1)
					associativeVC_output_file[0] << "cn = " << current_buffer << endl;
					associativeVC_output_file[0] << "Finished processing buffer in associative VC" << endl;
#endif//DEBUG
				}
			}
		}
	}
#if(DEBUG == 1)
	associativeVC_output_file[0] << "Done with VC" << endl;
#endif//DEBUG
	free(active_list);
}
