#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <linux/aio_abi.h>
#include <sys/syscall.h> // syscall numbers
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <limits>
#include <time.h>
#include <queue>
#include <set>
//#include <boost/filesystem.hpp>
#include <experimental/filesystem>
//using namespace boost::filesystem;
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/atomic.hpp>
namespace fs = std::experimental::filesystem::v1;
#include <cstddef>
#include <thread>
#include <atomic>
#include <mutex>
#include <shared_mutex>
#include <string>
using namespace std;
#include <omp.h>
#include <unistd.h>
#include <sys/mman.h>
#include "datatypes.h"
#include "utility.h"
#include "application_types.h"

class Asynchronous_IO;
class Utils;
class Graph_data_loader;
class AssocativeMultiLogVC;
class Graph_info;
class MultiLogContext;
class MultiLogEngine;
class MultiLogProgram;
class AssociativeProgram;
void Initialization(int argc, char *argv[]);

class Multi_log_cache
{
	public:
		LOG_CACHE_BLOCK_TYPE multi_log_cache;
		list<CPN_TYPE> freeCPN;
		mutex list_update_lock;
		vector < vector<list<struct page_info> > > partition_page_info;
		vector< vector<mutex_wrapper> > partition_page_info_lock;
		vector < vector< struct partition_cache_page_pointer > > vertexToPartitionPage;
		vector<mutex_wrapper>  vertexToPartitionPage_lock;
		vector< std::string > log_file;
		vector< int > log_file_id;
		vector < FILE_OFFSET_TYPE > currentFileTop;
		bool current_partition_file;
		PAGE_OFFSET_TYPE page_size;
		PARTITION_NUM_TYPE num_of_partitions;
		BUFFER_SIZE partition_size;
		CACHE_SIZE cache_size;
		Asynchronous_IO *io_ptr;
		VERTEX_TYPE NumNodes;
		unsigned int cacheType;

		Multi_log_cache(){}
		MultiLogContext *multi_log_context;
		void Initialize(MultiLogContext *multiLogContext, std::string file1, std::string file2, VERTEX_TYPE numNodes, PARTITION_NUM_TYPE partitions, BUFFER_SIZE partitionSize, CACHE_SIZE cacheSize, Asynchronous_IO *ioPtr, unsigned int cache_type);
		~Multi_log_cache();
		void send_message(LOG_MESSAGE_PTR_TYPE message, LOG_MESSAGE_SIZE_TYPE message_size);
		void send_message_partition(LOG_MESSAGE_PTR_TYPE message, LOG_MESSAGE_SIZE_TYPE message_size, PARTITION_NUM_TYPE partition_num);
		void handleReturnRequest(struct IORequest io_req);
		void print_partitions();
		unsigned int binarySearch(std::vector<VERTEX_TYPE> & partition_starting_vertex, VERTEX_TYPE l, VERTEX_TYPE r, VERTEX_TYPE x, VERTEX_TYPE *index, VERTEX_TYPE *number);
		unsigned int binarySearch(VERTEX_TYPE * partition_starting_vertex, VERTEX_TYPE l, VERTEX_TYPE r, VERTEX_TYPE x, VERTEX_TYPE *index, VERTEX_TYPE *number);
};

class Asynchronous_IO {
	public:
		class Multi_log_cache  *multi_log_cache;
#if(EDGE_LOG_PROCESSING == 1)
		Multi_log_cache  * edge_log_multi_log_cache;
#endif//EDGE_LOG_PROCESSING
#if(STRUCTURE_UPDATE == 1)
		class Multi_log_cache *structure_multi_log_cache;
#endif//STRUCTURE_UPDATE
		class Utils *utility_object;
		unsigned int io_maxevents;
		Asynchronous_IO(){};
		void Initialize();
		void IO_manager();
		void IO_manager_async_tid(IO_TID_TYPE tid);
		void AccessSSDN(FILE_OFFSET_TYPE file_offset, DATA_SIZE length, void *buffer, bool isRead, int myFlashFile, unsigned int data, IO_TID_TYPE tid, aio_context_t &ioctx);
};

class Utils//Util class is for loading from either graph data or partition file, basically it needs offset, file handle, and length
{
	public:
	Asynchronous_IO *asynchronousIO;
	Multi_log_cache  *multi_log_cache;
#if(EDGE_LOG_PROCESSING == 1)
	Multi_log_cache  * edge_log_multi_log_cache;
#endif//EDGE_LOG_PROCESSING
#if(STRUCTURE_UPDATE == 1)
	Multi_log_cache  * structure_multi_log_cache;
#endif//STRUCTURE_UPDATE
	shared_mutex lock;
	PAGE_NUM_TYPE numberOfPagesReturned_contiguous;
	PAGE_NUM_TYPE numberOfPagesRequested_contiguous;
	PAGE_NUM_TYPE numberOfPagesReturned;
	Utils(){}
	void Initialize(Asynchronous_IO *asyncIO);
	void loadContiguousFileToBuffer(int fileHandle, void *buffer, FILE_OFFSET_TYPE fileOffset, DATA_SIZE loadLength);
	void handleReturnRequest(struct IORequest io_req);
	void handleReturnRequest2(struct IORequest io_req);
	void loadListPagesFromFileToBuffer(int fileHandle, void *buffer, BUFFER_SIZE &numberOfMessages, vector <struct page_info> &page_list, struct partition_cache_page_pointer &cachePartitionPtr, unsigned int isEdgeLog);
	bool is_loading_done();
#if(STATS_PAGE_IN_EFFICIENCY == 1)
	FILE_OFFSET_TYPE total_number_pages_accessed = 0;
	FILE_OFFSET_TYPE total_number_edge_pages_accessed = 0;
#endif//STATS_PAGE_IN_EFFICIENCY
};

struct bufferIndexPtr {
	VERTEX_TYPE vertex_id;
	BUFFER_SIZE start_index;
	BUFFER_SIZE size;
#if(PROCESS_EDGE_WEIGHT == 1)
	DATA_SIZE edge_weight_index;
#endif//PROCESS_EDGE_WEIGHT
};

class Graph_data_loader
{
	public:
		void *buffer[2];
		bool buffer_ready_bit[2];
		BUFFER_SIZE buffer_elements[2];
		vector<mutex_wrapper> buffer_lock;
		vector<struct bufferIndexPtr> vertex_index_list[2];
		void *row_buffer;
		int vertexData_file_id;
		int vertexData_row_file_id;
#if(PROCESS_EDGE_WEIGHT == 1)
		int vertexData_in_edge_file_id;
		std::string edge_weights_filename;
		inline DATA_SIZE CALC_COLUMN_INDEX_OFFSET_IN_EDGE(DATA_SIZE columnIndex);
#endif//PROCESS_EDGE_WEIGHT
#if(STRUCTURE_UPDATE == 1)
		std::string structure_delete_edge_filename;
#endif//STRUCTURE_UPDATE
		Utils *logLoader;
		FILE_OFFSET_TYPE vertexFileOffset;
		BUFFER_SIZE bufferTop;
		BUFFER_SIZE colPtrBufferSize;
		BUFFER_SIZE availableBufferSpace;
		VERTEX_TYPE rowBufferVertices;
		VERTEX_TYPE NumberOfVertices;
		mutex done_lock;
		bool done;
		bool current_buffer;

		Graph_data_loader(){}
		void Initialize(std::string file1, class Utils *util_ptr, BUFFER_SIZE rowPtr_buffer_size, BUFFER_SIZE colPtr_buffer_size, VERTEX_TYPE VertexCount);
		~Graph_data_loader();
		inline DATA_SIZE CALC_COLUMN_INDEX_OFFSET(DATA_SIZE columnIndex);
		void load_next_list(bool *vertex_active_vector, VERTEX_TYPE partition_start_vertex, PARTITION_SIZE activeVertexSize);
#if(EDGE_LOG_MEASURE == 1)
		DATA_SIZE number_of_pages_requested;
#endif//EDGE_LOG_MEASURE
#if(STATS_PAGE_IN_EFFICIENCY == 1)
		FILE_OFFSET_TYPE total_number_edge_list_size = 0;
		unordered_map<FILE_OFFSET_TYPE, int> total_data_accessed_in_a_page;
		unordered_map<FILE_OFFSET_TYPE, int> total_vertices_accessed_in_a_page;
#endif//STATS_PAGE_IN_EFFICIENCY

};

class MultiLogVC
{
	public:
		MultiLogContext *multi_log_context;
		Graph_data_loader *graph_data;
		Utils *logLoader;
		Multi_log_cache  *multi_log_cache;
		BUFFER_SIZE maxElementsInBuffer;
		PARTITION_NUM_TYPE numOfPartitions;
		VERTEX_TYPE NumOfVertices;
		bool current_buffer;
		MultiLogVC(){}
		void Initialize(MultiLogContext *multiLogContext, Graph_data_loader *ptr,  Utils *util_ptr, BUFFER_SIZE bufferElementsSize, VERTEX_TYPE VertexCount, BUFFER_SIZE updateMessageBufferSize);
		void run();
		void * partition_vertex_values;
		void * partition_update_messages_buffer;
		EDGE_MESSAGE_TYPE * partition_update_messages_buffer_sorted;
#if(STRUCTURE_UPDATE == 1)
		Multi_log_cache  *structure_multi_log_cache;
		void * structure_partition_update_messages_buffer;
		STRUCTURE_EDGE_MESSAGE_TYPE * structure_partition_update_messages_buffer_sorted;//what should be the STRUCTURE_EDGE_MESSAGE_TYPE? what should be the structure edge message type???
		int structure_delete_in_edge_file_id;
		bool * structure_delete_edge_list;
		int structure_map_edge_list_id;
		VERTEX_TYPE * structure_map_edge_list;
		EDGE_LIST structure_edge_list;
		VERTEX_TYPE structure_edge_list_size;
		inline DATA_SIZE CALC_COLUMN_INDEX_OFFSET(VERTEX_TYPE vertex_id);
#endif//STRUCTURE_UPDATE
#if(EDGE_LOG_PROCESSING == 1)
		void * edge_log_partition_update_messages_buffer;
		Multi_log_cache   *edge_log_multi_log_cache;
#endif//EDGE_LOG_PROCESSING
		BUFFER_SIZE maxSizeInBuffer_messages;
		PARTITION_NUM_TYPE num_partitions;
#if(ASYNCHRONOUS_PROGRAMMING == 1)
		LIST_UPDATE_MESSAGE *update_messages_buffer_list;
		BUFFER_SIZE update_messages_buffer_top;
		BUFFER_SIZE *vertex_to_messages_buffer_list_ptr;
		EDGE_MESSAGE_TYPE * vertex_messages_temp_buffer;
		UPDATE_MESSAGE_LIST Results_data;
#if(VC_ASSOCIATIVE_PROGRAM == 1)
		inline void Application_function_combine(VERTEX_TYPE vertex_id, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context);
#endif//VC_ASSOCIATIVE_PROGRAM
#endif//ASYNCHRONOUS_PROGRAMMING
};

class AssocativeMultiLogVC //: public MultiLog
{
	public:
		MultiLogContext *multi_log_context;
		Graph_data_loader *graph_data;
		Utils *logLoader;
		Multi_log_cache  *multi_log_cache;
		BUFFER_SIZE maxElementsInBuffer;
		VERTEX_TYPE NumOfVertices;
		bool current_buffer;
		AssocativeMultiLogVC(){}
		void Initialize(MultiLogContext *multiLogContext, Graph_data_loader *ptr,  Utils *util_ptr, BUFFER_SIZE bufferElementsSize, VERTEX_TYPE VertexCount, BUFFER_SIZE updateMessageBufferSize);
		void run();
		void * partition_vertex_values;
		void * partition_update_messages_buffer;
		BUFFER_SIZE maxSizeInBuffer_messages;
		inline void Application_function_combine(VERTEX_TYPE vertex_id, EDGE_MESSAGE_TYPE &edge_message, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context);
};

class ApplicationProgram
{
	public:
		VERTEX_TYPE target;
		bool converged;
		bool hasActiveVertices;
		PAGERANK_VERTEX_VALUE_TYPE Delta;
		ApplicationProgram(){}
		virtual ~ApplicationProgram() = default;
};

class AssociativeProgram : public ApplicationProgram
{
	public:
		virtual void before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context) = 0;
		virtual inline void combine_message(VERTEX_TYPE vertex_id, EDGE_MESSAGE_TYPE &edge_message, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context) = 0;
		virtual inline void edge_program_weights(EDGE_WEIGHT_TYPE *Edge_weights, VERTEX_VALUE_TYPE &Vertex_value, EDGE_MESSAGE_TYPE *Result_data, MultiLogContext *multi_log_context) = 0;
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
		virtual void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context) = 0;
#else
		virtual void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context) = 0;
#endif//EDGE_PROGRAM_OPTIMIZATION
		virtual void after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context) = 0;
};

class MultiLogProgram : public ApplicationProgram
{
	public:
	virtual void before_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context) = 0;
#if(EDGE_PROGRAM_OPTIMIZATION == 1)
#if(ASYNCHRONOUS_PROGRAMMING == 0)
	virtual void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, MultiLogContext *multi_log_context) = 0;
#elif(ASYNCHRONOUS_PROGRAMMING == 1)
	virtual void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, UPDATE_MESSAGE_LIST Results_data, BUFFER_SIZE &Result_data_size, MultiLogContext *multi_log_context) = 0;
#if(PROCESS_EDGE_WEIGHT == 1)
	virtual void edge_program_lists_weights(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, EDGE_LIST Edge_list, VERTEX_TYPE Edge_list_size, IN_EDGE_AND_WEIGHT_FILE_TYPE *In_edge_weights, UPDATE_MESSAGE_LIST Results_data, BUFFER_SIZE &Result_data_size, MultiLogContext *multi_log_context) = 0;
#endif//PROCESS_EDGE_WEIGHT
#endif//ASYNCHRONOUS_PROGRAMMING
#else//EDGE_PROGRAM_OPTIMIZATION
	virtual void edge_program_lists(VERTEX_TYPE vertex_id, VERTEX_VALUE_TYPE &Vertex_value, MESSAGE_LIST &Message_list, EDGE_LIST &Edge_list, EDGE_MESSAGES_TYPE &Result_data, MultiLogContext *multi_log_context) = 0;
#endif//EDGE_PROGRAM_OPTIMIZATION
	virtual void after_iteration(ITERATION_NUM_TYPE iteration_num, MultiLogContext *multi_log_context) = 0;
};

class Graph_info
{
	public:
		VERTEX_TYPE NumNodes;
		DATA_SIZE NumEdges;
		std::string csr_file_content;
		std::string csr_file_header;
		int vertexValue_file_id;
		int vertexActive_file_id;
		void Initialize(int argc, char *argv[]);
};

#if(FLP_COMPILE == 1)
#include "../Applications/FLP/flp.h"
#endif//FLP_COMPILE
#if(FLP_ASYNC_COMPILE == 1)
#include "../Applications/FLP_ASYNC/flp.h"
#endif//FLP_ASYNC_COMPILE
#if(GGC_COMPILE == 1)
#include "../Applications/GGC/ggc.h"
#endif//GGC_COMPILE
#if(PAGE_RANK_THRESHOLD_COMPILE == 1)
#include "../Applications/PageRankThreshold/prt.h"
#endif
#if(BFS_COMPILE == 1)
#include "../Applications/BFS/BFS.h"
#endif//BFS_COMPILE
#if(PRTA_COMPILE == 1)
#include "../Applications/PageRankThresholdAsync/prt.h"
#endif//PRTA_COMPILE
#if(GC_COMPILE == 1)
#include "../Applications/GC/gc.h"
#endif//GC_COMPILE
#if(GC_EDGE_LOG_COMPILE == 1)
#include "../Applications/GC_EDGE_LOG/gc.h"
#endif//GC_COMPILE
#if(KCORE_COMPILE == 1)
#include "../Applications/KCore/kcore.h"
#endif//KCORE_COMPILE

class MultiLogContext
{
	public:
		unsigned int application_type;
		unsigned int programType;
		ITERATION_NUM_TYPE current_iteration;//need to increment it at appropriate place
		Graph_info graph_info;
		ApplicationProgram *program;
		Multi_log_cache *multi_log_cache;
#if(EDGE_LOG_PROCESSING == 1)
		Multi_log_cache *edge_log_multi_log_cache;
#endif//EDGE_LOG_PROCESSING
#if(STRUCTURE_UPDATE == 1)
		Multi_log_cache  *structure_multi_log_cache;
		bool * structure_delete_vertex_list;
#endif//STRUCTURE_UPDATE
#if(FLP_COMPILE == 1)
		FLP *program_edge_processing;
#endif//FLP_COMPILE
#if(FLP_ASYNC_COMPILE == 1)
		FLPA *program_edge_processing;
#endif//FLP_ASYNC_COMPILE
#if(GGC_COMPILE == 1)
		GGC *program_edge_processing;
#endif//GGC_COMPILE
#if(PAGE_RANK_THRESHOLD_COMPILE == 1)
		PageRankThreshold *program_edge_processing;
#endif//PAGE_RANK_THRESHOLD_COMPILE
#if(BFS_COMPILE == 1)
		BFS *program_edge_processing;
#endif//BFS_COMPILE
#if(PRTA_COMPILE == 1)
		PRTA *program_edge_processing;
#endif//PRTA_COMPILE
#if(GC_COMPILE == 1)
		GC *program_edge_processing;
#endif//GC_COMPILE
#if(GC_EDGE_LOG_COMPILE == 1)
		GC *program_edge_processing;
#endif//GC_EDGE_LOG_COMPILE
#if(KCORE_COMPILE == 1)
		KCore *program_edge_processing;
#endif//KCORE_COMPILE
		MultiLogContext(){}
		~MultiLogContext(){}
		void Initialize(int argc, char *argv[], unsigned int program_type);
		std::vector<VERTEX_TYPE> partition_starting_vertex;
		std::vector<VERTEX_TYPE> partition_size_vertex;
		PARTITION_NUM_TYPE numOfPartitions;
	
		void Finalize();
		class Timer app_timer;
		class Timer timer_before_iteration;
		class Timer timer_value_access;
		class Timer timer_active_vertices;
		class Timer timer_log_loading;
		class Timer timer_log_processing;
		class Timer timer_log_generation;
		class Timer timer_graph_access;
		class Timer timer_edge_program;
		class Timer timer_after_iteration;
		class Timer timer_column_index_iteration;
		class Timer timer_edge_processing;
		class Timer timer_multi_log_access;
		DATA_SIZE iterationMessages;
		DATA_SIZE totalMessages;
#if(PAGE_RANK_THRESHOLD_DEBUG == 1)
		VERTEX_TYPE num_active_vertices;
#endif//PAGE_RANK_THRESHOLD_DEBUG
#if(ASYNCHRONOUS_PROGRAMMING == 1)
#if(PROCESS_EDGE_WEIGHT == 1)
		void *Edge_weights_file_array;
#endif//PROCESS_EDGE_WEIGHT
#endif//ASYNCHRONOUS_PROGRAMMING
#if(STRUCTURE_UPDATE == 1)
		void delete_edge(LOG_MESSAGE_PTR_TYPE message, LOG_MESSAGE_SIZE_TYPE message_size);
		void delete_vertex(VERTEX_TYPE vertex_id);
#endif//STRUCTURE_UPDATE
#if(EDGE_LOG_PROCESSING == 1)
		bool edge_log_read;
		bool edge_log_write;
#if(EDGE_LOG_MEASURE == 1)
		DATA_SIZE edge_log_read_data;
#endif//EDGE_LOG_MEASURE
#endif//EDGE_LOG_PROCESSING
};

class MultiLogEngine
{
	public:
		Asynchronous_IO asyncIO;
		Utils utility_object;
		Graph_data_loader graph_loader;
		AssocativeMultiLogVC associateVC;
		MultiLogVC multiLogVC;
		Multi_log_cache  multi_log_cache;
#if(STRUCTURE_UPDATE == 1)
		Multi_log_cache structure_multi_log_cache;
#endif//STRUCTURE_UPDATE
#if(EDGE_LOG_PROCESSING == 1)
		Multi_log_cache  edge_log_multi_log_cache;
#endif//EDGE_LOG_PROCESSING
		MultiLogContext multi_log_context;
		MultiLogEngine(int argc, char *argv[], unsigned int programType);
		void run(ApplicationProgram *Program, unsigned int programType, unsigned int applicationType);
};
