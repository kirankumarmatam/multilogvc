#include "header.h"

vector<ofstream> associativeVC_output_file;
vector<ofstream> partition_loader_output_file;
vector<ofstream> graph_loader_output_file;
vector<ofstream> IO_output_file;
vector<ofstream> Utility_output_file;
vector<ofstream> Log_cache_output_file;

unsigned int NumOfAssociativeVCThreads;
unsigned int NumOfGraphLoaderThreads;
unsigned int NumOfPartitionLoaderThreads;
unsigned int NumOfIOThreads;
unsigned int NumOfUtilityThreads = 1;
unsigned int NumOfLogCacheThreads = 1;

void Initialization(int argc, char *argv[])
{
/*---------AssociativeVC related ones---------*/
	NumOfAssociativeVCThreads = 1;
	associativeVC_output_file.resize(NumOfAssociativeVCThreads);
	string fileName;
	associativeVC_output_file.resize(NumOfAssociativeVCThreads);
	for(unsigned int i = 0; i < NumOfAssociativeVCThreads; i++)
	{
		fileName = "/Data/Output/associativeVC_"+to_string(i)+".txt";
		associativeVC_output_file[i].open(fileName, ios::trunc);
		assert(associativeVC_output_file[i].is_open());
	}
/*---------Graph Loader related ones---------*/
	NumOfGraphLoaderThreads = 1;
	graph_loader_output_file.resize(NumOfGraphLoaderThreads);
	for(unsigned int i = 0; i < (NumOfGraphLoaderThreads); i++)
	{
		fileName = "/Data/Output/graphLoader_"+to_string(i)+".txt";
		graph_loader_output_file[i].open(fileName, ios::trunc);
		assert(graph_loader_output_file[i].is_open());
	}
/*---------Partition Loader related ones----------*/
	NumOfPartitionLoaderThreads = 1;
/*---------I/O related ones---------*/
	NumOfIOThreads = 1;
	IO_output_file.resize(NumOfIOThreads);
	for(unsigned int i = 0; i < (NumOfIOThreads); i++)
	{
		fileName = "/Data/Output/IO_"+to_string(i)+".txt";
		IO_output_file[i].open(fileName, ios::trunc);
		assert(IO_output_file[i].is_open());
	}
/*---------Utility related ones---------*/
	Utility_output_file.resize(NumOfUtilityThreads);
	for(unsigned int i = 0; i < (NumOfUtilityThreads); i++)
	{
		fileName = "/Data/Output/Utility_"+to_string(i)+".txt";
		Utility_output_file[i].open(fileName, ios::trunc);
		assert(Utility_output_file[i].is_open());
	}
/*---------Log cache related ones---------*/
	Log_cache_output_file.resize(NumOfLogCacheThreads);
	for(unsigned int i = 0; i < (NumOfLogCacheThreads); i++)
	{
		fileName = "/Data/Output/Log_cache_"+to_string(i)+".txt";
		Log_cache_output_file[i].open(fileName, ios::trunc);
		assert(Log_cache_output_file[i].is_open());
	}
}

	static inline int
io_destroy(aio_context_t ctx)
{
	return syscall(SYS_io_destroy, ctx);
}

void MultiLogContext::Finalize()
{
	app_timer.end_timer();
	app_timer.print_timer("Application time is ");
	timer_before_iteration.print_timer("Before iteration time is ");
	timer_value_access.print_timer("Value access time is ");
	timer_active_vertices.print_timer("Active vertices time is ");
	timer_log_loading.print_timer("Log loading time is ");
	timer_log_processing.print_timer("Log processing time is ");
	timer_log_generation.print_timer("Log generation time is ");
	timer_graph_access.print_timer("Graph access time is ");
	timer_edge_program.print_timer("Edge program time is ");
#if(ASYNCHRONOUS_PROGRAMMING == 1)
	timer_edge_processing.time_elapsed -= timer_multi_log_access.time_elapsed;
#endif//ASYNCHRONOUS_PROGRAMMING
	timer_edge_processing.print_timer("Edge program processing time is ");
	timer_multi_log_access.print_timer("Edge program multi log access time is ");
	timer_after_iteration.print_timer("After iteration time is ");
	timer_column_index_iteration.print_timer("Column index loading time ");
	class Timer timer_application_final;
	timer_application_final.time_elapsed = app_timer.time_elapsed - std::min(timer_log_loading.time_elapsed, timer_log_processing.time_elapsed);
	timer_application_final.print_timer("Associative application final time is ");
	class Timer timer_nsp_application_final;
	double sp_to_hp_ratio = 0.15, sb_to_hb_ratio = 1.0/2.0;
	timer_nsp_application_final.time_elapsed = app_timer.time_elapsed - timer_log_processing.time_elapsed * sp_to_hp_ratio - timer_log_loading.time_elapsed * sb_to_hb_ratio;
	timer_nsp_application_final.time_elapsed = timer_nsp_application_final.time_elapsed - std::min(timer_log_processing.time_elapsed * (1.0 - sp_to_hp_ratio), timer_log_loading.time_elapsed * (1.0 - sb_to_hb_ratio));
	timer_nsp_application_final.time_elapsed = timer_nsp_application_final.time_elapsed - timer_edge_processing.time_elapsed * sp_to_hp_ratio - timer_multi_log_access.time_elapsed * sb_to_hb_ratio;
	timer_nsp_application_final.time_elapsed = timer_nsp_application_final.time_elapsed - timer_graph_access.time_elapsed * sb_to_hb_ratio;
	timer_nsp_application_final.time_elapsed = timer_nsp_application_final.time_elapsed - timer_value_access.time_elapsed * sb_to_hb_ratio - timer_active_vertices.time_elapsed * sb_to_hb_ratio;
	timer_nsp_application_final.print_timer("Application nsp final time is ");
	cout << "totalMessages = " << totalMessages << endl;
	for(unsigned int i = 0; i < NumOfAssociativeVCThreads; i++)
	{
		associativeVC_output_file[i].close();
	}
	for(unsigned int i = 0; i < NumOfGraphLoaderThreads; i++)
	{
		graph_loader_output_file[i].close();
	}
	for(unsigned int i = 0; i < NumOfIOThreads; i++)
	{
		IO_output_file[i].close();
	}
	for(unsigned int i = 0; i < NumOfUtilityThreads; i++)
	{
		Utility_output_file[i].close();
	}
	for(unsigned int i = 0; i < NumOfLogCacheThreads; i++)
	{
		Log_cache_output_file[i].close();
	}
	return;
}
