#include "header.h"

extern std::queue<struct IORequest> cache_to_IO_request_queue;
extern mutex cache_to_IO_request_queue_lock;
extern vector<ofstream> associativeVC_output_file;
extern vector<ofstream> partition_loader_output_file;
extern vector<ofstream> graph_loader_output_file;
extern vector<ofstream> Utility_output_file;
extern vector<ofstream> IO_output_file;

void Utils::Initialize(Asynchronous_IO *asyncIO)
{
	asynchronousIO = asyncIO;
	asynchronousIO->utility_object = this;
	numberOfPagesRequested_contiguous = 0;
	numberOfPagesReturned_contiguous = 0;
	numberOfPagesReturned = 0;
}

void Utils::loadContiguousFileToBuffer(int fileHandle, void *buffer, FILE_OFFSET_TYPE fileOffset/*in bytes*/, DATA_SIZE loadLength/*in bytes*/)
{
#if(DEBUG_CODE == 1)
	Utility_output_file[0] << "fO = " << fileOffset << " lL = " << loadLength << endl;
#endif//DEBUG_CODE
	numberOfPagesRequested_contiguous += ceil((loadLength*1.0) / SSD_PAGE_SIZE);
	PAGE_NUM_TYPE numberOfPages = ceil((loadLength*1.0) / SSD_PAGE_SIZE);
#if(STATS_PAGE_IN_EFFICIENCY == 1)
	total_number_pages_accessed += numberOfPages;
	total_number_edge_pages_accessed += numberOfPages;
#endif//STATS_PAGE_IN_EFFICIENCY
#if(DEBUG_CODE == 1)
	Utility_output_file[0] << "nP = " << numberOfPages << endl;
#endif//DEBUG_CODE
	//need to change this to request more data at a time
	FILE_OFFSET_TYPE startingOffset = fileOffset;
//	numberOfPagesReturned = 0;
	IORequest io_request;
	for(PAGE_NUM_TYPE loop_1 = 0; loop_1 < numberOfPages; loop_1++)
	{
		io_request.fileHandle = fileHandle;
		io_request.buffer = (void *)((char *)buffer + SSD_PAGE_SIZE * (BUFFER_SIZE) loop_1);
		io_request.isRead = 1;
		io_request.returnReqType = 3;
		io_request.file_offset = startingOffset + SSD_PAGE_SIZE * (FILE_OFFSET_TYPE)loop_1;
		//		cout << "Req no. = " << loop_1 << " file_offset = " << io_request.file_offset / SSD_PAGE_SIZE << endl;
		io_request.length = SSD_PAGE_SIZE > (loadLength - SSD_PAGE_SIZE * (FILE_OFFSET_TYPE)loop_1) ? (loadLength - SSD_PAGE_SIZE * (FILE_OFFSET_TYPE)loop_1) : SSD_PAGE_SIZE;
		cache_to_IO_request_queue_lock.lock();
		cache_to_IO_request_queue.push(io_request);
		cache_to_IO_request_queue_lock.unlock();
	}
#if(DEBUG_CODE == 1)
	Utility_output_file[0] << "Finished submitting requests tot = " << numberOfPages << endl;
#endif//DEBUG_CODE
}

bool Utils::is_loading_done()
{
	lock.lock();
	if(numberOfPagesReturned_contiguous == numberOfPagesRequested_contiguous) { 
		numberOfPagesRequested_contiguous = 0;
		numberOfPagesReturned_contiguous = 0;
		lock.unlock();
		return 1;
	}
	else {
		lock.unlock();
		return 0;
	}
}

void Utils::loadListPagesFromFileToBuffer(int fileHandle, void *buffer, BUFFER_SIZE &numberOfMessages, vector <struct page_info> &page_list, struct partition_cache_page_pointer &cachePartitionPtr, unsigned int isEdgeLog)
{
#if(DEBUG_CODE == 1)
	Utility_output_file[0] << "nP = " << page_list.size() << endl;
#endif//DEBUG_CODE
	numberOfMessages = 0;
	numberOfPagesReturned = 0;
	PAGE_NUM_TYPE numberOfPagesRequested = 0;
	IORequest io_request;
	PAGE_NUM_TYPE loop_1 = 0;
	for(vector<struct page_info>::iterator it1 = page_list.begin(); it1 != page_list.end(); it1++)
	{
		if(it1->isCached == 1)
		{
			if(it1->cpn != -1)
			{
				if(cachePartitionPtr.cpn == it1->cpn)
				{
					if(isEdgeLog == 0)
					{
						memcpy((char *)buffer + SSD_PAGE_SIZE * (BUFFER_SIZE) loop_1, (char *)(multi_log_cache->multi_log_cache[it1->cpn].data), cachePartitionPtr.pageTopPointer);
						numberOfMessages += (cachePartitionPtr.pageTopPointer)/(sizeof(UPDATE_MESSAGE));
					}
					else if(isEdgeLog == 2)
					{
#if(STRUCTURE_UPDATE == 1)
						memcpy((char *)buffer + SSD_PAGE_SIZE * (BUFFER_SIZE) loop_1, (char *)(structure_multi_log_cache->multi_log_cache[it1->cpn].data), cachePartitionPtr.pageTopPointer);
						numberOfMessages += cachePartitionPtr.pageTopPointer;
#endif//STRUCTURE_UPDATE
					}
					else {
#if(EDGE_LOG_PROCESSING == 1)
						memcpy((char *)buffer + SSD_PAGE_SIZE * (BUFFER_SIZE) loop_1, (char *)(edge_log_multi_log_cache->multi_log_cache[it1->cpn].data), cachePartitionPtr.pageTopPointer);
						numberOfMessages += cachePartitionPtr.pageTopPointer;
#else//EDGE_LOG_PROCESSING
						assert(0);
#endif//EDGE_LOG_PROCESSING
					}
#if(DEBUG_CODE == 1)
					//if(multi_log_cache->multi_log_context->current_iteration >= 5)
					//{
					Utility_output_file[0] << "nOM = " << numberOfMessages  << endl;
//					for(unsigned int loop_2 = 0; loop_2 < (cachePartitionPtr.pageTopPointer)/(sizeof(UPDATE_MESSAGE)); loop_2++)
//					{
//						Utility_output_file[0] << "l2 = " << loop_2 << " t = " << (((UPDATE_MESSAGE *)(multi_log_cache->multi_log_cache[it1->cpn].data))[loop_2]).target << " s = "  << (((UPDATE_MESSAGE *)(multi_log_cache->multi_log_cache[it1->cpn].data))[loop_2]).data.source << " d = " << (((UPDATE_MESSAGE *)(multi_log_cache->multi_log_cache[it1->cpn].data))[loop_2]).data.data << endl;
//					}
					//}
#endif//DEBUG_CODE
					if(isEdgeLog == 0)
					{
						multi_log_cache->list_update_lock.lock();
						multi_log_cache->freeCPN.push_back(it1->cpn);
#if(debug_code == 1)
						//if(multi_log_cache->multi_log_context->current_iteration >= 5)
						//{
						utility_output_file[0] << "f s = " << multi_log_cache->freeCPN.size() << endl;
						//}
#endif//debug_code
						multi_log_cache->list_update_lock.unlock();
						cachePartitionPtr.pageTopPointer = SSD_PAGE_SIZE;				cachePartitionPtr.cpn = -1;
					}
					else if(isEdgeLog == 2)
					{
#if(STRUCTURE_UPDATE == 1)
						structure_multi_log_cache->list_update_lock.lock();
						structure_multi_log_cache->freeCPN.push_back(it1->cpn);
#if(debug_code == 1)
						//if(multi_log_cache->multi_log_context->current_iteration >= 5)
						//{
						utility_output_file[0] << "s f s = " << structure_multi_log_cache->freeCPN.size() << endl;
						//}
#endif//debug_code
						structure_multi_log_cache->list_update_lock.unlock();
						cachePartitionPtr.pageTopPointer = SSD_PAGE_SIZE;				cachePartitionPtr.cpn = -1;
#endif//STRUCTURE_UPDATE
					}

				}
				else {
					if(isEdgeLog == 0)
					{
						memcpy((char *)buffer + SSD_PAGE_SIZE * (BUFFER_SIZE) loop_1, (char *)(multi_log_cache->multi_log_cache[it1->cpn].data), SSD_PAGE_SIZE);
						numberOfMessages += (SSD_PAGE_SIZE)/(sizeof(UPDATE_MESSAGE));
					} else if(isEdgeLog == 2)
					{
#if(STRUCTURE_UPDATE == 1)
						memcpy((char *)buffer + SSD_PAGE_SIZE * (BUFFER_SIZE) loop_1, (char *)(structure_multi_log_cache->multi_log_cache[it1->cpn].data), SSD_PAGE_SIZE);
						numberOfMessages += SSD_PAGE_SIZE;
#else//STRUCTURE_UPDATE == 1
						assert(0);
#endif//STRUCTURE_UPDATE == 1

					}
					else {
#if(EDGE_LOG_PROCESSING == 1)
						memcpy((char *)buffer + SSD_PAGE_SIZE * (BUFFER_SIZE) loop_1, (char *)(edge_log_multi_log_cache->multi_log_cache[it1->cpn].data), SSD_PAGE_SIZE);
						numberOfMessages += SSD_PAGE_SIZE;
#else//EDGE_LOG_PROCESSING == 1
						assert(0);
#endif//EDGE_LOG_PROCESSING == 1
					}
				}
			}
		}
		else {
			io_request.fileHandle = fileHandle;
			io_request.buffer = (void *)((char *)buffer + SSD_PAGE_SIZE * (BUFFER_SIZE) loop_1);
			io_request.cpn = it1->cpn;
			io_request.isRead = 1;
			io_request.returnReqType = 2;
			io_request.file_offset = it1->file_offset;
//			cout << "Req no. = " << loop_1 << " file_offset = " << io_request.file_offset / SSD_PAGE_SIZE << " " << io_request.file_offset % SSD_PAGE_SIZE << " c = " << it1->cpn  <<endl;
			io_request.length = SSD_PAGE_SIZE;
			cache_to_IO_request_queue_lock.lock();
			cache_to_IO_request_queue.push(io_request);
			cache_to_IO_request_queue_lock.unlock();
			if(isEdgeLog == 0)
			{
				numberOfMessages += (SSD_PAGE_SIZE)/(sizeof(UPDATE_MESSAGE));
			} else {
				numberOfMessages += SSD_PAGE_SIZE;
			}
#if(DEBUG_CODE == 1)
//if(multi_log_cache->multi_log_context->current_iteration >= 5)
//{
			Utility_output_file[0] << "nOM = " << numberOfMessages  << endl;
//}
#endif//DEBUG_CODE
			numberOfPagesRequested++;
#if(STATS_PAGE_IN_EFFICIENCY == 1)
			total_number_pages_accessed++;
#endif//STATS_PAGE_IN_EFFICIENCY
		}
		loop_1++;
	}
#if(DEBUG_CODE == 1)
	Utility_output_file[0] << "Finished submitting log requests tot = " << loop_1 << " nOPR = " << numberOfPagesRequested << endl;
#endif//DEBUG_CODE
	while(1)
	{
		lock.lock();
		if(numberOfPagesReturned == numberOfPagesRequested) { lock.unlock();break; }
		lock.unlock();
	}
}

void Utils::handleReturnRequest(struct IORequest io_req)
{
	lock.lock();
	numberOfPagesReturned++;
#if(DEBUG_CODE == 1)
	IO_output_file[0] << "nOPR = " << numberOfPagesReturned << " fo = " << io_req.file_offset / SSD_PAGE_SIZE << endl;
//	for(unsigned int loop_2 = 0; loop_2 < (SSD_PAGE_SIZE/sizeof(UPDATE_MESSAGE)); loop_2++)
//	{
//		IO_output_file[0] << "l2 = " << loop_2 << " t = " << (((UPDATE_MESSAGE *)(io_req.buffer))[loop_2]).target << " s = "  << (((UPDATE_MESSAGE *)(io_req.buffer))[loop_2]).data.source << " d = " << (((UPDATE_MESSAGE *)(io_req.buffer))[loop_2]).data.data  << endl;
//	}
#endif//DEBUG_CODE
	lock.unlock();
}
void Utils::handleReturnRequest2(struct IORequest io_req)
{
	lock.lock();
	numberOfPagesReturned_contiguous++;
#if(DEBUG_CODE == 1)
	IO_output_file[0] << "nOPR = " << numberOfPagesReturned_contiguous << " fo = " << io_req.file_offset / SSD_PAGE_SIZE << endl;
/*	for(unsigned int loop_2 = 0; loop_2 < (SSD_PAGE_SIZE/sizeof(UPDATE_MESSAGE)); loop_2++)
	{
		IO_output_file[0] << "l2 = " << loop_2 << " t = " << (((UPDATE_MESSAGE *)(io_req.buffer))[loop_2]).target << " d = "  << (((UPDATE_MESSAGE *)(io_req.buffer))[loop_2]).data << endl;
	}*/
#endif//DEBUG_CODE
	lock.unlock();
}

void Graph_data_loader::Initialize(std::string file1, class Utils *util_ptr, BUFFER_SIZE rowPtr_buffer_size/*in bytes*/, BUFFER_SIZE colPtr_buffer_size/*in bytes*/, VERTEX_TYPE VertexCount)
{
	for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
	{
		if(posix_memalign((void **)&(buffer[loop_1]), SSD_PAGE_SIZE, colPtr_buffer_size))
		{
			fprintf(stderr, "cannot allocate io payload for data_wr\n");
			exit(0);
		}
	}
	if(posix_memalign((void **)&(row_buffer), SSD_PAGE_SIZE, rowPtr_buffer_size))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
	rowBufferVertices = (rowPtr_buffer_size / sizeof(DATA_SIZE)) - 1;
	NumberOfVertices = VertexCount;

	vertexData_file_id = open(file1.data(), O_RDONLY | O_DIRECT);
	if (vertexData_file_id == -1) {
		perror(file1.data());
		exit(1);
	}
	vertexData_row_file_id = open(file1.data(), O_RDONLY);
	if (vertexData_row_file_id == -1) {
		perror(file1.data());
		exit(1);
	}
	logLoader = util_ptr;
	vertexFileOffset = -1;
	bufferTop = 0;
	availableBufferSpace = colPtr_buffer_size;
	colPtrBufferSize = colPtr_buffer_size;
	buffer_elements[0] = 0;
	buffer_elements[1] = 0;
	buffer_ready_bit[0] = 0;
	buffer_ready_bit[1] = 0;
	buffer_lock.resize(2);
	current_buffer = 0;
	done = 0;
#if(EDGE_LOG_MEASURE == 1)
	number_of_pages_requested = 0;
#endif//EDGE_LOG_MEASURE

#if(DEBUG_CODE == 1)
	graph_loader_output_file[0] << "rBV = " << rowBufferVertices << " NOV = " << NumberOfVertices << " vertexFileOffset = " << vertexFileOffset << " aBS = " << availableBufferSpace << " cPBS = " << colPtr_buffer_size << endl;
#endif//DEBUG_CODE
}

Graph_data_loader::~Graph_data_loader()
{
	close(vertexData_file_id);
	for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
	{
		free(buffer[loop_1]);
	}
	free(row_buffer);
	free(buffer_ready_bit);
	free(buffer_elements);
}

#if(PROCESS_EDGE_WEIGHT == 1)
inline DATA_SIZE Graph_data_loader::CALC_COLUMN_INDEX_OFFSET_IN_EDGE(DATA_SIZE columnIndex)
{
	return (columnIndex * sizeof(IN_EDGE_AND_WEIGHT_FILE_TYPE));
}
#endif//PROCESS_EDGE_WEIGHT

inline DATA_SIZE Graph_data_loader::CALC_COLUMN_INDEX_OFFSET(DATA_SIZE columnIndex)
{
	return (((DATA_SIZE)NumberOfVertices+1) * sizeof(DATA_SIZE) + columnIndex * sizeof(VERTEX_TYPE));
}

void Graph_data_loader::load_next_list(bool *vertex_active_vector, VERTEX_TYPE partition_start_vertex, PARTITION_SIZE activeVertexSize/*numberOfVertices*/)
{
#if(DEBUG_CODE == 1)
	graph_loader_output_file[0] << "p_s = " << partition_start_vertex << " aVS = " << activeVertexSize << endl;
#endif//DEBUG_CODE
	bool active_vetex_present = 0;
	struct bufferIndexPtr buffer_ptr;
	BUFFER_SIZE rowPtr_load_length;
	VERTEX_TYPE loop_start_row_vertex, rowPtr_load_length_vertices, verticesRequired;
	FILE_OFFSET_TYPE fileOffset;
	BUFFER_SIZE vertexColPtrSize;
	PAGE_NUM_TYPE numberOfRequiredPages;
	FILE_OFFSET_TYPE currentColPtrFileOffset;
	for(VERTEX_TYPE loop_1 = 0; loop_1 < activeVertexSize; )
	{
#if(STRUCTURE_UPDATE == 1)
		//load the update for the vertex here
		//update the vertex index pointer at every instance
#endif//STRUCTURE_UPDATE
		loop_start_row_vertex = partition_start_vertex + loop_1;
		verticesRequired = activeVertexSize - loop_1;
		rowPtr_load_length_vertices = (rowBufferVertices < verticesRequired) ? rowBufferVertices : verticesRequired;
		rowPtr_load_length = (BUFFER_SIZE)(rowPtr_load_length_vertices+1) * sizeof(DATA_SIZE);
		fileOffset = (((FILE_OFFSET_TYPE)loop_start_row_vertex*sizeof(DATA_SIZE)) / SSD_PAGE_SIZE) * SSD_PAGE_SIZE;
#if(DEBUG_CODE == 1)
		graph_loader_output_file[0] << "l_1 = " << loop_1 << " l_s_r_v = " << loop_start_row_vertex << " vR = " << verticesRequired << " r_l_l_v = " << rowPtr_load_length_vertices << " r_l_l = " << rowPtr_load_length << " fO = " << fileOffset << endl;
#endif//DEBUG_CODE
//		logLoader->loadContiguousFileToBuffer(vertexData_file_id, row_buffer, fileOffset, rowPtr_load_length);
		lseek64(vertexData_row_file_id, ((FILE_OFFSET_TYPE)loop_start_row_vertex*sizeof(DATA_SIZE)), SEEK_SET);
		read(vertexData_row_file_id, row_buffer, rowPtr_load_length);
#if(DEBUG_CODE == 1)
		graph_loader_output_file[0] << "After loading a row buffer" << endl;
		for(VERTEX_TYPE loop_2 = 0; loop_2 < rowPtr_load_length_vertices+1; loop_2++)
		{
//			graph_loader_output_file[0] << loop_2 << " = " << ((DATA_SIZE *)row_buffer)[loop_2] << "; ";
		}
//		graph_loader_output_file[0] << endl;
#endif//DEBUG_CODE
		for(VERTEX_TYPE loop_2 = 0; loop_2 < rowPtr_load_length_vertices; loop_2++)
		{
			if(vertex_active_vector[loop_1 + loop_2] == 1)
			{
#if(PAGE_RANK_THRESHOLD_DEBUG == 1)
logLoader->multi_log_cache->multi_log_context->num_active_vertices += 1;
#endif//PAGE_RANK_THRESHOLD_DEBUG
//				vertex_active_vector[loop_1 + loop_2] = 0;
				active_vetex_present = 1;
				vertexColPtrSize = ((DATA_SIZE *)row_buffer)[loop_2+1] - ((DATA_SIZE *)row_buffer)[loop_2];
				numberOfRequiredPages = ceil(((CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE + (DATA_SIZE)vertexColPtrSize*sizeof(VERTEX_TYPE))*1.0) / SSD_PAGE_SIZE);
				currentColPtrFileOffset = (CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) / SSD_PAGE_SIZE) * SSD_PAGE_SIZE;

#if(STATS_PAGE_IN_EFFICIENCY == 1)
				total_number_edge_list_size += vertexColPtrSize;
				for(FILE_OFFSET_TYPE loop_3 = currentColPtrFileOffset; loop_3 < CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2+1]); loop_3 += SSD_PAGE_SIZE)
				{
					FILE_OFFSET_TYPE min_index = max(loop_3, CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]));
					FILE_OFFSET_TYPE max_index = min(loop_3+SSD_PAGE_SIZE, CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2+1]));
					if(total_data_accessed_in_a_page.find(loop_3) == total_data_accessed_in_a_page.end())
					{
						total_data_accessed_in_a_page[loop_3] = 0;
						total_vertices_accessed_in_a_page[loop_3] = 0;
					}
					total_data_accessed_in_a_page[loop_3] += (max_index - min_index);
					total_vertices_accessed_in_a_page[loop_3] += 1;
				}
#endif//STATS_PAGE_IN_EFFICIENCY

#if(DEBUG_CODE == 1 || BFS_TESTING_1 == 1)
//if(loop_2 % 10 == 0)
	graph_loader_output_file[0] << "l1 = " <<  loop_1 << " l2 = " << loop_2 << " l1+l2 = "<< (loop_1+loop_2) << " rbl0 = " << ((DATA_SIZE *)row_buffer)[loop_2] << " rbl1 = " << ((DATA_SIZE *)row_buffer)[loop_2+1] << " l_2 = " << loop_2 << " vCPS = " << vertexColPtrSize << " nORP = " << numberOfRequiredPages << " cCPFO = " << currentColPtrFileOffset  << " vFO = " << vertexFileOffset << endl;
//				exit(0);
#endif//DEBUG_CODE || BFS_TESTING_1
				//Leaving it here - was adding output statements
				if(vertexColPtrSize == 0)
				{
#if(PROCESS_EDGE_WEIGHT == 0)
					buffer_ptr = {loop_start_row_vertex+loop_2, bufferTop, vertexColPtrSize};
#elif(PROCESS_EDGE_WEIGHT == 1)
					buffer_ptr = {loop_start_row_vertex+loop_2, bufferTop, vertexColPtrSize, CALC_COLUMN_INDEX_OFFSET_IN_EDGE(((DATA_SIZE *)row_buffer)[loop_2])};
#endif//PROCESS_EDGE_WEIGHT
#if(DEBUG_CODE == 1)
//if(loop_2 % 10 == 0)
					graph_loader_output_file[0] << "vI = " << buffer_ptr.vertex_id << " bT = " << buffer_ptr.start_index << " s = " << buffer_ptr.size << endl;
#endif//DEBUG_CODE
					vertex_index_list[current_buffer].push_back(buffer_ptr);
					buffer_elements[current_buffer] += 1;
					continue;
				}
				else if((vertexFileOffset != -1) && (vertexFileOffset == currentColPtrFileOffset) && (numberOfRequiredPages == 1))
				{
#if(PROCESS_EDGE_WEIGHT == 0)
					buffer_ptr = {loop_start_row_vertex+loop_2,bufferTop - SSD_PAGE_SIZE + CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE, vertexColPtrSize};
#elif(PROCESS_EDGE_WEIGHT == 1)
					buffer_ptr = {loop_start_row_vertex+loop_2,bufferTop - SSD_PAGE_SIZE + CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE, vertexColPtrSize, CALC_COLUMN_INDEX_OFFSET_IN_EDGE(((DATA_SIZE *)row_buffer)[loop_2])};
#endif//PROCESS_EDGE_WEIGHT
#if(DEBUG_CODE == 1)
//if(loop_2 % 10 == 0)
					graph_loader_output_file[0] << "vI = " << buffer_ptr.vertex_id << " bT = " << buffer_ptr.start_index << " s = " << buffer_ptr.size << endl;
#endif//DEBUG_CODE
					vertex_index_list[current_buffer].push_back(buffer_ptr);
					buffer_elements[current_buffer] += 1;
					continue;
				}
				if(availableBufferSpace >= ((BUFFER_SIZE)numberOfRequiredPages*SSD_PAGE_SIZE))
				{
#if(DEBUG_CODE == 1)
//					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG_CODE
#if(MEASURE == 1)
					logLoader->multi_log_cache->multi_log_context->timer_column_index_iteration.start_timer();
#endif//MEASURE
					logLoader->loadContiguousFileToBuffer(vertexData_file_id, (char *)(buffer[current_buffer]) + bufferTop, currentColPtrFileOffset, (DATA_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE);
#if(EDGE_LOG_MEASURE == 1)
					number_of_pages_requested += numberOfRequiredPages;
#endif//EDGE_LOG_MEASURE
#if(MEASURE == 1)
					logLoader->multi_log_cache->multi_log_context->timer_column_index_iteration.end_timer();
#endif//MEASURE
#if(DEBUG_CODE == 1)
//					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG_CODE
#if(PROCESS_EDGE_WEIGHT == 0)
					buffer_ptr = {loop_start_row_vertex+loop_2, bufferTop + CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE, vertexColPtrSize};
#elif(PROCESS_EDGE_WEIGHT == 1)
					buffer_ptr = {loop_start_row_vertex+loop_2, bufferTop + CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE, vertexColPtrSize, CALC_COLUMN_INDEX_OFFSET_IN_EDGE(((DATA_SIZE *)row_buffer)[loop_2])};
#endif//PROCESS_EDGE_WEIGHT
#if(DEBUG_CODE == 1)
//					graph_loader_output_file[0] << "vI = " << buffer_ptr.vertex_id << " bT = " << buffer_ptr.start_index << " s = " << buffer_ptr.size << endl;
#endif//DEBUG_CODE
#if(DEBUG_CODE == 1)
//if(loop_2 % 10 == 0)
					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG_CODE
					vertex_index_list[current_buffer].push_back(buffer_ptr);
#if(DEBUG_CODE == 1)
//					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG_CODE
					buffer_elements[current_buffer] += 1;
#if(DEBUG_CODE == 1)
//					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG_CODE
					bufferTop += (BUFFER_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE;
#if(DEBUG_CODE == 1)
//					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG_CODE
					availableBufferSpace -= ((BUFFER_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE);
#if(DEBUG_CODE == 1)
//					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG_CODE
					vertexFileOffset = currentColPtrFileOffset + (DATA_SIZE)(numberOfRequiredPages-1) * SSD_PAGE_SIZE;
#if(DEBUG_CODE == 1)
//					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG_CODE
				}
				else {
#if(DEBUG_CODE == 1)
					graph_loader_output_file[0] << "Sending a buffer from graph data loader" << endl;
#endif//DEBUG_CODE
					while(logLoader->is_loading_done() == 0){}
					buffer_lock[current_buffer].lock();
					buffer_ready_bit[current_buffer] = 1;
					buffer_lock[current_buffer].unlock();
					current_buffer = (current_buffer + 1) % 2;
					while(1)
					{
						buffer_lock[current_buffer].lock();
						if(buffer_ready_bit[current_buffer] == 0)
						{
							buffer_lock[current_buffer].unlock();
							break;
						}
						buffer_lock[current_buffer].unlock();
					}
#if(MEASURE == 1)
					logLoader->multi_log_cache->multi_log_context->timer_column_index_iteration.start_timer();
#endif//MEASURE
					logLoader->loadContiguousFileToBuffer(vertexData_file_id, buffer[current_buffer], currentColPtrFileOffset, (BUFFER_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE);
#if(EDGE_LOG_MEASURE == 1)
					number_of_pages_requested += numberOfRequiredPages;
#endif//EDGE_LOG_MEASURE
#if(MEASURE == 1)
					logLoader->multi_log_cache->multi_log_context->timer_column_index_iteration.end_timer();
#endif//MEASURE
#if(PROCESS_EDGE_WEIGHT == 0)
					buffer_ptr = {loop_start_row_vertex+loop_2, CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE, vertexColPtrSize};
#elif(PROCESS_EDGE_WEIGHT == 1)
					buffer_ptr = {loop_start_row_vertex+loop_2, CALC_COLUMN_INDEX_OFFSET(((DATA_SIZE *)row_buffer)[loop_2]) % SSD_PAGE_SIZE, vertexColPtrSize, CALC_COLUMN_INDEX_OFFSET_IN_EDGE(((DATA_SIZE *)row_buffer)[loop_2])};
#endif//PROCESS_EDGE_WEIGHT
#if(DEBUG_CODE == 1)
//if(loop_2 % 10 == 0)
					graph_loader_output_file[0] << "vI = " << buffer_ptr.vertex_id << " bT = " << buffer_ptr.start_index << " s = " << buffer_ptr.size << endl;
#endif//DEBUG_CODE
					vertex_index_list[current_buffer].push_back(buffer_ptr);
					buffer_elements[current_buffer] += 1;
					bufferTop = numberOfRequiredPages * SSD_PAGE_SIZE;
					availableBufferSpace = colPtrBufferSize - ((BUFFER_SIZE)numberOfRequiredPages * SSD_PAGE_SIZE);
					vertexFileOffset = currentColPtrFileOffset + ((DATA_SIZE)numberOfRequiredPages-1) * SSD_PAGE_SIZE;
#if(DEBUG_CODE == 1)
//					graph_loader_output_file[0] << "b_e = " << buffer_elements[current_buffer] << " bT = " << bufferTop << " aBS = " << availableBufferSpace << " vFO = " << vertexFileOffset << endl;
#endif//DEBUG_CODE
				}
			}
		}
		loop_1 += rowPtr_load_length_vertices;
	}
	while(logLoader->is_loading_done() == 0){}
	buffer_lock[current_buffer].lock();
	if(active_vetex_present == 1)
	{
#if(DEBUG_CODE == 1)
		graph_loader_output_file[0] << "Sending last buffer in the partition cb = " << current_buffer <<endl;
#endif//DEBUG_CODE
		buffer_ready_bit[current_buffer] = 1;
	}
	done_lock.lock();
	done=1;
	done_lock.unlock();
	buffer_lock[current_buffer].unlock();
	if(active_vetex_present == 1)
	{
		current_buffer = (current_buffer + 1) % 2;
	}
	bufferTop = 0;//Left here
	vertexFileOffset = -1;
}

void AssocativeMultiLogVC::Initialize(MultiLogContext *multiLogContext, Graph_data_loader *ptr, Utils *util_ptr, BUFFER_SIZE bufferElementsSize/*in bytes*/, VERTEX_TYPE VertexCount, BUFFER_SIZE updateMessageBufferSize/*in bytes*/)
{
	multi_log_context = multiLogContext;
	graph_data = ptr;
	logLoader = util_ptr;
	maxElementsInBuffer = bufferElementsSize / sizeof(DATA_SIZE);
#if(TESTING == 0)
	multi_log_context->numOfPartitions = ceil((VertexCount*1.0) / maxElementsInBuffer);
#else
	multi_log_context->numOfPartitions = 2;//for testing
#endif//TESTING
	NumOfVertices = VertexCount;
	current_buffer = 0;
/*	if(posix_memalign((void **)&partition_vertex_values, SSD_PAGE_SIZE, maxElementsInBuffer * sizeof(VERTEX_VALUE_TYPE)*16))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
*/
	partition_vertex_values = malloc(maxElementsInBuffer * sizeof(VERTEX_VALUE_TYPE));
	if(posix_memalign((void **)&partition_update_messages_buffer, SSD_PAGE_SIZE, updateMessageBufferSize))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}

//	partition_update_messages_buffer = malloc(updateMessageBufferSize);
	maxSizeInBuffer_messages = updateMessageBufferSize;
#if(DEBUG_CODE == 1)
	associativeVC_output_file[0] << "mEIB = " << maxElementsInBuffer << " nOP = " << multi_log_context->numOfPartitions << " NOV = " << NumOfVertices << " uMBS = " << updateMessageBufferSize << " mIBm = " << maxSizeInBuffer_messages << endl;
#endif//DEBUG_CODE
}

void AssocativeMultiLogVC::run()
{
#if(MEASURE == 1)
	multi_log_context->iterationMessages = 0;
	multi_log_context->timer_before_iteration.start_timer();
#endif//MEASURE
	(dynamic_cast<AssociativeProgram*>(multi_log_context->program))->before_iteration(multi_log_context->current_iteration, multi_log_context);
#if(MEASURE == 1)
	multi_log_context->timer_before_iteration.end_timer();
#endif//MEASURE
	PARTITION_NUM_TYPE numOfPartitions_processed = 0;
	/*	bool *active_list;
		if(posix_memalign((void **)&active_list, SSD_PAGE_SIZE, sizeof(bool) * maxElementsInBuffer))
		{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
		}
	 */	bool *active_list;
	active_list = (bool *)malloc(sizeof(bool) * maxElementsInBuffer);
	/*	for(BUFFER_SIZE loop_1 = 0; loop_1 < maxElementsInBuffer; loop_1++)
		{
		active_list[loop_1] = 1;
		}*/
#if(DEBUG_CODE == 1)
	associativeVC_output_file[0] << "mEIB = " << maxElementsInBuffer <<endl;
#endif//DEBUG_CODE
	while(numOfPartitions_processed < multi_log_context->numOfPartitions) {//all the partitions are done
		VERTEX_TYPE startingVertexIndexOfPartition = numOfPartitions_processed * maxElementsInBuffer;
		PARTITION_SIZE activeVertexSize = maxElementsInBuffer < (NumOfVertices - numOfPartitions_processed * maxElementsInBuffer)? maxElementsInBuffer:(NumOfVertices - numOfPartitions_processed*maxElementsInBuffer);
#if(DEBUG_CODE == 1)
		associativeVC_output_file[0] << "nPp = " << numOfPartitions_processed << " sVIP = " << startingVertexIndexOfPartition << " aVS = " << activeVertexSize << endl;
#endif//DEBUG_CODE
		//		logLoader->loadContiguousFileToBuffer(multi_log_context->graph_info.vertexValue_file_id, partition_vertex_values, startingVertexIndexOfPartition * sizeof(VERTEX_VALUE_TYPE), activeVertexSize * sizeof(VERTEX_VALUE_TYPE));
		//		VERTEX_VALUE_TYPE *visited = (VERTEX_VALUE_TYPE *)calloc(multi_log_context->graph_info.NumNodes,sizeof(VERTEX_VALUE_TYPE)); 
		//	lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);        read(multi_log_context->graph_info.vertexValue_file_id, (void *)visited, multi_log_context->graph_info.NumNodes * sizeof(VERTEX_VALUE_TYPE));
		//		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		//		read(multi_log_context->graph_info.vertexValue_file_id, (void *)visited,1024 * sizeof(VERTEX_VALUE_TYPE));
		//		for(BUFFER_SIZE loop_1 = 0; loop_1 < 1024;  loop_1++)
		//		{
		//			associativeVC_output_file[0] << loop_1 << " = " << ((VERTEX_VALUE_TYPE *)visited)[loop_1] << " " << "; "; 
		//		}
		//		associativeVC_output_file[0] << endl;
		//		free(visited);
		//		associativeVC_output_file[0] << "mEIB = " << maxElementsInBuffer << endl;
		//		memset(partition_vertex_values, 0, maxElementsInBuffer * sizeof(VERTEX_VALUE_TYPE));
#if(MEASURE == 1)
		multi_log_context->timer_value_access.start_timer();
#endif//MEASURE
		lseek64(multi_log_context->graph_info.vertexValue_file_id, (FILE_OFFSET_TYPE)startingVertexIndexOfPartition * sizeof(VERTEX_VALUE_TYPE), SEEK_SET);
		//		lseek64(multi_log_context->graph_info.vertexValue_file_id, 0, SEEK_SET);
		read(multi_log_context->graph_info.vertexValue_file_id, (void *)partition_vertex_values, (FILE_OFFSET_TYPE)activeVertexSize * sizeof(VERTEX_VALUE_TYPE));
#if(MEASURE == 1)
		multi_log_context->timer_value_access.end_timer();
#endif//MEASURE
		//		partition_vertex_values = (VERTEX_VALUE_TYPE *)calloc(maxElementsInBuffer,sizeof(VERTEX_VALUE_TYPE));
		//		read(multi_log_context->graph_info.vertexValue_file_id, (void *)partition_vertex_values, 1024 * sizeof(VERTEX_VALUE_TYPE));

		//		for(BUFFER_SIZE loop_1 = 0; loop_1 < 1024; loop_1++)
		//		{
		//			associativeVC_output_file[0] << loop_1 << " = " << ((VERTEX_VALUE_TYPE *)partition_vertex_values)[loop_1] << "; ";
		//		}
		//		associativeVC_output_file[0] << endl;

		/*----------------Partition streaming - BEGIN ---------------------*/
		//		logLoader->loadContiguousFileToBuffer(multi_log_context->graph_info.vertexActive_file_id, active_list, startingVertexIndexOfPartition * sizeof(bool), activeVertexSize * sizeof(bool));
		/*		memset(active_list, 0, activeVertexSize * sizeof(bool));
				if(multi_log_context->current_iteration == 0)
				{
				if(startingVertexIndexOfPartition == 0)
				{
				active_list[0] = 1;
				}
				}
		 */		
		//			cout << "errno = "<< errno << endl;
#if(MEASURE == 1)
		multi_log_context->timer_active_vertices.start_timer();
#endif//MEASURE
		if(lseek64(multi_log_context->graph_info.vertexActive_file_id, ((FILE_OFFSET_TYPE)startingVertexIndexOfPartition) * sizeof(bool), SEEK_SET) < 0)
		{
			cout << "Error with lseek" << endl;
			exit(0);
		}
		//			cout << "errno = "<< errno << endl;
		if(read(multi_log_context->graph_info.vertexActive_file_id, active_list, (FILE_OFFSET_TYPE)activeVertexSize * sizeof(bool)) != ((FILE_OFFSET_TYPE)activeVertexSize * sizeof(bool)))
		{
			cout << "errno = "<< errno << endl;
			perror("active vertices file");
			cout << "Error with read" << endl;
			exit(0);
		}
#if(MEASURE == 1)
		multi_log_context->timer_active_vertices.end_timer();
#endif//MEASURE
#if(DEBUG_CODE == 1)
		associativeVC_output_file[0] << "Active vertex list start v file id = " << multi_log_context->graph_info.vertexValue_file_id << " A file id = "<<multi_log_context->graph_info.vertexActive_file_id << endl;
		associativeVC_output_file[0] << "aVS = " << activeVertexSize << endl;
		for(BUFFER_SIZE loop_1 = 0; loop_1 < activeVertexSize; loop_1++)
		{
			if(active_list[loop_1] == 1)
			{
//				associativeVC_output_file[0] << loop_1 << " = " << active_list[loop_1] << " " << ((VERTEX_VALUE_TYPE *)partition_vertex_values)[loop_1] << "; ";
			}
		}
		associativeVC_output_file[0] << "\nActive vertex list end" << endl;
#endif//DEBUG_CODE
		if(multi_log_context->current_iteration != 0)
		{
			vector<struct page_info> page_list;
			BUFFER_SIZE log_space_usage;
			log_space_usage = 0;
			BUFFER_SIZE numberOfMessages;
			//For now fetch in multiples of page only
			for(list<struct page_info>::iterator it1 = (multi_log_cache->partition_page_info)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed].begin(); it1 != (multi_log_cache->partition_page_info)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed].end(); it1++)
			{
				log_space_usage += SSD_PAGE_SIZE;
				page_list.push_back(*it1);
#if(DEBUG_CODE == 1)
				associativeVC_output_file[0] << "lsu = " << log_space_usage << " it1 fo = " << (*it1).file_offset << " it1 cpn = " << (*it1).cpn << " it1 iC = " << (*it1).isCached << endl;
#endif//DEBUG_CODE
				if(log_space_usage >= maxSizeInBuffer_messages)
				{
#if(MEASURE == 1)
					multi_log_context->timer_log_loading.start_timer();
#endif//MEASURE
					logLoader->loadListPagesFromFileToBuffer(multi_log_cache->log_file_id[((multi_log_cache->current_partition_file) + 1) % 2], (void *)(partition_update_messages_buffer), numberOfMessages, page_list, (multi_log_cache->vertexToPartitionPage)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed], 0);
#if(MEASURE == 1)
					multi_log_context->timer_log_loading.end_timer();
#endif//MEASURE
					page_list.clear();
					log_space_usage = 0;
#if(MEASURE == 1)
					multi_log_context->iterationMessages += numberOfMessages;
					multi_log_context->timer_log_processing.start_timer();
#endif//MEASURE
					for(BUFFER_SIZE loop_2 = 0; loop_2 < numberOfMessages; loop_2++)
					{
//						(dynamic_cast<AssociativeProgram*>(multi_log_context->program))->combine_message(((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target, ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].data, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition], &(active_list[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition]), multi_log_context);
						Application_function_combine(((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target, ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].data, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition], &(active_list[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition]), multi_log_context);
//						((VERTEX_VALUE_TYPE *)partition_vertex_values)[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition] += ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].data;
					}
#if(MEASURE == 1)
					multi_log_context->timer_log_processing.end_timer();
#endif//MEASURE
				}
			}
			if(log_space_usage > 0)
			{
#if(DEBUG_CODE == 1)
				associativeVC_output_file[0] << "cpn = " << (multi_log_cache->vertexToPartitionPage)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed].cpn << " pTP = " << (multi_log_cache->vertexToPartitionPage)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed].pageTopPointer << endl;
				if((multi_log_cache->vertexToPartitionPage)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed].cpn == 1092)
				{
					/*					for(BUFFER_SIZE loop_2 = 0; loop_2 < ((multi_log_cache->vertexToPartitionPage)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed].pageTopPointer)/sizeof(UPDATE_MESSAGE); loop_2++)
										{
										associativeVC_output_file[0] <<  "l2 = " << loop_2 << " t = " <<  ((UPDATE_MESSAGE *)(multi_log_cache->multi_log_cache[((multi_log_cache->vertexToPartitionPage)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed].cpn)]).data)[loop_2].target << " d = " << ((UPDATE_MESSAGE *)(multi_log_cache->multi_log_cache[((multi_log_cache->vertexToPartitionPage)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed].cpn)]).data)[loop_2].data << endl;
										}
					 */				}
				associativeVC_output_file[0] << "Before load pages" << endl;
#endif//DEBUG_CODE
#if(MEASURE == 1)
				multi_log_context->timer_log_loading.start_timer();
#endif//MEASURE
				logLoader->loadListPagesFromFileToBuffer(multi_log_cache->log_file_id[((multi_log_cache->current_partition_file) + 1) % 2], (void *)(partition_update_messages_buffer), numberOfMessages, page_list, (multi_log_cache->vertexToPartitionPage)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed], 0);
#if(MEASURE == 1)
				multi_log_context->timer_log_loading.end_timer();
#endif//MEASURE
				page_list.clear();
#if(DEBUG_CODE == 1)
				associativeVC_output_file[0] << "lsu = " << log_space_usage << endl << " nOM = " << numberOfMessages << endl;;
#endif//DEBUG_CODE
				log_space_usage = 0;
#if(MEASURE == 1)
				multi_log_context->iterationMessages += numberOfMessages;
				multi_log_context->timer_log_processing.start_timer();
#endif//MEASURE
				for(BUFFER_SIZE loop_2 = 0; loop_2 < numberOfMessages; loop_2++)
				{
#if(DEBUG_CODE == 1)
					//					                associativeVC_output_file[0] <<  "l2 = " << loop_2 << " t = " << ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target << endl;
					//							associativeVC_output_file[0] <<  "l2 = " << loop_2 << " t = " << ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target << " d = " <<  ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].data << endl;
					//							associativeVC_output_file[0] <<  "l2 = " << loop_2 << " t = " << ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target << " d = " <<  ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].data << " v = " << ((VERTEX_VALUE_TYPE *)partition_vertex_values)[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition] << endl;
//					associativeVC_output_file[0] <<  "l2 = " << loop_2 << " t = " << ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target << " d = " <<  ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].data << " v = " << ((VERTEX_VALUE_TYPE *)partition_vertex_values)[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition] << " a = " << active_list[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition] << endl;
#endif//DEBUG_CODE
//					(dynamic_cast<AssociativeProgram*>(multi_log_context->program))->combine_message(((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target, ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].data, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition], &(active_list[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition]), multi_log_context);
					Application_function_combine(((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target, ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].data, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition], &(active_list[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition]), multi_log_context);
//					((VERTEX_VALUE_TYPE *)partition_vertex_values)[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].target - startingVertexIndexOfPartition] += ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_2].data;
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] <<  "l2 = " << loop_2 << endl;
#endif//DEBUG_CODE
				}
#if(MEASURE == 1)
				multi_log_context->timer_log_processing.end_timer();
#endif//MEASURE
			}
			(multi_log_cache->partition_page_info)[((multi_log_cache->current_partition_file) + 1) % 2][numOfPartitions_processed].clear();
		}
		/*
#pragma omp parallel num_threads(2)
		{
			unsigned int tid = omp_get_thread_num();
			if(tid == 0)
			{
				log_loader->load_next_partition(startingVertexIndexOfPartition, activeVertexSize);
			}
			else {
				bool exit;
				while(1) {//all the buffers are done
					exit = 0;
					while(1){
						((log_loader->buffer_lock)[log_current_buffer]).lock();
						if((log_loader->buffer_ready_bit)[log_current_buffer] == 1)
						{
							((log_loader->buffer_lock)[log_current_buffer]).unlock();
							break;
						}
						else {
#if(DEBUG_CODE == 1)
				//			associativeVC_output_file[0] << "cb = " << current_buffer << endl;
#endif//DEBUG_CODE
							log_loader->done_lock.lock();
							if(log_loader->done == 1) {
								log_loader->done = 0;
#if(DEBUG_CODE == 1)
				//				associativeVC_output_file[0] << "done cb = " << current_buffer << endl;
#endif//DEBUG_CODE
								exit = 1;
							}
							log_loader->done_lock.unlock();
						}
						((log_loader->buffer_lock)[log_current_buffer]).unlock();
						if(exit == 1) break;
					}
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] << "lcb = " << log_current_buffer << endl;
					associativeVC_output_file[0] << "Received a log buffer from graph data loader" << endl;
#endif//DEBUG_CODE
					if(exit == 1) break;
					BUFFER_SIZE log_numberOfElementsInBuffer = (log_loader->buffer_elements[log_current_buffer]);
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] << "lnOEIB = " << log_numberOfElementsInBuffer << endl;
#endif//DEBUG_CODE
//#pragma omp parallel for
					for(BUFFER_SIZE loop_2 = 0; loop_2 < log_numberOfElementsInBuffer; loop_2++)
					{
							associativeVC_output_file[0] << " " << ((VERTEX_TYPE *)((char *)((graph_data->buffer)[current_buffer]) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index))[loop_3];
					}
					((log_loader->buffer_lock)[log_current_buffer]).lock();
					(log_loader->buffer_ready_bit)[log_current_buffer] = 0;
					((log_loader->buffer_lock)[log_current_buffer]).unlock();
					log_current_buffer = (log_current_buffer + 1) % 2;
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] << "cn = " << log_current_buffer << endl;
					associativeVC_output_file[0] << "Finished processing log buffer in associative VC" << endl;
#endif//DEBUG_CODE
				}
			}
		}*/
/*----------------Partition streaming - END ---------------------*/
#if(DEBUG_CODE == 1)
		for(BUFFER_SIZE loop_1 = 0; loop_1 < activeVertexSize; loop_1++)
		{
			if(active_list[loop_1] == 1)
			{
//				associativeVC_output_file[0] << loop_1 << " = " << active_list[loop_1] << " " << ((VERTEX_VALUE_TYPE *)partition_vertex_values)[loop_1] << "; ";
			}
		}
		associativeVC_output_file[0] << "PSEND" << endl;
#endif//DEBUG_CODE
		UPDATE_MESSAGE result_data;
		numOfPartitions_processed++;
#if(MEASURE == 1)
		multi_log_context->timer_log_generation.start_timer();
#endif//MEASURE
#pragma omp parallel num_threads(2)
		{
			unsigned int tid = omp_get_thread_num();
			if(tid == 0)
			{
				graph_data->load_next_list(active_list, startingVertexIndexOfPartition, activeVertexSize);
			}
			else {
				bool exit;
				while(1) {//all the buffers are done
					exit = 0;
#if(MEASURE == 1)
					multi_log_context->timer_graph_access.start_timer();
#endif//MEASURE
					while(1){
						((graph_data->buffer_lock)[current_buffer]).lock();
						if((graph_data->buffer_ready_bit)[current_buffer] == 1)
						{
							((graph_data->buffer_lock)[current_buffer]).unlock();
							break;
						}
						else {
#if(DEBUG_CODE == 1)
							//			associativeVC_output_file[0] << "cb = " << current_buffer << endl;
#endif//DEBUG_CODE
							graph_data->done_lock.lock();
							if(graph_data->done == 1) {
								graph_data->done = 0;
#if(DEBUG_CODE == 1)
								//				associativeVC_output_file[0] << "done cb = " << current_buffer << endl;
#endif//DEBUG_CODE
								exit = 1;
							}
							graph_data->done_lock.unlock();
						}
						((graph_data->buffer_lock)[current_buffer]).unlock();
						if(exit == 1) break;
					}
#if(MEASURE == 1)
					multi_log_context->timer_graph_access.end_timer();
#endif//MEASURE
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] << "cb = " << current_buffer << endl;
					associativeVC_output_file[0] << "Received a buffer from graph data loader" << endl;
#endif//DEBUG_CODE
					if(exit == 1) break;
					BUFFER_SIZE numberOfElementsInBuffer = (graph_data->buffer_elements[current_buffer]);
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] << "nOEIB = " << numberOfElementsInBuffer << endl;
#endif//DEBUG_CODE
					//#pragma omp parallel for
#if(MEASURE == 1)
					multi_log_context->timer_edge_program.start_timer();
#endif//MEASURE
					for(BUFFER_SIZE loop_2 = 0; loop_2 < numberOfElementsInBuffer; loop_2++)
					{
#if(DEBUG_CODE == 1)
						associativeVC_output_file[0] << "vi = " << graph_data->vertex_index_list[current_buffer][loop_2].vertex_id << " si = " << (graph_data->vertex_index_list)[current_buffer][loop_2].start_index << " s = " << graph_data->vertex_index_list[current_buffer][loop_2].size << endl;
#endif//DEBUG_CODE
						multi_log_context->timer_edge_processing.start_timer();
#if(EDGE_PROGRAM_OPTIMIZATION == 0)
						EDGE_LIST edge_list;
						for(VERTEX_TYPE loop_3 = 0; loop_3 < graph_data->vertex_index_list[current_buffer][loop_2].size; loop_3++)
						{
							edge_list.push_back(((VERTEX_TYPE *)((char *)((graph_data->buffer)[current_buffer]) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index))[loop_3]);
						}
#if(DEBUG_CODE == 1)
						//						associativeVC_output_file[0] << "a el " << endl;
#endif//DEBUG_CODE

						EDGE_MESSAGES_TYPE result_data;
						(dynamic_cast<AssociativeProgram*>(multi_log_context->program))->edge_program_lists(graph_data->vertex_index_list[current_buffer][loop_2].vertex_id, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition], edge_list, result_data, multi_log_context);
#else//EDGE_PROGRAM_OPTIMIZATION
#if(ASSOCIATIVE_PROGRAM == 1)
						multi_log_context->program_edge_processing->edge_program_lists(graph_data->vertex_index_list[current_buffer][loop_2].vertex_id, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition], ((VERTEX_TYPE *)((char *)((graph_data->buffer)[current_buffer]) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index)), graph_data->vertex_index_list[current_buffer][loop_2].size, multi_log_context);
#endif//ASSOCIATIVE_PROGRAM					
#endif//EDGE_PROGRAM_OPTIMIZATION
						multi_log_context->timer_edge_processing.end_timer();
#if(DEBUG_CODE == 1)
						//						associativeVC_output_file[0] << "a epl " << endl;
#endif//DEBUG_CODE
#if(EDGE_PROGRAM_OPTIMIZATION == 0)
						multi_log_context->timer_multi_log_access.start_timer();
						for(VERTEX_TYPE loop_3 = 0; loop_3 < result_data.size(); loop_3++)
						{
							UPDATE_MESSAGE message_to_send;
							message_to_send.target = edge_list[loop_3];
							message_to_send.data = result_data[loop_3];
#if(DEBUG_CODE == 1)
							//							associativeVC_output_file[0] << "l3 = " << loop_3 << " t = " << message_to_send.target << " d = " << message_to_send.data << endl;
#endif//DEBUG_CODE
							multi_log_cache->send_message((char *)(&message_to_send), sizeof(UPDATE_MESSAGE));
						}
						multi_log_context->timer_multi_log_access.end_timer();
#if(DEBUG_CODE == 1)
						//						associativeVC_output_file[0] << "a sm " << endl;
#endif//DEBUG_CODE
						edge_list.clear();
						result_data.clear();
#endif//EDGE_PROGRAM_OPTIMIZATION
					}
#if(MEASURE == 1)
					multi_log_context->timer_edge_program.end_timer();
#endif//MEASURE

					graph_data->vertex_index_list[current_buffer].clear();
					graph_data->buffer_elements[current_buffer] = 0;
					((graph_data->buffer_lock)[current_buffer]).lock();
					(graph_data->buffer_ready_bit)[current_buffer] = 0;
					((graph_data->buffer_lock)[current_buffer]).unlock();
					current_buffer = (current_buffer + 1) % 2;
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] << "cn = " << current_buffer << endl;
					associativeVC_output_file[0] << "Finished processing buffer in associative VC" << endl;
#endif//DEBUG_CODE
				}
			}
		}
#if(MEASURE == 1)
		multi_log_context->timer_log_generation.end_timer();
#endif//MEASURE
#if(MEASURE == 1)
		multi_log_context->timer_value_access.start_timer();
#endif//MEASURE
		lseek64(multi_log_context->graph_info.vertexValue_file_id, ((DATA_SIZE)startingVertexIndexOfPartition) * sizeof(VERTEX_VALUE_TYPE), SEEK_SET);
		write(multi_log_context->graph_info.vertexValue_file_id, partition_vertex_values, ((BUFFER_SIZE)activeVertexSize) * sizeof(VERTEX_VALUE_TYPE));
#if(MEASURE == 1)
		multi_log_context->timer_value_access.end_timer();
#endif//MEASURE
	}
#if(DEBUG_CODE == 1)
	associativeVC_output_file[0] << "Done with VC" << endl;
#endif//DEBUG_CODE
	free(active_list);
	/*---------------------Update multi-log----------------*/
	multi_log_cache->currentFileTop[((multi_log_cache->current_partition_file) + 1) % 2] = 0;
	multi_log_cache->current_partition_file = (multi_log_cache->current_partition_file+1)%2;
	//	for(unsigned int i = 0; i < NumOfUtilityThreads; i++)
	//	{
	//		Utility_output_file[i].close();
	//	}
	//	free(partition_update_messages_buffer);
#if(MEASURE == 1)
	multi_log_context->timer_after_iteration.start_timer();
#endif//MEASURE
	(dynamic_cast<AssociativeProgram*>(multi_log_context->program))->after_iteration(multi_log_context->current_iteration, multi_log_context);
#if(MEASURE == 1)
	multi_log_context->timer_after_iteration.end_timer();
#endif//MEASURE
}

inline void AssocativeMultiLogVC::Application_function_combine(VERTEX_TYPE vertex_id, EDGE_MESSAGE_TYPE &edge_message, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context)
{
	if(multi_log_context->application_type == PAGE_RANK_APPLICATION)
	{
#if(PAGE_RANK_COMPILE == 1)
		vertex_value += edge_message;
#endif//PAGE_RANK_COMPILE
	}
	else if(multi_log_context->application_type == PAGE_RANK_THRESHOLD_APPLICATION)
	{
#if(PAGE_RANK_THRESHOLD_COMPILE == 1)
		if((fabsf(edge_message) > 10.0) || (multi_log_context->current_iteration == 1))
		{
			*active_list = 1;
			if(multi_log_context->current_iteration != 1)
			{
				edge_message -= 10.0;
			}
		}
		vertex_value.change += edge_message;
#endif//PAGE_RANK_THRESHOLD_COMPILE
	}
	else if(multi_log_context->application_type == BFS_APPLICATION)
	{
#if(BFS_COMPILE == 1)
		if(vertex_id == multi_log_context->program->target)
		{
			multi_log_context->program->converged = true;
		}
		else {
			if(vertex_value == 0)
			{
				*active_list = 1;
				multi_log_context->program->hasActiveVertices = true;
			}
			//case 1 - not visited, case 2 - visited in this iteration, case 3 - already visited, case 4 - not at all visited
			//don't change the active vertex list except in 1st case
		}
#endif//BFS_COMPILE
	}
}
