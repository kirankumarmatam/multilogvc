#include "header.h"

extern std::queue<struct IORequest> cache_to_IO_request_queue;
extern mutex cache_to_IO_request_queue_lock;
extern vector<ofstream> associativeVC_output_file;
extern vector<ofstream> partition_loader_output_file;
extern vector<ofstream> graph_loader_output_file;
extern vector<ofstream> Utility_output_file;
extern vector<ofstream> IO_output_file;

#if(ASYNCHRONOUS_PROGRAMMING == 1 || STRUCTURE_UPDATE == 1)
	extern CONFIGURATION configuration;
#endif//ASYNCHRONOUS_PROGRAMMING || STRUCTURE_UPDATE == 1

void MultiLogVC::Initialize(MultiLogContext *multiLogContext, Graph_data_loader *ptr, Utils *util_ptr, BUFFER_SIZE bufferElementsSize/*in bytes*/, VERTEX_TYPE VertexCount, BUFFER_SIZE updateMessageBufferSize/*in bytes*/)
{
	multi_log_context = multiLogContext;
	graph_data = ptr;
	logLoader = util_ptr;
	maxElementsInBuffer = bufferElementsSize / sizeof(DATA_SIZE);

	NumOfVertices = VertexCount;
	current_buffer = 0;
/*	if(posix_memalign((void **)&partition_vertex_values, SSD_PAGE_SIZE, maxElementsInBuffer * sizeof(VERTEX_VALUE_TYPE)*16))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
*/
	if(posix_memalign((void **)&partition_update_messages_buffer, SSD_PAGE_SIZE, updateMessageBufferSize))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
	partition_update_messages_buffer_sorted = (EDGE_MESSAGE_TYPE *) malloc(updateMessageBufferSize);

#if(STRUCTURE_UPDATE == 1)
	if(posix_memalign((void **)&structure_partition_update_messages_buffer, SSD_PAGE_SIZE, updateMessageBufferSize))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
	structure_partition_update_messages_buffer_sorted = (STRUCTURE_EDGE_MESSAGE_TYPE *) malloc(updateMessageBufferSize);
#endif//STRUCTURE_UPDATE
#if(EDGE_LOG_PROCESSING == 1)
	if(posix_memalign((void **)&edge_log_partition_update_messages_buffer, SSD_PAGE_SIZE, updateMessageBufferSize))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
#if(EDGE_LOG_MEASURE == 1)
	multi_log_context->edge_log_read_data = 0;
#endif//EDGE_LOG_MEASURE
#endif//EDGE_LOG_PROCESSING
	maxSizeInBuffer_messages = updateMessageBufferSize;

	FILE_OFFSET_TYPE *rowPtr = (FILE_OFFSET_TYPE *)malloc(sizeof(FILE_OFFSET_TYPE) * ((FILE_OFFSET_TYPE)NumOfVertices + 1));
	FILE * pFile = fopen((multi_log_context->graph_info.csr_file_content).data(),"r");
	if (pFile == NULL)
	{
		cout << "Error opening graph file" << endl;
		exit(0);
	}
	fread((void *)rowPtr, sizeof(FILE_OFFSET_TYPE), NumOfVertices+1, pFile);
	fclose(pFile);

	//Below calculation should be done based on in-coming edges not out-going edges
	VERTEX_TYPE loop_var;
	BUFFER_SIZE space_used;
	VERTEX_TYPE loop_num;
	VERTEX_TYPE vertex_list_size;
	assert(multi_log_context->partition_starting_vertex.size() == 0);
	assert(multi_log_context->partition_size_vertex.size() == 0);
	for(VERTEX_TYPE loop_1 = 0; loop_1 < NumOfVertices;)
	{
		loop_num = 0;
		space_used = maxSizeInBuffer_messages / 2; //diving by 8, because we are using out-going edges for calculation, where as we should have used in-coming edges
		multi_log_context->partition_starting_vertex.push_back(loop_1);
#if(DEBUG_FLP_1 == 1)
		associativeVC_output_file[0] << "l1 = " << loop_1 << " s = " << multi_log_context->partition_starting_vertex.size() << endl; 
#endif//DEBUG_FLP_1
		while(1)
		{
			if(loop_1 == NumOfVertices)
			{
				multi_log_context->partition_size_vertex.push_back(loop_num);
				break;
			}
			vertex_list_size = rowPtr[loop_1+1] - rowPtr[loop_1];
			if(space_used < (vertex_list_size*sizeof(UPDATE_MESSAGE)))
			{
				multi_log_context->partition_size_vertex.push_back(loop_num);
				break;
			}
			space_used = space_used - (vertex_list_size*sizeof(UPDATE_MESSAGE));
			loop_1++;
			loop_num++;
		}
#if(ASYNCHRONOUS_PROGRAMMING == 1 || STRUCTURE_UPDATE == 1)
		if(loop_num > configuration.multiLogVC_vertex_to_message_list_ptr_size)
		{
			configuration.multiLogVC_vertex_to_message_list_ptr_size = loop_num;
		}
#endif//ASYNCHRONOUS_PROGRAMMING == 1 || STRUCTURE_UPDATE == 1
	}
	
	free(rowPtr);
	multi_log_context->numOfPartitions = multi_log_context->partition_starting_vertex.size();
	assert(multi_log_context->partition_starting_vertex.size() == multi_log_context->partition_size_vertex.size());

#if(ASYNCHRONOUS_PROGRAMMING == 1)
	//for now don't free these two arrays
	vertex_to_messages_buffer_list_ptr = (BUFFER_SIZE *)malloc(sizeof(BUFFER_SIZE)*(DATA_SIZE)(configuration.multiLogVC_vertex_to_message_list_ptr_size));
	for(BUFFER_SIZE loop_1 = 0; loop_1 < (configuration.multiLogVC_vertex_to_message_list_ptr_size); loop_1++)
	{
		vertex_to_messages_buffer_list_ptr[loop_1] = -1;
	}
	update_messages_buffer_list = (LIST_UPDATE_MESSAGE *)malloc(configuration.multiLogVC_messages_list_buffer_size);
	update_messages_buffer_top = 0;
	vertex_messages_temp_buffer = (EDGE_MESSAGE_TYPE *)malloc((configuration.graph_loader_colInd_buffer_size / sizeof(VERTEX_TYPE))*sizeof(EDGE_MESSAGE_TYPE));
	Results_data = (UPDATE_MESSAGE_LIST)malloc((configuration.graph_loader_colInd_buffer_size / sizeof(VERTEX_TYPE))*sizeof(UPDATE_MESSAGE));
#if(PROCESS_EDGE_WEIGHT == 1)
	graph_data->vertexData_in_edge_file_id = open(graph_data->edge_weights_filename.data(), O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
	if (graph_data->vertexData_in_edge_file_id == -1) {
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}

	/* Stretch the file size to the size of the (mmapped) array of ints
	 */
	DATA_SIZE FileSize = ((DATA_SIZE)(multi_log_context->graph_info.NumEdges))*sizeof(IN_EDGE_AND_WEIGHT_FILE_TYPE);

	int result = lseek64(graph_data->vertexData_in_edge_file_id, FileSize, SEEK_SET);
	if (result == -1) {
		close(graph_data->vertexData_in_edge_file_id);
		perror("Error calling lseek64() to 'stretch' the file");
		exit(EXIT_FAILURE);
	}
	result = write(graph_data->vertexData_in_edge_file_id, "", 1);
	if (result != 1) {
		close(graph_data->vertexData_in_edge_file_id);
		perror("Error writing last byte of the file");
		exit(EXIT_FAILURE);
	}
	multi_log_context->Edge_weights_file_array = mmap64(0, FileSize, PROT_READ | PROT_WRITE, MAP_SHARED, graph_data->vertexData_in_edge_file_id, 0);
	if (multi_log_context->Edge_weights_file_array == MAP_FAILED) {
		close(graph_data->vertexData_in_edge_file_id);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}
#endif//PROCESS_EDGE_WEIGHT
#endif//ASYNCHRONOUS_PROGRAMMING

#if(STRUCTURE_UPDATE == 1)
	structure_delete_in_edge_file_id = open(graph_data->structure_delete_edge_filename.data(), O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
	if (structure_delete_in_edge_file_id == -1) {
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}

	/* Stretch the file size to the size of the (mmapped) array of ints
	 */
	DATA_SIZE FileSize = ((DATA_SIZE)(multi_log_context->graph_info.NumEdges))*sizeof(bool);

	associativeVC_output_file[0] << "sFS = " << FileSize << endl;

	int result = lseek64(structure_delete_in_edge_file_id, FileSize, SEEK_SET);
	if (result == -1) {
		close(structure_delete_in_edge_file_id);
		perror("Error calling lseek64() to 'stretch' the file");
		exit(EXIT_FAILURE);
	}
	result = write(structure_delete_in_edge_file_id, "", 1);
	if (result != 1) {
		close(structure_delete_in_edge_file_id);
		perror("Error writing last byte of the file");
		exit(EXIT_FAILURE);
	}
	structure_delete_edge_list = (bool *)mmap64(0, FileSize, PROT_READ | PROT_WRITE, MAP_SHARED, structure_delete_in_edge_file_id, 0);
	if (structure_delete_edge_list == MAP_FAILED) {
		close(structure_delete_in_edge_file_id);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}
	memset(structure_delete_edge_list, 0, FileSize);
	structure_map_edge_list_id = open(multi_log_context->graph_info.csr_file_header.data(), O_RDONLY);
	if (structure_map_edge_list_id == -1) {
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}

	/* Stretch the file size to the size of the (mmapped) array of ints
	 */
	FileSize = (((DATA_SIZE)(multi_log_context->graph_info.NumEdges))*sizeof(VERTEX_TYPE)) + (((DATA_SIZE)multi_log_context->graph_info.NumNodes+1)*sizeof(DATA_SIZE)) + (sizeof(VERTEX_TYPE) + sizeof(DATA_SIZE));

	structure_map_edge_list = (VERTEX_TYPE *)mmap64(0, FileSize, PROT_READ, MAP_SHARED, structure_map_edge_list_id, 0);
	if (structure_map_edge_list == MAP_FAILED) {
		close(structure_map_edge_list_id);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	
	}
#if(DEBUG_CODE == 1)
	for(BUFFER_SIZE loop_1 = 0; loop_1 < (FileSize)/sizeof(VERTEX_TYPE); loop_1++)
	{
		associativeVC_output_file[0] << "smel l_1 = " << loop_1 << " " << structure_map_edge_list[loop_1] << endl;
	}
#endif//DEBUG_CODE
	structure_edge_list = (EDGE_LIST)malloc(configuration.multiLogVC_vertex_to_message_list_ptr_size * sizeof(VERTEX_TYPE));
#endif//STRUCTURE_UPDATE

#if(DEBUG_CODE == 1)
	associativeVC_output_file[0] << "nOP = " << multi_log_context->numOfPartitions << endl;
	for(PARTITION_NUM_TYPE loop_1 = 0; loop_1 < multi_log_context->numOfPartitions; loop_1++)
	{
		associativeVC_output_file[0] << multi_log_context->partition_starting_vertex[loop_1] << " " << multi_log_context->partition_size_vertex[loop_1] << ";";
	}
	associativeVC_output_file[0] << endl;
#endif//DEBUG_CODE

#if(DEBUG_CODE == 1)
	associativeVC_output_file[0] << "mEIB = " << maxElementsInBuffer << " nOP = " << multi_log_context->numOfPartitions << " NOV = " << NumOfVertices << " uMBS = " << updateMessageBufferSize << " mIBm = " << maxSizeInBuffer_messages << endl;
#endif//DEBUG_CODE
}

#if(STRUCTURE_UPDATE == 1)
inline DATA_SIZE MultiLogVC::CALC_COLUMN_INDEX_OFFSET(VERTEX_TYPE vertex_id)
{
	return (((DATA_SIZE)(graph_data->NumberOfVertices)+1) * sizeof(DATA_SIZE) + *((DATA_SIZE *)((char *)structure_map_edge_list + (DATA_SIZE)vertex_id*sizeof(DATA_SIZE) + 3*sizeof(VERTEX_TYPE))) * sizeof(VERTEX_TYPE) + sizeof(VERTEX_TYPE) + sizeof(DATA_SIZE));
}
#endif//STRUCTURE_UPDATE

void MultiLogVC::run()
{
#if(MEASURE == 1)
	multi_log_context->iterationMessages = 0;
	multi_log_context->timer_before_iteration.start_timer();
#endif//MEASURE
	(dynamic_cast<MultiLogProgram*>(multi_log_context->program))->before_iteration(multi_log_context->current_iteration, multi_log_context);

#if(MEASURE == 1)
	multi_log_context->timer_before_iteration.end_timer();
#endif//MEASURE
	PARTITION_NUM_TYPE numOfPartitions_processed = 0;
/*	bool *active_list;
	if(posix_memalign((void **)&active_list, SSD_PAGE_SIZE, sizeof(bool) * maxElementsInBuffer))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
*/	bool *active_list;
/*	for(BUFFER_SIZE loop_1 = 0; loop_1 < maxElementsInBuffer; loop_1++)
	{
		active_list[loop_1] = 1;
	}*/
	PARTITION_SIZE activeVertexSize;
	while(numOfPartitions_processed < multi_log_context->numOfPartitions) {//all the partitions are done
#if(ASYNCHRONOUS_PROGRAMMING == 1)
		update_messages_buffer_top = 0;
#endif//ASYNCHRONOUS_PROGRAMMING
		active_list = (bool *)malloc(sizeof(bool) * multi_log_context->partition_size_vertex[numOfPartitions_processed]);
#if(DEBUG_CODE == 1)
		associativeVC_output_file[0] << "psv = " << multi_log_context->partition_size_vertex[numOfPartitions_processed] <<endl;
#endif//DEBUG_CODE
		VERTEX_TYPE startingVertexIndexOfPartition = multi_log_context->partition_starting_vertex[numOfPartitions_processed];
		activeVertexSize = multi_log_context->partition_size_vertex[numOfPartitions_processed];
#if(DEBUG_CODE == 1)
		associativeVC_output_file[0] << "nPp = " << numOfPartitions_processed << " sVIP = " << startingVertexIndexOfPartition << " aVS = " << activeVertexSize << endl;
#endif//DEBUG_CODE
		partition_vertex_values = malloc(multi_log_context->partition_size_vertex[numOfPartitions_processed] * sizeof(VERTEX_VALUE_TYPE));
#if(MEASURE == 1)
		multi_log_context->timer_value_access.start_timer();
#endif//MEASURE
		lseek64(multi_log_context->graph_info.vertexValue_file_id, (FILE_OFFSET_TYPE)startingVertexIndexOfPartition * sizeof(VERTEX_VALUE_TYPE), SEEK_SET);
		read(multi_log_context->graph_info.vertexValue_file_id, (void *)partition_vertex_values, (FILE_OFFSET_TYPE)activeVertexSize * sizeof(VERTEX_VALUE_TYPE));
#if(MEASURE == 1)
		multi_log_context->timer_value_access.end_timer();
#endif//MEASURE
/*----------------Structural updates - BEGIN ------------------*/
		//based on the deleted node information update the active vertex list
		//how to implement the edge list deletion?
		//where to store the edge list deletion?
		//keep a bit for every edge? or delete the edge itself - need to do more as we need to maintain the stuff for csr
		//mmap the bit vector
		//do binary search on the edge list and 
/*----------------Structural updates - END --------------------*/
/*----------------Partition streaming - BEGIN ---------------------*/
#if(MEASURE == 1)
		multi_log_context->timer_active_vertices.start_timer();
#endif//MEASURE
		if(lseek64(multi_log_context->graph_info.vertexActive_file_id, ((FILE_OFFSET_TYPE)startingVertexIndexOfPartition) * sizeof(bool), SEEK_SET) < 0)
		{
			cout << "Error with lseek" << endl;
			exit(0);
		}
		if(read(multi_log_context->graph_info.vertexActive_file_id, active_list, (FILE_OFFSET_TYPE)activeVertexSize * sizeof(bool)) != ((FILE_OFFSET_TYPE)activeVertexSize * sizeof(bool)))
		{
			cout << "errno = "<< errno << endl;
			perror("active vertices file");
			cout << "Error with read" << endl;
			exit(0);
		}
#if(MEASURE == 1)
		multi_log_context->timer_active_vertices.end_timer();
#endif//MEASURE
#if(DEBUG_CODE == 1)
		associativeVC_output_file[0] << "Active vertex list start v file id = " << multi_log_context->graph_info.vertexValue_file_id << " A file id = "<<multi_log_context->graph_info.vertexActive_file_id << endl;
		associativeVC_output_file[0] << "aVS = " << activeVertexSize << endl;
		for(BUFFER_SIZE loop_1 = 0; loop_1 < activeVertexSize; loop_1++)
		{
			if(active_list[loop_1] == 1)
			{
//				associativeVC_output_file[0] << loop_1 << " = " << active_list[loop_1] << " " << ((VERTEX_VALUE_TYPE *)partition_vertex_values)[loop_1] << "; ";
			}
		}
		associativeVC_output_file[0] << "\nActive vertex list end" << endl;
#endif//DEBUG_CODE
		BUFFER_SIZE *message_index;
		message_index = (BUFFER_SIZE *)calloc(multi_log_context->partition_size_vertex[numOfPartitions_processed]+1, sizeof(BUFFER_SIZE));
		BUFFER_SIZE *message_index_current_count;
#if(ASYNCHRONOUS_PROGRAMMING == 0)
		if(multi_log_context->current_iteration != 0)
		{
#endif//ASYNCHRONOUS_PROGRAMMING
			message_index_current_count = (BUFFER_SIZE *)calloc(multi_log_context->partition_size_vertex[numOfPartitions_processed], sizeof(BUFFER_SIZE));
			vector<struct page_info> page_list;
			BUFFER_SIZE log_space_usage;
			log_space_usage = 0;
			BUFFER_SIZE numberOfMessages;
			bool prev_partition_file;
#if(ASYNCHRONOUS_PROGRAMMING == 0)
			prev_partition_file = ((multi_log_cache->current_partition_file) + 1) % 2;
#elif(ASYNCHRONOUS_PROGRAMMING == 1)
			prev_partition_file = multi_log_cache->current_partition_file;
#endif//ASYNCHRONOUS_PROGRAMMING
			//For now fetch in multiples of page only
			for(list<struct page_info>::iterator it1 = (multi_log_cache->partition_page_info)[prev_partition_file][numOfPartitions_processed].begin(); it1 != (multi_log_cache->partition_page_info)[prev_partition_file][numOfPartitions_processed].end(); it1++)
			{
				log_space_usage += SSD_PAGE_SIZE;
				page_list.push_back(*it1);
#if(DEBUG_CODE == 1)
				associativeVC_output_file[0] << "lsu = " << log_space_usage << " it1 fo = " << (*it1).file_offset << " it1 cpn = " << (*it1).cpn << " it1 iC = " << (*it1).isCached << endl;
#endif//DEBUG_CODE
			}
			assert(log_space_usage <= maxSizeInBuffer_messages);
			if(log_space_usage > 0)
			{
#if(MEASURE == 1)
				multi_log_context->timer_log_loading.start_timer();
#endif//MEASURE
				logLoader->loadListPagesFromFileToBuffer(multi_log_cache->log_file_id[prev_partition_file], (void *)(partition_update_messages_buffer), numberOfMessages, page_list, (multi_log_cache->vertexToPartitionPage)[prev_partition_file][numOfPartitions_processed], 0);
#if(MEASURE == 1)
				multi_log_context->timer_log_loading.end_timer();
#endif//MEASURE
				page_list.clear();
				log_space_usage = 0;
#if(MEASURE == 1)
				multi_log_context->iterationMessages += numberOfMessages;
				multi_log_context->timer_log_processing.start_timer();
#endif//MEASURE
				for(BUFFER_SIZE loop_1 = 0; loop_1 < numberOfMessages; loop_1++)
				{
					assert(((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_1].target >= multi_log_context->partition_starting_vertex[numOfPartitions_processed]);
					message_index[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed] + 1] += 1;
				}
				for(BUFFER_SIZE loop_1 = 1; loop_1 <= multi_log_context->partition_size_vertex[numOfPartitions_processed]; loop_1++)
				{
					message_index[loop_1] += message_index[loop_1 - 1];
				}

				for(BUFFER_SIZE loop_1 = 0; loop_1 < numberOfMessages; loop_1++)
				{
					partition_update_messages_buffer_sorted[message_index_current_count[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed]] + message_index[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed]]] = ((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_1].data;
					message_index_current_count[((UPDATE_MESSAGE *)partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed]] += 1;
				}
#if(MEASURE == 1)
				multi_log_context->timer_log_processing.end_timer();
#endif//MEASURE
			}
			free(message_index_current_count);
			(multi_log_cache->partition_page_info)[prev_partition_file][numOfPartitions_processed].clear();
#if(ASYNCHRONOUS_PROGRAMMING == 0)
		}
#endif//ASYNCHRONOUS_PROGRAMMING
#if(VC_ASSOCIATIVE_PROGRAM == 1 && ASYNCHRONOUS_PROGRAMMING == 1)
		multi_log_context->timer_log_processing.start_timer();
		for(BUFFER_SIZE loop_1 = 0; loop_1 < multi_log_context->partition_size_vertex[numOfPartitions_processed]; loop_1++)
		{
			Application_function_combine(startingVertexIndexOfPartition + loop_1, &(partition_update_messages_buffer_sorted[message_index[loop_1]]), message_index[loop_1 + 1] - message_index[loop_1], ((VERTEX_VALUE_TYPE *)partition_vertex_values)[loop_1], &(active_list[loop_1]), multi_log_context);
		}
		for(BUFFER_SIZE loop_1 = 0; loop_1 <= multi_log_context->partition_size_vertex[numOfPartitions_processed]; loop_1++)
		{
			message_index[loop_1] = 0;
		}
		multi_log_context->timer_log_processing.end_timer();
#endif//VC_ASSOCIATIVE_PROGRAM && ASYNCHRONOUS_PROGRAMMING

#if(STRUCTURE_UPDATE == 1)
		BUFFER_SIZE *structure_message_index;
		structure_message_index = (BUFFER_SIZE *)calloc(multi_log_context->partition_size_vertex[numOfPartitions_processed]+1, sizeof(BUFFER_SIZE));
		BUFFER_SIZE *structure_message_index_current_count;
		structure_message_index_current_count = (BUFFER_SIZE *)calloc(multi_log_context->partition_size_vertex[numOfPartitions_processed], sizeof(BUFFER_SIZE));
		vector<struct page_info> structure_page_list;
		BUFFER_SIZE structure_log_space_usage = 0;
		BUFFER_SIZE structure_numberOfMessages;
		bool structure_prev_partition_file = 0;
		for(list<struct page_info>::iterator it1 = (structure_multi_log_cache->partition_page_info)[structure_prev_partition_file][numOfPartitions_processed].begin(); it1 != (structure_multi_log_cache->partition_page_info)[structure_prev_partition_file][numOfPartitions_processed].end(); it1++)
		{
			structure_log_space_usage += SSD_PAGE_SIZE;
			structure_page_list.push_back(*it1);
#if(DEBUG_CODE == 1)
			associativeVC_output_file[0] << "slsu = " << structure_log_space_usage << " it1 fo = " << (*it1).file_offset << " it1 cpn = " << (*it1).cpn << " it1 iC = " << (*it1).isCached << endl;
#endif//DEBUG_CODE
		}

		logLoader->loadListPagesFromFileToBuffer(structure_multi_log_cache->log_file_id[structure_prev_partition_file], (void *)(structure_partition_update_messages_buffer), structure_numberOfMessages, structure_page_list, (structure_multi_log_cache->vertexToPartitionPage)[structure_prev_partition_file][numOfPartitions_processed], 2);
		structure_numberOfMessages = structure_numberOfMessages / sizeof(STRUCTURE_UPDATE_MESSAGE);
#if(DEBUG_CODE == 1)
		associativeVC_output_file[0] << "snOM = " << structure_numberOfMessages << endl;
#endif//DEBUG_CODE
		structure_page_list.clear();
		structure_log_space_usage = 0;
		for(BUFFER_SIZE loop_1 = 0; loop_1 < structure_numberOfMessages; loop_1++)
		{
#if(DEBUG_CODE == 1)
			associativeVC_output_file[0] << "spumb l_1 = " << loop_1 << " t = " << ((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].target << " " << ((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].data.target_id << " " << ((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].data.operation << " " << ((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].data.source_id << " psv =  " << multi_log_context->partition_starting_vertex[numOfPartitions_processed] << endl;
#endif//DEBUG_CODE
			assert(((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].target >= multi_log_context->partition_starting_vertex[numOfPartitions_processed]);
			structure_message_index[((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed] + 1] += 1;
		}
		for(BUFFER_SIZE loop_1 = 1; loop_1 <= multi_log_context->partition_size_vertex[numOfPartitions_processed]; loop_1++)
		{
			structure_message_index[loop_1] += structure_message_index[loop_1 - 1];
#if(DEBUG_CODE == 1)
			associativeVC_output_file[0] << "smi l_1 = " << loop_1 << " " << structure_message_index[loop_1] << endl;
#endif//DEBUG_CODE
		}

		for(BUFFER_SIZE loop_1 = 0; loop_1 < structure_numberOfMessages; loop_1++)
		{
			structure_partition_update_messages_buffer_sorted[structure_message_index_current_count[((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed]] + structure_message_index[((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed]]] = ((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].data;
			structure_message_index_current_count[((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed]] += 1;
#if(DEBUG_CODE == 1)
			associativeVC_output_file[0] << "spumbsc l_1 = " << loop_1 << " t = " <<  ((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].target << " c = " << structure_message_index_current_count[((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed]] << " cc = " << structure_message_index[((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].target - multi_log_context->partition_starting_vertex[numOfPartitions_processed]] << " t = " << ((STRUCTURE_UPDATE_MESSAGE *)structure_partition_update_messages_buffer)[loop_1].data.target_id << endl;
#endif//DEBUG_CODE
		}
#if(DEBUG_CODE == 1)
		for(BUFFER_SIZE loop_1 = 0; loop_1 < structure_numberOfMessages; loop_1++)
		{
			associativeVC_output_file[0] << "scheck t = " << ((STRUCTURE_EDGE_MESSAGE_TYPE *)structure_partition_update_messages_buffer_sorted)[loop_1].target_id << " o = " << ((STRUCTURE_EDGE_MESSAGE_TYPE *)structure_partition_update_messages_buffer_sorted)[loop_1].operation << " s = " << ((STRUCTURE_EDGE_MESSAGE_TYPE *)structure_partition_update_messages_buffer_sorted)[loop_1].source_id << endl;
		}
#endif//DEBUG_CODE

		for(BUFFER_SIZE loop_1 = 0; loop_1 < structure_numberOfMessages; loop_1++)
		{
			VERTEX_TYPE index, count;
			VERTEX_TYPE structure_vertex_id = structure_partition_update_messages_buffer_sorted[loop_1].target_id;
			VERTEX_TYPE structure_Edge_list_size = *((DATA_SIZE *)((char *)structure_map_edge_list + ((DATA_SIZE)structure_vertex_id+1)*sizeof(DATA_SIZE) + 3*sizeof(VERTEX_TYPE))) - *((DATA_SIZE *)((char *)structure_map_edge_list + ((DATA_SIZE)structure_vertex_id)*sizeof(DATA_SIZE) + 3*sizeof(VERTEX_TYPE)));//works only when DATA_SIZE = 2 * VERTEX_TYPE
#if(DEBUG_CODE == 1)
			associativeVC_output_file[0] << "spumbs l_1 = " << loop_1 << " svi = " << structure_vertex_id << " smel = " << *((DATA_SIZE *)((char *)structure_map_edge_list + ((DATA_SIZE)structure_vertex_id)*sizeof(DATA_SIZE) + 3*sizeof(VERTEX_TYPE)))<< " " << *((DATA_SIZE *)((char *)structure_map_edge_list + ((DATA_SIZE)structure_vertex_id+1)*sizeof(DATA_SIZE) + 3*sizeof(VERTEX_TYPE)))<< " sEls = " << structure_Edge_list_size << endl;
#endif//DEBUG_CODE
			assert(structure_Edge_list_size > 0);

			DATA_SIZE Index = CALC_COLUMN_INDEX_OFFSET(structure_vertex_id);
			structure_multi_log_cache->binarySearch((VERTEX_TYPE *)((char *)structure_map_edge_list + Index), 0, structure_Edge_list_size-1, structure_partition_update_messages_buffer_sorted[loop_1].source_id, &index, &count);
			associativeVC_output_file[0] << "I = " << Index << " i = " << index << endl;
			structure_delete_edge_list[*((DATA_SIZE *)((char *)structure_map_edge_list + ((DATA_SIZE)structure_vertex_id)*sizeof(DATA_SIZE) + 3*sizeof(VERTEX_TYPE)))+index] = 1;
		}
		//clear structure message index arrays
		free(structure_message_index);
		free(structure_message_index_current_count);
		(structure_multi_log_cache->partition_page_info)[structure_prev_partition_file][numOfPartitions_processed].clear();
		for(BUFFER_SIZE loop_1 = 0; loop_1 < activeVertexSize; loop_1++)
		{
#if(DEBUG_CODE == 1)
			associativeVC_output_file[0] << "sal l_1 = " << loop_1 << " " << active_list[loop_1] << " " << multi_log_context->structure_delete_vertex_list[startingVertexIndexOfPartition + loop_1] << endl;
#endif//DEBUG_CODE
			if(active_list[loop_1] == 1 && multi_log_context->structure_delete_vertex_list[startingVertexIndexOfPartition + loop_1] == 1)
			{
				active_list[loop_1] = 0;
			}
		}
#endif//STRUCTURE_UPDATE

		//Do proper book keeping management for the partition loading
		/*----------------Partition streaming - END ---------------------*/
#if(DEBUG_CODE == 1)
		associativeVC_output_file[0] << "PSEND i = " << multi_log_context->current_iteration << endl;
#endif//DEBUG_CODE
		EDGE_MESSAGES_TYPE result_data;
		numOfPartitions_processed++;
#if(MEASURE == 1)
		multi_log_context->timer_log_generation.start_timer();
#endif//MEASURE
#pragma omp parallel num_threads(2)
		{
			unsigned int tid = omp_get_thread_num();
			if(tid == 0)
			{
#if(EDGE_LOG_PROCESSING == 1)
				if(multi_log_context->edge_log_read == 0)
				{
#endif//EDGE_LOG_PROCESSING
					graph_data->load_next_list(active_list, startingVertexIndexOfPartition, activeVertexSize);
#if(EDGE_LOG_PROCESSING == 1)
				}
#endif//EDGE_LOG_PROCESSING
			}
			else {
//				for(BUFFER_SIZE loop_temp = 0; loop_temp < activeVertexSize; loop_temp++)
//								{
//								cout << " al = " <<  active_list[loop_temp] << endl;
//								}

				bool exit;
				while(1) {//all the buffers are done
					exit = 0;
#if(EDGE_LOG_PROCESSING == 1)
					if(multi_log_context->edge_log_read == 0)
					{
#endif//EDGE_LOG_PROCESSING
#if(MEASURE == 1)
						multi_log_context->timer_graph_access.start_timer();
#endif//MEASURE
						while(1){
							((graph_data->buffer_lock)[current_buffer]).lock();
							if((graph_data->buffer_ready_bit)[current_buffer] == 1)
							{
								((graph_data->buffer_lock)[current_buffer]).unlock();
								break;
							}
							else {
#if(DEBUG_CODE == 1)
								//			associativeVC_output_file[0] << "cb = " << current_buffer << endl;
#endif//DEBUG_CODE
								graph_data->done_lock.lock();
								if(graph_data->done == 1) {
									graph_data->done = 0;
#if(DEBUG_CODE == 1)
									//				associativeVC_output_file[0] << "done cb = " << current_buffer << endl;
#endif//DEBUG_CODE
									exit = 1;
								}
								graph_data->done_lock.unlock();
							}
							((graph_data->buffer_lock)[current_buffer]).unlock();
							if(exit == 1) break;
						}
#if(MEASURE == 1)
						multi_log_context->timer_graph_access.end_timer();
#endif//MEASURE
#if(DEBUG_CODE == 1)
						associativeVC_output_file[0] << "cb = " << current_buffer << endl;
						associativeVC_output_file[0] << "Received a buffer from graph data loader" << endl;
#endif//DEBUG_CODE
						if(exit == 1) break;
#if(EDGE_LOG_PROCESSING == 1)
					}
					else {
#if(MEASURE == 1)
						multi_log_context->timer_graph_access.start_timer();
#endif//MEASURE
						vector<struct page_info> edge_log_page_list;
						BUFFER_SIZE edge_log_log_space_usage;
						edge_log_log_space_usage = 0;
						BUFFER_SIZE edge_log_numberOfBytes;
						bool edge_log_prev_partition_file;
						edge_log_prev_partition_file = ((edge_log_multi_log_cache->current_partition_file) + 1) % 2;

						for(list<struct page_info>::iterator it1 = (edge_log_multi_log_cache->partition_page_info)[edge_log_prev_partition_file][numOfPartitions_processed-1].begin(); it1 != (edge_log_multi_log_cache->partition_page_info)[edge_log_prev_partition_file][numOfPartitions_processed-1].end(); it1++)
						{
							edge_log_log_space_usage += SSD_PAGE_SIZE;
							edge_log_page_list.push_back(*it1);
#if(EDGE_LOG_DEBUG_CODE == 1)
							associativeVC_output_file[0] << "elsu = " << edge_log_log_space_usage << " it1 fo = " << (*it1).file_offset << " it1 cpn = " << (*it1).cpn << " it1 iC = " << (*it1).isCached << endl;
#endif//EDGE_LOG_DEBUG_CODE
						}
						assert(edge_log_log_space_usage <= maxSizeInBuffer_messages);
						if(edge_log_log_space_usage > 0)
						{
							logLoader->loadListPagesFromFileToBuffer(edge_log_multi_log_cache->log_file_id[edge_log_prev_partition_file], (void *)(edge_log_partition_update_messages_buffer), edge_log_numberOfBytes, edge_log_page_list, (edge_log_multi_log_cache->vertexToPartitionPage)[edge_log_prev_partition_file][numOfPartitions_processed-1], 1);
#if(EDGE_LOG_DEBUG_CODE == 1)
							associativeVC_output_file[0] << "elnB = " << edge_log_numberOfBytes << endl;
							for(BUFFER_SIZE loop_t1 = 0; loop_t1 < edge_log_numberOfBytes; loop_t1 += sizeof(VERTEX_TYPE))
							{
								associativeVC_output_file[0] << "l_t1 = " << loop_t1 << " v = " << *((VERTEX_TYPE *)((char *)(edge_log_partition_update_messages_buffer) + loop_t1))  << endl;
							}
#endif//EDGE_LOG_DEBUG_CODE
#if(EDGE_LOG_MEASURE == 1)
							multi_log_context->edge_log_read_data += edge_log_numberOfBytes;
#endif//EDGE_LOG_MEASURE
							BUFFER_SIZE current_list_index = 0;
							struct bufferIndexPtr temp_bufferPtr;
							while(current_list_index < edge_log_numberOfBytes)
							{
								temp_bufferPtr.vertex_id = *((VERTEX_TYPE *)((char *)(edge_log_partition_update_messages_buffer) + current_list_index));
								temp_bufferPtr.start_index = current_list_index + 2 * sizeof(VERTEX_TYPE);
								temp_bufferPtr.size = *((VERTEX_TYPE *)((char *)(edge_log_partition_update_messages_buffer) + current_list_index + sizeof(VERTEX_TYPE)));
								current_list_index += sizeof(VERTEX_TYPE)*(2 + temp_bufferPtr.size);
								graph_data->vertex_index_list[current_buffer].push_back(temp_bufferPtr);
#if(EDGE_LOG_DEBUG_CODE == 1)
								associativeVC_output_file[0] << "e_l v = " << temp_bufferPtr.vertex_id << " s_i = " << temp_bufferPtr.start_index << " s = " << temp_bufferPtr.size  << endl;
#endif//EDGE_LOG_DEBUG_CODE
								//declare edge_log_edge_message_ptr_type
							}
							edge_log_page_list.clear();
							edge_log_log_space_usage = 0;
						}
					}
#if(EDGE_LOG_DEBUG_CODE == 1)
						associativeVC_output_file[0] << "elcpf = " << edge_log_multi_log_cache->current_partition_file << endl;
#endif//EDGE_LOG_DEBUG_CODE
					if(multi_log_context->edge_log_write == 1)
					{
						bool edge_log_prev_partition_file;
						edge_log_prev_partition_file = ((edge_log_multi_log_cache->current_partition_file) + 1) % 2;
						(edge_log_multi_log_cache->partition_page_info)[edge_log_prev_partition_file][numOfPartitions_processed-1].clear();
						struct partition_cache_page_pointer *cachePartitionPtr = &((edge_log_multi_log_cache->vertexToPartitionPage)[edge_log_prev_partition_file][numOfPartitions_processed-1]);
						if(cachePartitionPtr->cpn != -1)
						{
							edge_log_multi_log_cache->list_update_lock.lock();						
							edge_log_multi_log_cache->freeCPN.push_back(cachePartitionPtr->cpn);
#if(DEBUG_CODE == 1)
							Utility_output_file[0] << "f s = " << edge_log_multi_log_cache->freeCPN.size() << endl;
#endif//DEBUG_CODE
							edge_log_multi_log_cache->list_update_lock.unlock();
#if(EDGE_LOG_DEBUG_CODE == 1)
							associativeVC_output_file[0] << "pTP = " << cachePartitionPtr->pageTopPointer << " cpn = " << cachePartitionPtr->cpn << endl;
#endif//EDGE_LOG_DEBUG_CODE
							cachePartitionPtr->pageTopPointer = SSD_PAGE_SIZE;
							cachePartitionPtr->cpn = -1;
						}
					}
#if(MEASURE == 1)
						multi_log_context->timer_graph_access.end_timer();
#endif//MEASURE
					//do I need to work be careful about adding the cpn to the page list? keep locks when clearing the pages?
#endif//EDGE_LOG_PROCESSING
					BUFFER_SIZE numberOfElementsInBuffer;
#if(EDGE_LOG_PROCESSING == 1)
					if(multi_log_context->edge_log_read == 0)
					{
#endif//EDGE_LOG_PROCESSING
						numberOfElementsInBuffer = (graph_data->buffer_elements[current_buffer]);
#if(EDGE_LOG_PROCESSING == 1)
					}
					else {
						numberOfElementsInBuffer = graph_data->vertex_index_list[current_buffer].size();
					}
#endif//EDGE_LOG_PROCESSING
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] << "nOEIB = " << numberOfElementsInBuffer << endl;
#endif//DEBUG_CODE
					//#pragma omp parallel for
#if(MEASURE == 1)
				multi_log_context->timer_edge_program.start_timer();
#endif//MEASURE
				for(BUFFER_SIZE loop_2 = 0; loop_2 < numberOfElementsInBuffer; loop_2++)
				{
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] << "vi = " << graph_data->vertex_index_list[current_buffer][loop_2].vertex_id << " si = " << (graph_data->vertex_index_list)[current_buffer][loop_2].start_index << " s = " << graph_data->vertex_index_list[current_buffer][loop_2].size << endl;
#endif//DEBUG_CODE
					multi_log_context->timer_edge_processing.start_timer();
#if(EDGE_PROGRAM_OPTIMIZATION == 0)
					MESSAGE_LIST message_list;
						for(VERTEX_TYPE loop_3 = message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition]; loop_3 < message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id + 1 - startingVertexIndexOfPartition]; loop_3++)
						{
							message_list.push_back(partition_update_messages_buffer_sorted[loop_3]);
						}
						EDGE_LIST edge_list;
						for(VERTEX_TYPE loop_3 = 0; loop_3 < graph_data->vertex_index_list[current_buffer][loop_2].size; loop_3++)
						{
							edge_list.push_back(((VERTEX_TYPE *)((char *)((graph_data->buffer)[current_buffer]) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index))[loop_3]);
						}
						//need to fix sending message to appropriate destination
						multi_log_context->program_edge_processing->edge_program_lists(graph_data->vertex_index_list[current_buffer][loop_2].vertex_id, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition], message_list, edge_list, result_data, multi_log_context);
#else//EDGE_PROGRAM_OPTIMIZATION
#if(VC_PROGRAM == 1)
#if(ASYNCHRONOUS_PROGRAMMING == 0)
#if(STRUCTURE_UPDATE == 1)
						structure_edge_list_size = 0;
						for(BUFFER_SIZE loop_t1 = 0; loop_t1 < graph_data->vertex_index_list[current_buffer][loop_2].size; loop_t1++)
						{
#if(DEBUG_CODE == 1)
							associativeVC_output_file[0] << "sdel l_1 = " << loop_t1 << structure_delete_edge_list[*((DATA_SIZE *)((char *)structure_map_edge_list + (DATA_SIZE)(graph_data->vertex_index_list[current_buffer][loop_2].vertex_id)*sizeof(DATA_SIZE) + 3*sizeof(VERTEX_TYPE))) + loop_t1] << " ei = " << *((DATA_SIZE *)((char *)structure_map_edge_list + (DATA_SIZE)(graph_data->vertex_index_list[current_buffer][loop_2].vertex_id + 1)*sizeof(DATA_SIZE) + 3*sizeof(VERTEX_TYPE))) + loop_t1 << " sels = " << structure_edge_list_size <<  endl;
#endif//DEBUG_CODE
							if(structure_delete_edge_list[*((DATA_SIZE *)((char *)structure_map_edge_list + (DATA_SIZE)(graph_data->vertex_index_list[current_buffer][loop_2].vertex_id)*sizeof(DATA_SIZE) + 3*sizeof(VERTEX_TYPE)))+loop_t1] == 0)
							{
								structure_edge_list[structure_edge_list_size] = ((VERTEX_TYPE *)((char *)((graph_data->buffer)[current_buffer]) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index))[loop_t1];
								structure_edge_list_size++;
							}
						}
#endif//STRUCTURE_UPDATE
#if(EDGE_LOG_PROCESSING == 1)
						if(multi_log_context->edge_log_read == 0)
						{
#endif//EDGE_LOG_PROCESSING
#if(STRUCTURE_UPDATE == 1)
							multi_log_context->program_edge_processing->edge_program_lists(graph_data->vertex_index_list[current_buffer][loop_2].vertex_id, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition], &(partition_update_messages_buffer_sorted[message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition]]), message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id + 1 - startingVertexIndexOfPartition] - message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition], structure_edge_list, structure_edge_list_size, multi_log_context);
#else//STRUCTURE_UPDATE
							multi_log_context->program_edge_processing->edge_program_lists(graph_data->vertex_index_list[current_buffer][loop_2].vertex_id, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition], &(partition_update_messages_buffer_sorted[message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition]]), message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id + 1 - startingVertexIndexOfPartition] - message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition], ((VERTEX_TYPE *)((char *)((graph_data->buffer)[current_buffer]) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index)), graph_data->vertex_index_list[current_buffer][loop_2].size, multi_log_context);
#endif//STRUCTURE_UPDATE
#if(EDGE_LOG_PROCESSING == 1)
						}
						else {
							//keep current buffer to 0, make sure you declare it
							//for edge log we are not supporting asynchronous programming
							multi_log_context->program_edge_processing->edge_program_lists(graph_data->vertex_index_list[current_buffer][loop_2].vertex_id, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition], &(partition_update_messages_buffer_sorted[message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition]]), message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id + 1 - startingVertexIndexOfPartition] - message_index[graph_data->vertex_index_list[current_buffer][loop_2].vertex_id - startingVertexIndexOfPartition], ((VERTEX_TYPE *)((char *)(edge_log_partition_update_messages_buffer) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index)), graph_data->vertex_index_list[current_buffer][loop_2].size, multi_log_context);
						}
#endif//EDGE_LOG_PROCESSING
#elif(ASYNCHRONOUS_PROGRAMMING == 1)
						VERTEX_TYPE current_vertex_id = graph_data->vertex_index_list[current_buffer][loop_2].vertex_id;
						assert(current_vertex_id >= startingVertexIndexOfPartition);
						assert( ( (numOfPartitions_processed < multi_log_context->partition_starting_vertex.size()) && (current_vertex_id < multi_log_context->partition_starting_vertex[numOfPartitions_processed]) ) || ( (numOfPartitions_processed == multi_log_context->partition_starting_vertex.size()) && (current_vertex_id < multi_log_context->graph_info.NumNodes) ) );
						BUFFER_SIZE Message_list_size = (message_index[current_vertex_id + 1 - startingVertexIndexOfPartition] - message_index[current_vertex_id - startingVertexIndexOfPartition]);
						memcpy((void *)vertex_messages_temp_buffer, (void *)&(partition_update_messages_buffer_sorted[message_index[current_vertex_id - startingVertexIndexOfPartition]]), Message_list_size * sizeof(EDGE_MESSAGE_TYPE));
#if(DEBUG_CODE == 1 || PRTA_TESTING == 1)
						associativeVC_output_file[0] << "cvi = " << current_vertex_id << endl;
#endif//DEBUG_CODE
//						assert(Message_list_size == 0);
						for(BUFFER_SIZE loop_3 = vertex_to_messages_buffer_list_ptr[current_vertex_id - startingVertexIndexOfPartition]; loop_3 != -1; loop_3 = update_messages_buffer_list[loop_3].pointer)
						{
							vertex_messages_temp_buffer[Message_list_size++] = update_messages_buffer_list[loop_3].data;
						}
						assert(Message_list_size < (configuration.graph_loader_colInd_buffer_size / sizeof(EDGE_MESSAGE_TYPE)));
#if(DEBUG_CODE == 1 || PRTA_TESTING == 1)
							associativeVC_output_file[0] << "Mls = " << Message_list_size << endl;
#endif//DEBUG_CODE
						vertex_to_messages_buffer_list_ptr[current_vertex_id - startingVertexIndexOfPartition] = -1;
						BUFFER_SIZE result_data_size;
						result_data_size = 0;
#if(PROCESS_EDGE_WEIGHT == 0)
						multi_log_context->program_edge_processing->edge_program_lists(current_vertex_id, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[current_vertex_id - startingVertexIndexOfPartition], vertex_messages_temp_buffer, Message_list_size, ((VERTEX_TYPE *)((char *)((graph_data->buffer)[current_buffer]) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index)), graph_data->vertex_index_list[current_buffer][loop_2].size, Results_data, result_data_size, multi_log_context);
#elif(PROCESS_EDGE_WEIGHT == 1)
						multi_log_context->program_edge_processing->edge_program_lists_weights(current_vertex_id, ((VERTEX_VALUE_TYPE *)partition_vertex_values)[current_vertex_id - startingVertexIndexOfPartition], vertex_messages_temp_buffer, Message_list_size, ((VERTEX_TYPE *)((char *)((graph_data->buffer)[current_buffer]) + (graph_data->vertex_index_list)[current_buffer][loop_2].start_index)), graph_data->vertex_index_list[current_buffer][loop_2].size, (IN_EDGE_AND_WEIGHT_FILE_TYPE *)((char *)(multi_log_context->Edge_weights_file_array) + graph_data->vertex_index_list[current_buffer][loop_2].edge_weight_index), Results_data, result_data_size, multi_log_context);
#endif//PROCESS_EDGE_WEIGHT
#if(DEBUG_CODE == 1 || PRTA_TESTING == 1)
						associativeVC_output_file[0] << "rds = " << result_data_size << endl;
#endif//DEBUG_CODE
						assert(result_data_size <= graph_data->vertex_index_list[current_buffer][loop_2].size);
						for(VERTEX_TYPE loop_3 = 0; loop_3 < result_data_size; loop_3++)
						{
							PARTITION_NUM_TYPE partition_num, count;
							multi_log_cache->binarySearch(multi_log_context->partition_starting_vertex, 0, multi_log_context->partition_starting_vertex.size()-1,  (Results_data[loop_3]).target, &partition_num, &count);
#if(DEBUG_CODE == 1 || PRTA_TESTING == 1)
if(loop_3 % 10 == 0)
							associativeVC_output_file[0] << "sVIP = " << startingVertexIndexOfPartition << " eVIP = " << (multi_log_context->partition_starting_vertex)[partition_num+1] << " t = " << (Results_data[loop_3]).target << " pn = " << partition_num << " s = " << Results_data[loop_3].data.source << " d = " << Results_data[loop_3].data.data << endl;
#endif//DEBUG_CODE || PRTA_TESTING
assert(0 <= partition_num);
assert(partition_num < multi_log_context->partition_starting_vertex.size());
							if((partition_num != (numOfPartitions_processed-1)) || ((partition_num == (numOfPartitions_processed-1)) && (Results_data[loop_3].target <= current_vertex_id)) || ((partition_num == (numOfPartitions_processed-1)) && (active_list[Results_data[loop_3].target - startingVertexIndexOfPartition] == 0)))
							{
//								multi_log_context->timer_multi_log_access.start_timer();
								multi_log_cache->send_message_partition((char *)(&Results_data[loop_3]), sizeof(UPDATE_MESSAGE), partition_num);
//								multi_log_context->timer_multi_log_access.end_timer();
							}
							else {
								update_messages_buffer_list[update_messages_buffer_top].data = Results_data[loop_3].data;
								assert(Results_data[loop_3].target >= startingVertexIndexOfPartition);
						assert(  ( (numOfPartitions_processed < multi_log_context->partition_starting_vertex.size()) && (Results_data[loop_3].target < multi_log_context->partition_starting_vertex[numOfPartitions_processed]) ) || ( (numOfPartitions_processed == multi_log_context->partition_starting_vertex.size()) && (Results_data[loop_3].target < multi_log_context->graph_info.NumNodes) )  );
								update_messages_buffer_list[update_messages_buffer_top].pointer = vertex_to_messages_buffer_list_ptr[Results_data[loop_3].target - startingVertexIndexOfPartition];
//								assert(update_messages_buffer_list[update_messages_buffer_top].pointer != 0);
								vertex_to_messages_buffer_list_ptr[Results_data[loop_3].target - startingVertexIndexOfPartition] = update_messages_buffer_top;
								update_messages_buffer_top++;
								assert(update_messages_buffer_top <= (configuration.multiLogVC_messages_list_buffer_size/sizeof(LIST_UPDATE_MESSAGE)));
							}
						}
#endif//ASYNCHRONOUS_PROGRAMMING
#endif//VC_PROGRAM
#endif//EDGE_PROGRAM_OPTIMIZATION
						multi_log_context->timer_edge_processing.end_timer();
#if(DEBUG_CODE == 1)
#if(EDGE_PROGRAM_OPTIMIZATION == 0)
						associativeVC_output_file[0] << "mls = " << message_list.size() << " els = " << edge_list.size() << " rds = " << result_data.size() << endl;
#endif//EDGE_PROGRAM_OPTIMIZATION
#endif//DEBUG_CODE
#if(EDGE_PROGRAM_OPTIMIZATION == 0)
						multi_log_context->timer_multi_log_access.start_timer();
						for(VERTEX_TYPE loop_3 = 0; loop_3 < result_data.size(); loop_3++)
						{
							UPDATE_MESSAGE message_to_send;
							message_to_send.target = edge_list[loop_3];
							message_to_send.data = result_data[loop_3];
							multi_log_cache->send_message((char *)(&message_to_send), sizeof(UPDATE_MESSAGE));
						}
						multi_log_context->timer_multi_log_access.end_timer();
						message_list.clear();
						edge_list.clear();
						result_data.clear();	
#endif//EDGE_PROGRAM_OPTIMIZATION
					}
#if(MEASURE == 1)
					multi_log_context->timer_edge_program.end_timer();
#endif//MEASURE
					graph_data->vertex_index_list[current_buffer].clear();
					graph_data->buffer_elements[current_buffer] = 0;
					((graph_data->buffer_lock)[current_buffer]).lock();
					(graph_data->buffer_ready_bit)[current_buffer] = 0;
					((graph_data->buffer_lock)[current_buffer]).unlock();
#if(EDGE_LOG_PROCESSING == 1)
					if(multi_log_context->edge_log_read == 0)
					{
#endif//EDGE_LOG_PROCESSING
					current_buffer = (current_buffer + 1) % 2;
#if(EDGE_LOG_PROCESSING == 1)
					}
#endif//EDGE_LOG_PROCESSING
#if(DEBUG_CODE == 1)
					associativeVC_output_file[0] << "cn = " << current_buffer << endl;
					associativeVC_output_file[0] << "Finished processing buffer in associative VC" << endl;
#endif//DEBUG_CODE
#if(EDGE_LOG_PROCESSING == 1)
					if(multi_log_context->edge_log_read == 1)
					{
						break;
					}
#endif//EDGE_LOG_PROCESSING
				}
			}
		}
#if(MEASURE == 1)
		multi_log_context->timer_log_generation.end_timer();
#endif//MEASURE
#if(MEASURE == 1)
		multi_log_context->timer_value_access.start_timer();
#endif//MEASURE
		lseek64(multi_log_context->graph_info.vertexValue_file_id, ((DATA_SIZE)startingVertexIndexOfPartition) * sizeof(VERTEX_VALUE_TYPE), SEEK_SET);
		write(multi_log_context->graph_info.vertexValue_file_id, partition_vertex_values, ((BUFFER_SIZE)activeVertexSize) * sizeof(VERTEX_VALUE_TYPE));
#if(MEASURE == 1)
		multi_log_context->timer_value_access.end_timer();
#endif//MEASURE
		free(message_index);
		free(partition_vertex_values);
		free(active_list);
	}
#if(DEBUG_CODE == 1)
	associativeVC_output_file[0] << "Done with VC" << endl;
#endif//DEBUG_CODE
/*---------------------Update multi-log----------------*/
#if(ASYNCHRONOUS_PROGRAMMING == 0)
#if(EDGE_LOG_PROCESSING == 1)
	if(multi_log_context->edge_log_write == 1)
	{
		//change file top
		edge_log_multi_log_cache->currentFileTop[((edge_log_multi_log_cache->current_partition_file) + 1) % 2] = 0;
		edge_log_multi_log_cache->current_partition_file = (edge_log_multi_log_cache->current_partition_file+1)%2;
	}
#endif//EDGE_LOG_PROCESSING
	multi_log_cache->currentFileTop[((multi_log_cache->current_partition_file) + 1) % 2] = 0;
	multi_log_cache->current_partition_file = (multi_log_cache->current_partition_file+1)%2;
#endif//ASYNCHRONOUS_PROGRAMMING
//	for(unsigned int i = 0; i < NumOfUtilityThreads; i++)
//	{
//		Utility_output_file[i].close();
//	}
//	free(partition_update_messages_buffer);
#if(MEASURE == 1)
	multi_log_context->timer_after_iteration.start_timer();
#endif//MEASURE
	(dynamic_cast<MultiLogProgram*>(multi_log_context->program))->after_iteration(multi_log_context->current_iteration, multi_log_context);
#if(MEASURE == 1)
	multi_log_context->timer_after_iteration.end_timer();
#endif//MEASURE
#if(EDGE_LOG_MEASURE == 1)
	multi_log_context->timer_graph_access.print_timer("Graph access time = ");
	cout << "number of graph pages requested = " << graph_data->number_of_pages_requested << endl;
#endif//EDGE_LOG_MEASURE
}

#if(VC_ASSOCIATIVE_PROGRAM == 1)
inline void MultiLogVC::Application_function_combine(VERTEX_TYPE vertex_id, MESSAGE_LIST Message_list, VERTEX_TYPE Message_list_size, VERTEX_VALUE_TYPE &vertex_value, bool *active_list, MultiLogContext *multi_log_context)
{
	for(VERTEX_TYPE loop_1 = 0; loop_1 < Message_list_size; loop_1++)
	{
		vertex_value.change += Message_list[loop_1];
	}
}
#endif//VC_ASSOCIATIVE_PROGRAM

#if(STRUCTURE_UPDATE == 1)
void MultiLogContext::delete_edge(LOG_MESSAGE_PTR_TYPE message, LOG_MESSAGE_SIZE_TYPE message_size)
{
	structure_multi_log_cache->send_message(message, message_size);
}

void MultiLogContext::delete_vertex(VERTEX_TYPE vertex_id)
{
	structure_delete_vertex_list[vertex_id] = 1;
}
#endif//STRUCTURE_UPDATE
