#include "header.h"

extern std::queue<struct IORequest> cache_to_IO_request_queue;
extern mutex cache_to_IO_request_queue_lock;
extern vector<ofstream> Log_cache_output_file;


void Multi_log_cache::Initialize(MultiLogContext *multiLogContext, std::string file1, std::string file2, VERTEX_TYPE numNodes, PARTITION_NUM_TYPE partitions, BUFFER_SIZE partitionSize, CACHE_SIZE cacheSize, Asynchronous_IO *ioPtr, unsigned int cache_type)
{
	multi_log_context = multiLogContext;
	NumNodes = numNodes;
	num_of_partitions = partitions;
	partition_size = partitionSize;
	page_size = SSD_PAGE_SIZE;
	cache_size = cacheSize;
	io_ptr = ioPtr;
	cacheType = cache_type;
	if(cacheType == 0)
		io_ptr->multi_log_cache = this;
	else if(cacheType == 1)
	{
#if(STRUCTURE_UPDATE == 1)
		io_ptr->structure_multi_log_cache = this;
#endif//STRUCTURE_UPDATE
	}
	else if(cacheType == 2)
	{
#if(EDGE_LOG_PROCESSING == 1)
		io_ptr->edge_log_multi_log_cache = this;
#endif//EDGE_LOG_PROCESSING
	}
	current_partition_file = 0;
	currentFileTop.resize(2);
	currentFileTop[0] = 0;
	currentFileTop[1] = 0;
	log_file.resize(2);
	log_file[0] = file1;
	log_file[1] = file2;
	log_file_id.resize(2);
	log_file_id[0] = open(log_file[0].data(), O_RDWR | O_DIRECT | O_CREAT | O_TRUNC, S_IRWXU);
	if (log_file_id[0] == -1) {
		perror(log_file[0].data());
		exit(1);
	}
	log_file_id[1] = open(log_file[1].data(), O_RDWR | O_DIRECT | O_CREAT | O_TRUNC, S_IRWXU);
	if (log_file_id[1] == -1) {
		perror(log_file[1].data());
		exit(1);
	}
#if(EDGE_LOG_PROCESSING == 1 && EDGE_LOG_DEBUG_CODE == 1)
	Log_cache_output_file[0] << "file1 = " << log_file[0].data() << " file2 = " << log_file[1].data() << endl;
#endif//DEBUG_CODE
	vertexToPartitionPage.resize(2);
	for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
	{
		vertexToPartitionPage[loop_1].resize(num_of_partitions);
		for (unsigned int i=0;i<num_of_partitions;i++)
		{
			vertexToPartitionPage[loop_1][i].pageTopPointer = SSD_PAGE_SIZE;
			vertexToPartitionPage[loop_1][i].cpn = -1;
		}
	}
	for (unsigned int i=0;i<num_of_partitions;i++)
	{
		mutex_wrapper tmp_mtx;
		vertexToPartitionPage_lock.push_back(tmp_mtx);
	}

	if(posix_memalign((void **)&multi_log_cache, SSD_PAGE_SIZE, cache_size/SSD_PAGE_SIZE*sizeof(CacheBlock)))
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
	partition_page_info.resize(2);
	partition_page_info_lock.resize(2);
	for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
	{
		partition_page_info[loop_1].resize(num_of_partitions);
		for(unsigned int loop_2 = 0; loop_2 < num_of_partitions; loop_2++)
		{
			mutex_wrapper tmp_mtx;
			partition_page_info_lock[loop_1].push_back(tmp_mtx);
		}
	}
	for (unsigned int i=0; i<cache_size/SSD_PAGE_SIZE; i++)
	{
		freeCPN.push_back(i);
	}
}


Multi_log_cache::~Multi_log_cache()
{
	close(log_file_id[0]);
	for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
	{
		vertexToPartitionPage[loop_1].clear();
	}
	vertexToPartitionPage.clear();
	vertexToPartitionPage_lock.clear();
	for(unsigned int loop_1 = 0; loop_1 < 2; loop_1++)
	{
		for(unsigned int loop_2 = 0; loop_2 < num_of_partitions; loop_2++)
		{
			partition_page_info[loop_1][loop_2].clear();
		}
		partition_page_info_lock[loop_1].clear();
	}
	free(multi_log_cache);
}


void Multi_log_cache::send_message(LOG_MESSAGE_PTR_TYPE message, LOG_MESSAGE_SIZE_TYPE message_size)
{
	PARTITION_NUM_TYPE partition_num;
	if(multi_log_context->programType == 1)
	{
		assert(cacheType != 2);
		partition_num = ((UPDATE_MESSAGE *)message)->target / partition_size;
	}
	else if(multi_log_context->programType == 2)
	{
		PARTITION_NUM_TYPE count;
		//need to adjust the data type of binary search
#if(DEBUG_CODE == 1)
		Log_cache_output_file[0] <<"bS" << endl;
#endif//DEBUG_CODE
		VERTEX_TYPE search_target;
		if(cacheType != 2) search_target = ((UPDATE_MESSAGE *)message)->target;
		else search_target = *((VERTEX_TYPE *)message);
		binarySearch(multi_log_context->partition_starting_vertex, 0, multi_log_context->partition_starting_vertex.size()-1, search_target, &partition_num, &count);
#if(DEBUG_CODE == 1)
		if(cacheType == 2)
		{
			Log_cache_output_file[0] << "i = " << multi_log_context->current_iteration << " e d = " <<  search_target << " pn = " << partition_num << " c = " << count << " m_s = " << message_size << " pTP = " << vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer  << " c_p_f = " << current_partition_file << endl;
			for(BUFFER_SIZE loop_t1 = 0; loop_t1 < message_size; loop_t1 += sizeof(VERTEX_TYPE))
			{
				Log_cache_output_file[0] << "l_t1 = " << loop_t1 << " v = " << *((VERTEX_TYPE *)(message + loop_t1))  << endl;
			}
		}
#endif//DEBUG_CODE
		assert(count == 1);
	}
//	PARTITION_NUM_TYPE partition_num =  ((UPDATE_MESSAGE *)message)->target / (NumNodes / num_of_partitions);
	LOG_MESSAGE_SIZE_TYPE dataWritten = 0;
	LOG_MESSAGE_SIZE_TYPE dataToWrite = message_size;
	vertexToPartitionPage_lock[partition_num].lock();
	PAGE_OFFSET_TYPE availableSpace;
	while(dataWritten < message_size)
	{
		assert(vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer <= page_size);
		availableSpace = page_size - vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer;
		dataToWrite = availableSpace > (message_size - dataWritten) ? (message_size - dataWritten) : availableSpace;
		if(cacheType != 2)
			assert(dataToWrite <= SSD_PAGE_SIZE);
#if(DEBUG_CODE == 1)
		Log_cache_output_file[0] << "dW = " << dataWritten << " aS = " << availableSpace << " cT = " << cacheType << endl;
#endif//DEBUG_CODE
		if(vertexToPartitionPage[current_partition_file][partition_num].cpn != -1)
		{
			memcpy((char *)(multi_log_cache[vertexToPartitionPage[current_partition_file][partition_num].cpn].data) + vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer, (char *)message + dataWritten, dataToWrite);
		}
		availableSpace -= dataToWrite;
		dataWritten += dataToWrite;
		if(availableSpace == 0)
		{
			if(vertexToPartitionPage[current_partition_file][partition_num].cpn != -1)
			{
				unsigned int returnType;
				if(cacheType == 0)returnType = 1;
				else if(cacheType == 1) returnType = 4;
				else if(cacheType == 2) returnType = 5;
				IORequest io_request = {log_file_id[current_partition_file], vertexToPartitionPage[current_partition_file][partition_num].cpn, (void *)(multi_log_cache[vertexToPartitionPage[current_partition_file][partition_num].cpn].data), 0, returnType, currentFileTop[current_partition_file], SSD_PAGE_SIZE, vertexToPartitionPage[current_partition_file][partition_num].partition_page_info_ptr};
				cache_to_IO_request_queue_lock.lock();
				cache_to_IO_request_queue.push(io_request);
				cache_to_IO_request_queue_lock.unlock();
				currentFileTop[current_partition_file] += SSD_PAGE_SIZE;
				//for the last page need to maintain the number of data elements in the page also
			}
#if(DEBUG_CODE == 1)
			Log_cache_output_file[0] << "b fC" << endl;
#endif//DEBUG_CODE
			CPN_TYPE availableCPN;
			while(1)
			{
				list_update_lock.lock();
				if(!freeCPN.empty())
				{
					availableCPN = freeCPN.front();
					freeCPN.pop_front();
#if(DEBUG_CODE == 1)
					Log_cache_output_file[0] << "f s = " << freeCPN.size() << endl;
#endif//DEBUG_CODE
					list_update_lock.unlock();
					break;
				} else {
					list_update_lock.unlock();
				}
			}
#if(DEBUG_CODE == 1)
			Log_cache_output_file[0] << "a fC" << endl;
#endif//DEBUG_CODE
			struct page_info page_location_info;
			page_location_info.cpn = availableCPN;
			page_location_info.isCached = 1;
			struct partition_cache_page_pointer cache_pointer;
			cache_pointer.cpn = availableCPN;
			cache_pointer.pageTopPointer = 0;

			partition_page_info_lock[current_partition_file][partition_num].lock();
			partition_page_info[current_partition_file][partition_num].push_back(page_location_info);
			cache_pointer.partition_page_info_ptr = --partition_page_info[current_partition_file][partition_num].end();
			partition_page_info_lock[current_partition_file][partition_num].unlock();
			//We already have the lock for this
			vertexToPartitionPage[current_partition_file][partition_num] = cache_pointer;
#if(EDGE_LOG_DEBUG_CODE == 1)
			if(cacheType == 2)
			{
				Log_cache_output_file[0] << "el cpn = " << page_location_info.cpn << endl;
			}
#endif//EDGE_LOG_DEBUG_CODE
		}
		else {
#if(DEBUG_CODE == 1)
			if(cacheType == 2)
				Log_cache_output_file[0] << "d = " <<  *(VERTEX_TYPE *)message << " pn = " << partition_num << " cpf = " << current_partition_file << " pTP = " << vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer << " t = " << *((VERTEX_TYPE *)message) << " data s = " << ((VERTEX_TYPE *)message)[1] << " cpn = " << vertexToPartitionPage[current_partition_file][partition_num].cpn << endl;
#endif//DEBUG_CODE
			vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer += dataToWrite;
		}
	}
	vertexToPartitionPage_lock[partition_num].unlock();
	//keep all the partition pages list in memory, you might need to keep 2 lists for each of the alternating partition
}

#if(ASYNCHRONOUS_PROGRAMMING == 1)
extern CONFIGURATION configuration;

void Multi_log_cache::send_message_partition(LOG_MESSAGE_PTR_TYPE message, LOG_MESSAGE_SIZE_TYPE message_size, PARTITION_NUM_TYPE partition_num)
{
	LOG_MESSAGE_SIZE_TYPE dataWritten = 0;
	LOG_MESSAGE_SIZE_TYPE dataToWrite = message_size;
	assert(SSD_PAGE_SIZE % message_size == 0);
	vertexToPartitionPage_lock[partition_num].lock();
	PAGE_OFFSET_TYPE availableSpace;
	while(dataWritten < message_size)
	{
		assert(vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer <= page_size);
		availableSpace = page_size - vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer;
		dataToWrite = availableSpace > (message_size - dataWritten) ? (message_size - dataWritten) : availableSpace;
		assert(dataToWrite <= SSD_PAGE_SIZE);
		assert((dataToWrite == message_size) || (dataToWrite == 0));
#if(DEBUG_CODE == 1)
//		Log_cache_output_file[0] << "dW = " << dataWritten << " aS = " << availableSpace << endl;
#endif//DEBUG_CODE
		if(vertexToPartitionPage[current_partition_file][partition_num].cpn != -1)
		{
			memcpy((char *)(multi_log_cache[vertexToPartitionPage[current_partition_file][partition_num].cpn].data) + vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer, (char *)message + dataWritten, dataToWrite);
		}
		availableSpace -= dataToWrite;
		dataWritten += dataToWrite;
		if(availableSpace == 0)
		{
			if(vertexToPartitionPage[current_partition_file][partition_num].cpn != -1)
			{
				unsigned int returnType;
				if(cacheType == 0)returnType = 1;
				else if(cacheType == 1) returnType = 4;
				else if(cacheType == 2) returnType = 5;
				IORequest io_request = {log_file_id[current_partition_file], vertexToPartitionPage[current_partition_file][partition_num].cpn, (void *)(multi_log_cache[vertexToPartitionPage[current_partition_file][partition_num].cpn].data), 0, returnType, currentFileTop[current_partition_file], SSD_PAGE_SIZE, vertexToPartitionPage[current_partition_file][partition_num].partition_page_info_ptr};
				cache_to_IO_request_queue_lock.lock();
				cache_to_IO_request_queue.push(io_request);
				cache_to_IO_request_queue_lock.unlock();
				currentFileTop[current_partition_file] = ( currentFileTop[current_partition_file] + SSD_PAGE_SIZE) % configuration.max_message_file_top;
#if(DEBUG_CODE == 1)
				Log_cache_output_file[0] << "cpf = " << current_partition_file[current_partition_file] << " pn = " << partition_num << endl;
#endif//DEBUG_CODE
				assert(((currentFileTop[current_partition_file] + SSD_PAGE_SIZE) % configuration.max_message_file_top) % SSD_PAGE_SIZE == 0);
				//for the last page need to maintain the number of data elements in the page also
			}
#if(DEBUG_CODE == 1)
			Log_cache_output_file[0] << "b fC" << endl;
#endif//DEBUG_CODE
			CPN_TYPE availableCPN;
			while(1)
			{
				list_update_lock.lock();
				if(!freeCPN.empty())
				{
					availableCPN = freeCPN.front();
					freeCPN.pop_front();
#if(DEBUG_CODE == 1)
					Log_cache_output_file[0] << "f s = " << freeCPN.size() << endl;
#endif//DEBUG_CODE
					list_update_lock.unlock();
					break;
				} else {
					list_update_lock.unlock();
				}
			}
#if(DEBUG_CODE == 1)
			Log_cache_output_file[0] << "ac = " << availableCPN << " pn = " << partition_num << endl;
#endif//DEBUG_CODE
			struct page_info page_location_info;
			page_location_info.cpn = availableCPN;
			page_location_info.isCached = 1;
			struct partition_cache_page_pointer cache_pointer;
			cache_pointer.cpn = availableCPN;
			cache_pointer.pageTopPointer = 0;

			partition_page_info_lock[current_partition_file][partition_num].lock();
			partition_page_info[current_partition_file][partition_num].push_back(page_location_info);
			cache_pointer.partition_page_info_ptr = --partition_page_info[current_partition_file][partition_num].end();
			partition_page_info_lock[current_partition_file][partition_num].unlock();
			//We already have the lock for this
			vertexToPartitionPage[current_partition_file][partition_num] = cache_pointer;
		}
		else {
#if(DEBUG_CODE == 1)
	//		Log_cache_output_file[0] << "d = " <<  ((UPDATE_MESSAGE *)message)->target << " pn = " << partition_num << " cpf = " << current_partition_file << " pTP = " << vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer << " t = " << (*((UPDATE_MESSAGE *)message)).target << " data = " << (*((UPDATE_MESSAGE *)message)).data << " cpn = " << vertexToPartitionPage[current_partition_file][partition_num].cpn << endl;
#endif//DEBUG_CODE
			vertexToPartitionPage[current_partition_file][partition_num].pageTopPointer += dataToWrite;
		}
	}
	vertexToPartitionPage_lock[partition_num].unlock();
	//keep all the partition pages list in memory, you might need to keep 2 lists for each of the alternating partition
}
#endif//ASYNCHRONOUS_PROGRAMMING

//number of pages, partitions, data structure for keeping track of partition pages, threshold rate after which one has to evict the pages, file to which one has to evict

void Multi_log_cache::handleReturnRequest(struct IORequest io_req)
{
	(io_req.page_info_ptr)->isCached = 0;
	(io_req.page_info_ptr)->file_offset = io_req.file_offset;
	list_update_lock.lock();
	freeCPN.push_back(io_req.cpn);
	list_update_lock.unlock();
}


void Multi_log_cache::print_partitions()
{
	for(unsigned int loop_1 = 0; loop_1 < num_of_partitions; loop_1++)
	{
		for(list<struct page_info>::iterator page_info_ptr = partition_page_info[current_partition_file][loop_1].begin(); page_info_ptr != partition_page_info[current_partition_file][loop_1].end(); page_info_ptr++)
		{
			if(page_info_ptr->isCached == 1)
			{
				cout << loop_1 << " " << page_info_ptr->isCached << " " << page_info_ptr->cpn << endl;
				for(unsigned int loop_3 = 0; loop_3 < vertexToPartitionPage[current_partition_file][loop_1].pageTopPointer; loop_3 += sizeof(unsigned int))
				{
					cout << loop_1 << " " << loop_3 / sizeof(unsigned int) << " " << ((unsigned int *)(multi_log_cache[page_info_ptr->cpn].data))[loop_3 / sizeof(unsigned int)] << endl;
				}
			}
			else {
				cout << loop_1 << " " << page_info_ptr->isCached << " " << page_info_ptr->file_offset << endl;

			}
		}
	}
}
