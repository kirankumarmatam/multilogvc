#include "header.h"

int main(int argc, char *argv[])
{
	Initialization(argc, argv);
	std::string csr_file_header(argv[1]);
	std::string csr_file_content(argv[2]);
	VERTEX_TYPE NumNodes;
	DATA_SIZE NumEdges;
	fstream csrHeader;
	csrHeader.open(argv[1]);
	csrHeader.read((char *)(&NumNodes), sizeof(VERTEX_TYPE));
	csrHeader.read((char *)(&NumEdges), sizeof(DATA_SIZE));
	csrHeader.close();
	omp_set_nested(100);
	cout << "NumNodes = " << NumNodes << " NumEdges = " << NumEdges << endl;

	Asynchronous_IO asyncIO;
	Utils utility_object(&asyncIO);
	BUFFER_SIZE rowPtr_buffer_size = 2 * (BUFFER_SIZE)SSD_PAGE_SIZE, colPtr_buffer_size = 1024 * (BUFFER_SIZE)SSD_PAGE_SIZE;
#if(DEBUG == 1)
	cout << "rPbs = " << rowPtr_buffer_size << " cPbs = " << colPtr_buffer_size << endl;
	cout << "Before graph data loader" << endl;
#endif//DEBUG
	Graph_data_loader graph_loader(csr_file_content, &utility_object, rowPtr_buffer_size, colPtr_buffer_size, NumNodes);
#if(DEBUG == 1)
	cout << "Before associateVC deceleration" << endl;
#endif//DEBUG
	AssocativeMultiLogVC associateVC(&graph_loader, rowPtr_buffer_size, NumNodes);
#if(DEBUG == 1)
	cout << "Before associateVC run" << endl;
#endif//DEBUG
#pragma omp parallel num_threads(2)
	{
		int tid = omp_get_thread_num();
		if(tid == 0)
		{
			associateVC.run();
#if(DEBUG == 1)
			cout << "After associateVC run" << endl;
#endif//DEBUG
			Finalize();
			exit(0);
		}
		else {
			asyncIO.IO_manager();
		}
	}
}
