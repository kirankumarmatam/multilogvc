#include "header.h"

int main(int argc, char *argv[])
{
	std::string log_file1(argv[1]);
	std::string log_file2(argv[2]);
	PARTITION_NUM_TYPE num_partitions = 5;
	CACHE_SIZE cache_size = SSD_PAGE_SIZE * num_partitions * 10;
	Asynchronous_IO IO_ptr;
	Multi_log_cache multi_log_cache(log_file1, log_file2, 100000, num_partitions, cache_size, &IO_ptr);
	IO_ptr.multi_log_cache = &multi_log_cache;

#pragma omp parallel num_threads(2)
	{
		int tid = omp_get_thread_num();
		if(tid == 0)
		{
			for(unsigned int loop_1 = 0; loop_1 < 100000; loop_1++)
			{
				unsigned int *message = (unsigned int *)malloc(sizeof(unsigned int));
				*message = loop_1;
				multi_log_cache.send_message((LOG_MESSAGE_PTR_TYPE)message, sizeof(unsigned int), loop_1);
				free(message);
			}
			multi_log_cache.print_partitions();
			exit(0);
		}
		else {
			IO_ptr.IO_manager();
		}
	}
}
