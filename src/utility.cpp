#include "header.h"

void Timer::start_timer()
{
	clock_gettime(CLOCK_MONOTONIC, &timer_start);
}

void Timer::end_timer()
{
	clock_gettime(CLOCK_MONOTONIC, &timer_end);
	time_elapsed += ((double)timer_end.tv_sec - (double)timer_start.tv_sec) * 1000000000;
	time_elapsed += ((double)timer_end.tv_nsec - (double)timer_start.tv_nsec);
}

void Timer::record_print_timer(const char *string)
{
	clock_gettime(CLOCK_MONOTONIC, &timer_end);
	double t = 0;
	t += ((double)timer_end.tv_sec - (double)timer_start.tv_sec) * 1000000000;
	t += ((double)timer_end.tv_nsec - (double)timer_start.tv_nsec);
	cout << string << " " << t * 1.0 / 1000000000 << " (sec)"<< endl;
}

void Timer::print_timer(const char *string)
{
	cout << string << " " << time_elapsed * 1.0 / 1000000000 << " (sec)"<< endl;
}

double Timer::get_timer()
{
	clock_gettime(CLOCK_MONOTONIC, &timer_end);
	time_elapsed += ((double)timer_end.tv_sec - (double)timer_start.tv_sec) * 1000000000;
	time_elapsed += ((double)timer_end.tv_nsec - (double)timer_start.tv_nsec);
//	cout << (double)timer_end.tv_sec << " " << (double)timer_end.tv_nsec << " " << (double)timer_start.tv_sec << " " << (double)timer_start.tv_nsec << endl;
	return time_elapsed;
}
