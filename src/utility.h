class Timer {
	public:
		Timer()
		{
			time_elapsed = 0;
		}
		double time_elapsed;
		struct timespec timer_start, timer_end;
		void start_timer();
		void end_timer();
		void record_print_timer(const char *string);
		void print_timer(const char *str);
		double get_timer();
};
